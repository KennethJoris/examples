<?php

/**
 *
 * THE FORM SENDER
 *
 */

function sendIt($theData) {

	if(!isset($theData)) { return ['success' => false, 'message' => 'No form data!']; }

	$to      = 'info@kenneth-joris.be';
	$subject = 'Webformulier kenneth-joris.be';
	$headers = 'From: Webformulier <info@kenneth-joris.be>';
	$message = '';
	$message.= 'Naam: ' . $theData['name'] . "\r\n";
	$message.= 'E-mail: ' . $theData['email'] . "\r\n";
	$message.= 'Bericht:' . "\r\n" . "\r\n" . $theData['message'];

	$send = mail($to, $subject, $message, $headers);

	$theThanksMessage = '<h3>Thanks %NAME%,</h3><p>I will contact you as soon as possible.</p>';
	$theThanksMessage = str_replace('%NAME%', $theData['name'], $theThanksMessage);

	return $send ? ['success' => true, 'message' => $theThanksMessage] : ['success' => false, 'message' => 'Something went wrong with the sending of the email!'];

}

if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message']) && !isset($_POST['honeypot'])) {

	$theData = $_POST;

	echo json_encode(sendIt($theData));

	die;

}

?>