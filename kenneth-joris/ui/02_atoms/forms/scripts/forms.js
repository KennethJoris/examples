const form = (function(w, d, $, undefined) {

	'use strict';

	const s = {
		selectors: {
			theForm: ".js-form",
			theFieldset: ".js-form__field--floating-label",
			theLabel: "label",
			theInput: "input, select, textarea",
			theContactForm: "#contact__form"
		},
		classes: {
			isActive: "is-active",
			isCompleted: "is-completed",
		}

	},
		els = {},
		init = function() {

			// define elements
			els.theForm = d.querySelectorAll(s.selectors.theForm),
				els.theFieldset = d.querySelectorAll(s.selectors.theFieldset),
				els.theContactForm = d.querySelector(s.selectors.theContactForm);

			// no elements
			if (!els.theForm.length) { return; }

			// the Binds
			const theInputs = d.querySelectorAll(s.selectors.theInput),
				theInputsLength = theInputs.length;

			for (let index = 0; index < theInputsLength; index++) {
				theInputs[index].value = ''; // empty fields
				theInputs[index].addEventListener('focusout', focusOut);
			}

			const theInputsAndLabels = d.querySelectorAll(s.selectors.theInput + ", " + s.selectors.theLabel),
				theInputsAndLabelsLength = theInputsAndLabels.length;

			for (let index = 0; index < theInputsAndLabelsLength; index++) {
				theInputsAndLabels[index].addEventListener('focusin', focusIn);
				theInputsAndLabels[index].addEventListener('click', focusIn);
				theInputsAndLabels[index].addEventListener('change', focusIn);
			}

		},
		focusIn = function() {
			const el = this,
				parent = el.parentNode;

			parent.classList.add(s.classes.isActive);
			parent.classList.add(s.classes.isCompleted);
		},
		focusOut = function() {
			const el = this,
				parent = el.parentNode;

			if (el.value === "") {
				parent.classList.remove(s.classes.isCompleted);
			}
			parent.classList.remove(s.classes.isActive);
		};

	return {
		init: init
	};

}(window, window.document, window.jQuery));
