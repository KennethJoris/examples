const validation = (function(w, d, FORM, undefined) {

	'use strict';

	const s = {
		selectors: {
			theForm: '#contact__form',
			theContent: '.contact__content',
			theInput: '.input',
			theMsg: "#contact__form__message"
		},
		classes: {
			theValidation: 'form__field'
		}
	},
		els = {},
		init = function() {

			// no dependecies
			if (!FORM.validator) { console.log('FORM Validator is not loaded!'); return; }

			// define elements
			els.theForm = d.querySelector(s.selectors.theForm);
			els.theContent = d.querySelector(s.selectors.theContent);
			els.theMsg = d.querySelector(s.selectors.theMsg);

			// no element
			if (!els.theForm || !els.theContent) { return; }

			// define settings
			s.theFormContent = els.theContent.innerHTML;
			s.theSettings = {
				live: true,
				liveAfterSubmit: true,
				keyup: false,
				onError: function(el, isError) {

					var theParent = getParent(el, s.classes.theValidation);

					if (isError) {
						theParent.classList.add('field--error');
					} else {
						theParent.classList.remove('field--error');
					}

				},
				onSubmit: function(e) {

					var theData = getFormData(els.theForm);
					e.preventDefault();

					sendForm(theData);
				}
			};

			// define the validator
			s.theValidator = FORM.validator(s.selectors.theForm, s.theSettings);

		},
		getFormData = function(theForm) {

			// define variables
			var theData = [];

			// no params
			if (!theForm) { return; }

			// loop through all fields and collect the name and value as a string
			Array.prototype.slice.call(theForm.querySelectorAll('[name]')).forEach(function(el) {

				// check for honeypot - don't add this field if empty
				if (el.name === 'honeypot' && el.value.length === 0) { return; }
				// add to array
				theData.push(el.name + '=' + el.value);

			});

			// concat all
			theData = theData.join('&');

			// return
			return theData;

		},
		sendForm = function(dataToSend) {

			// define variables
			var xmlhttp = new XMLHttpRequest();

			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {

					// define variables
					var theResponse = JSON.parse(xmlhttp.responseText);

					// check for success
					theResponse.success ? showThanks(theResponse.message) : console.log(theResponse.message);

				}
			};

			xmlhttp.open("POST", './mail.php', true);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send(dataToSend);

		},
		showThanks = function(theMessage) {

			// hide form msg
			els.theMsg.style.display = 'none';

			// show form msg
			els.theMsg.innerHTML = theMessage;
			els.theMsg.style.display = 'block';

			// reset form
			resetForm();

		},
		resetForm = function() {

			els.theContent.innerHTML = s.theFormContent;
			init();

		},
		getParent = function(el, theClass) {

			// define variables
			var theParent = el.parentNode;

			// no params
			if (!el || !theClass) { return; }

			// as long as theParent does not contains the given class or theParent is not the document
			while (theParent && !theParent.classList.contains(theClass) && theParent !== d) {

				// get the next parent
				theParent = theParent.parentNode;

			}

			// return
			return theParent;

		};

	return {
		init: init,
		resetForm: resetForm
	};

}(window, window.document, window.FORM));
