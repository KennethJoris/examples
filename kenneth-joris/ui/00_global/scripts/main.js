(function(w, d, undefined) {

	"use strict";

	const raf = requestAnimationFrame || mozRequestAnimationFrame || webkitRequestAnimationFrame || msRequestAnimationFrame,
		init = function() {

			w.scrollHandler.init();
			w.scrollIntoViewHandler.init();
			w.form.init();
			w.validation.init();

			d.body.classList.add('window-is-loaded');
			d.documentElement.classList.remove('no-js');

		};

	// when all is loaded
	raf ? raf(function() { w.setTimeout(init, 0); }) : w.addEventListener('load', init);

}(window, window.document));
