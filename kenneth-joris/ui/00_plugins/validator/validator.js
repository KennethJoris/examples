/**
 * Form Validator
 */

(function(w, FORM, undefined) {

	if (!FORM.validator) {

		var Form = function(id, settings) {

			var form = this;

			this.id = id;
			this.el = document.getElementById(this.id) || document.querySelector(this.id);

			if (!this.el) { console.error('FORM Validator: element not found.'); return; }

			this.el.setAttribute('novalidate', true);
			this.defaults = {
				live: true,
				liveAfterSubmit: false,
				focusOnFirstError: true,
				keyup: false,
				debug: false,
				onError: function(el, isError) { },
				onSubmitError: function() { },
				onSubmit: function() { }
			};

			this.settings = Object.create(this.defaults);
			if (settings) { [].forEach.call(Object.keys(settings), function(key) { form.settings[key] = settings[key]; }); }

			this.debug = this.settings.debug;
			this.settings.live && this.liveEvents();

			this.el.addEventListener('submit', function(e) {

				if (!form.valid()) {

					e.preventDefault();
					form.settings.onSubmitError();

					!form.submitted && form.settings.liveAfterSubmit && form.liveEvents();

				} else if (typeof (form.settings.onSubmit) === typeof (function() { })) {

					form.settings.onSubmit(e);

				}

				form.submitted = true;

			});

		};

		Form.prototype.liveEvents = function() {

			var form = this;

			if (this.el.nodeName.toLowerCase() !== 'form') { console.error('Element with id #' + this.id + ' is not a form element'); return false; }

			[].forEach.call(this.el.elements, function(el, i) {

				form.debug && console.log(el);

				if (el.type) {

					!form.settings.keyup && el.addEventListener('blur', function() { form.onBlur.call(this, form, el); });
					form.settings.keyup && el.addEventListener('keyup', function() { form.onBlur.call(this, form, el); });

					if (el.type === 'file') {

						el.addEventListener('change', function(e) {

							//console.log(form.isFile(el));
							this.value && !form.isFile(this) ? form.settings.onError(el, true) : form.settings.onError(el, false);

						});

					}
				}

				if (el.nodeName.toLowerCase() === 'select') {

					el.addEventListener('change', function() {
						!form.hasValue(this) ? form.settings.onError(el, true) : form.settings.onError(el, false);
					});

				}

			});

		};

		Form.prototype.onBlur = function(form, el) {

			if (this.required && !this.value) { form.settings.onError(el, true); return; }

			switch (this.type) {
				case 'number':
					this.value && !form.isNumber(this) ? form.settings.onError(el, true) : form.settings.onError(el, false);
					break;
				case 'email':
					this.value && !form.isEmail(this) ? form.settings.onError(el, true) : form.settings.onError(el, false);
					break;
				case 'tel':
					this.value && !form.isTel(this) ? form.settings.onError(el, true) : form.settings.onError(el, false);
					break;
				case 'url':
					this.value && !form.isUrl(this) ? form.settings.onError(el, true) : form.settings.onError(el, false);
					break;
				case 'date':
					this.value && !form.isDate(this) ? form.settings.onError(el, true) : form.settings.onError(el, false);
					break;
				case 'button':
				case 'submit':
					break;
				default:
					this.value && !form.hasValue(this) ? form.settings.onError(el, true) : form.settings.onError(el, false);

			}
		};

		Form.prototype.valid = function() {

			var form = this,
				errors = 0,
				focusEl;

			if (!this.el || this.el.nodeName.toLowerCase() !== 'form') {

				console.error('Element with id #' + this.id + ' is not a form element');
				return false;

			}

			[].forEach.call(this.el.elements, function(el) {

				form.debug && console.log('validate', el);

				if (!el) { return; }
				if (el.required && !form.hasValue(el)) {

					form.settings.onError(el, true);

					if (!errors) { focusEl = el; }

					errors++;

				} else {

					if (el.type) {

						switch (el.type) {
							case 'number':
								if (el.value && !form.isNumber(el)) {
									form.settings.onError(el, true);
									if (!errors) { focusEl = el; }
									errors++;
								} else {
									form.settings.onError(el, false);
								}
								break;
							case 'email':
								if (el.value && !form.isEmail(el)) {
									form.settings.onError(el, true);
									if (!errors) { focusEl = el; }
									errors++;
								} else {
									form.settings.onError(el, false);
								}
								break;
							case 'tel':
								if (el.value && !form.isTel(el)) {
									form.settings.onError(el, true);
									if (!errors) { focusEl = el; }
									errors++;
								} else {
									form.settings.onError(el, false);
								}
								break;
							case 'url':
								if (el.value && !form.isUrl(el)) {
									form.settings.onError(el, true);
									if (!errors) { focusEl = el; }
									errors++;
								} else {
									form.settings.onError(el, false);
								}
								break;
							case 'date':
								if (el.value && !form.isDate(el)) {
									form.settings.onError(el, true);
									if (!errors) { focusEl = el; }
									errors++;
								} else {
									form.settings.onError(el, false);
								}
								break;
							case 'file':
								if (el.value && !form.isFile(el)) {
									form.settings.onError(el, true);
									if (!errors) { focusEl = el; }
									errors++;
								} else {
									form.settings.onError(el, false);
								}
								break;
							case 'radio':
							case 'checkbox':
								if (el.required) {
									if (!form.isChecked(el.name)) {
										form.settings.onError(el, true);
										if (!errors) { focusEl = el; }
										errors++;
									} else {
										form.settings.onError(el, false);
									}
								}
								break;
							case 'button':
							case 'submit':
								break;

						}

					}

					if (el.nodeName.toLowerCase() === 'select') {

						if (el.required && !form.hasValue(el)) {

							form.settings.onError(el, true);
							if (!errors) { focusEl = el; }
							errors++;

						} else {

							form.settings.onError(el, false);

						}

					}

				}

			});

			if (this.settings.focusOnFirstError) {
				this.settings.debug && console.log(focusEl);
				focusEl && focusEl.focus();
			}

			return !errors;

		};

		Form.prototype.hasValue = function(el) {

			if (el.nodeName.toLowerCase() === 'select') {
				this.debug && console.log('hasValue', el.options[el.selectedIndex].value, el.options[el.selectedIndex].value !== '');
				return el.options[el.selectedIndex].value !== '';
			}

			if (el.type && (el.type === 'checkbox' || el.type === 'radio')) {
				return form.isChecked(el.name) ? el.name : false;
			}

			return el.value ? el.value : false;

		};

		Form.prototype.isNumber = function(el) {

			var value = parseFloat(el.value);
			this.debug && console.log('isNumber', value);
			return !!(this.hasValue(el) && el.checkValidity() && !isNaN(value) && isFinite(value));

		};

		Form.prototype.isEmail = function(el) {

			this.debug && console.log('isEmail', el.value);
			return !!(this.hasValue(el) && el.checkValidity() && /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(el.value));

		};

		Form.prototype.isDate = function(el) {

			this.debug && console.log('isDate', el.value);

			///^(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)\d{2}))|(2902((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/
			// dd-mm-yyyy /(\d){1,2}[\/-](\d){2}[\/-](\d){4}/
			// yyyy-mm-dd /(\d){4}[\/-](\d){2}[\/-](\d){2}/
			// mm-dd-yyyy /(\d){2}[\/-](\d){1,2}[\/-](\d){4}/

			// todo check correct date

			return !!(this.hasValue(el) && el.checkValidity() && /(\d){4}[\/-](\d){2}[\/-](\d){2}/.test(el.value));

		};

		Form.prototype.isTel = function(el) {

			var val = el.value.match(/\d/g) || '';
			this.debug && console.log('isTel', el.value);
			return !!(this.hasValue(el) && el.checkValidity() && val.length >= 9 && val.length <= 15);

		};

		Form.prototype.isUrl = function(el) {

			this.debug && console.log('isUrl', el.value);
			return !!(this.hasValue(el) && el.checkValidity() && /([--:\w?@%&+~#=]*\.[a-z]{2,4}\/{0,2})((?:[?&](?:\w+)=(?:\w+))+|[--:\w?@%&+~#=]+)?/.test(el.value));

		};

		Form.prototype.isFile = function(el) {

			var errors = 0;

			this.debug && console.log('isFile');

			if (el.accept) {
				if (el.files) {

					[].forEach.call(el.files, function(file) {
						var accept = el.accept.split('/'),
							type = file.type.split('/');

						(type[0] !== accept[0]) && errors++;
						(accept[1] !== '*' && type[1] !== accept[1]) && errors++;
					});

					return !errors;

				}
			}

			return !!(this.hasValue(el) && el.checkValidity());

		};

		Form.prototype.isChecked = function(name) {

			return !!this.el.querySelectorAll('[name="' + name + '"]:checked').length;

		};

		FORM.validator = function(id, debug) { return new Form(id, debug); };

	}

}(window, window.FORM = window.FORM || {}));
