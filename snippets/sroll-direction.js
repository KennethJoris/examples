const scrollHandler = (function(w, d, undefined) {

	'use strict';

	const s = {
		selectors: {
			theHtml: 'html'
		},
		classes: {
			scrollUp: 'js_scrolling-up',
			scrollDown: 'js_scrolling-down',
			scrollDeadZone: 'js_scrolling-deadzone'
		},
		theScrollTimer: null,
		theNewScrollPosition: null,
		theOldScrollPosition: null,
		theScrollDeadzone: null,
		theCurrentScrollDirection: null,
		theCurrentBreakpoint: null,
		ticking: false,

	},
		els = {},
		init = function() {

			// define elements
			els.theHtml = document.querySelector(s.selectors.theHtml);

			// no elements
			if (!els.theHtml) { return; }

			// define settings
			s.theOldScrollPosition = w.pageYOffset;
			s.theScrollDeadzone = 100;

			// scroll setup
			w.addEventListener('scroll', scrollIt);

		},
		scrollIt = function() {

			// Get the new scroll position
			s.theNewScrollPosition = window.scrollY;
			requestTick();

		},
		requestTick = function() {

			// Trottle with requestAnimationFrame
			if (!s.ticking) {
				requestAnimationFrame(update);
			}
			s.ticking = true;

		},
		update = function() {

			// reset the tick so we can
			// capture the next onScroll
			s.ticking = false;

			// Define the action	
			if (s.theNewScrollPosition < s.theOldScrollPosition && s.theNewScrollPosition > s.theScrollDeadzone) {

				// Scrolling up
				els.theHtml.classList.add(s.classes.scrollUp);
				els.theHtml.classList.remove(s.classes.scrollDown);
				els.theHtml.classList.remove(s.classes.scrollDeadZone);

				// set the current scroll positon
				s.theCurrentScrollDirection = "up";

			} else if (s.theNewScrollPosition > s.theOldScrollPosition && s.theNewScrollPosition > s.theScrollDeadzone) {

				// Scrolling down
				els.theHtml.classList.add(s.classes.scrollDown);
				els.theHtml.classList.remove(s.classes.scrollUp);
				els.theHtml.classList.remove(s.classes.scrollDeadZone);

				// set the current scroll positon
				s.theCurrentScrollDirection = "down";

			} else if (s.theNewScrollPosition < s.theScrollDeadzone) {

				// Scrolling within deadzone
				els.theHtml.classList.add(s.classes.scrollDeadZone);
				els.theHtml.classList.remove(s.classes.scrollUp);
				els.theHtml.classList.remove(s.classes.scrollDown);

				// set the current scroll positon
				s.theCurrentScrollDirection = "deadzone";
			}

			// Replace the new scroll position
			s.theOldScrollPosition = s.theNewScrollPosition;

			// get the current breakpoint
			// s.theCurrentBreakpoint = w.getComputedStyle(d.querySelector('body'), ':before').getPropertyValue('content').replace(/\"/g, '');

		}

	return {
		init: init
	};

}(window, window.document));
