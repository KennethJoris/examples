/*

Js Animate

duration – the total animation time in ms.
timing – the function to calculate animation progress. Gets a time fraction from 0 to 1, returns the animation progress, usually from 0 to 1.
draw – the function to draw the animation.

example:

animateIt({
	duration: 1000,
	timing(timeFraction) {
		return timeFraction;
	},
	draw(progress) {
		elem.style.width = progress * 100 + '%';
	},
	animationStart() {
		console.log('animation start);
	},
	animationIteration() {
		console.log('animation running);
	},
	animationEnd() {
		console.log('animation end);
	}
});

Different timing functions: https://github.com/Genenenenaam/easing-utils

*/

const animateIt = ({ timing, draw, duration, animationStart, animationIteration, animationEnd }) => {

	let start = performance.now();

	/*--------------------------------------------
	- Callback
	---------------------------------------------*/
	// On start
	animationStart && animationStart();

	/*--------------------------------------------
	- Animation
	---------------------------------------------*/
	requestAnimationFrame(function animate(time) {
		// timeFraction goes from 0 to 1
		let timeFraction = (time - start) / duration;
		if (timeFraction > 1) timeFraction = 1;
		if (timeFraction < 0) timeFraction = 0;

		// calculate the current animation state
		let progress = timing(timeFraction);

		draw(progress); // draw it

		if (timeFraction < 1) {
			requestAnimationFrame(animate);
		}

		/*--------------------------------------------
		- Callbacks
		---------------------------------------------*/
		// On iteration
		if (timeFraction > 0 && timeFraction < 1) {
			animationIteration && animationIteration();
		}

		// On end
		if (timeFraction >= 1) {
			animationEnd && animationEnd();
		}

	});
};
