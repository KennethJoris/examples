/*--------------------------------------------
- Example
---------------------------------------------*//*

- SUBSCRIBE
---------------------------------------------

// create a function to subscribe to topics
var mySubscriber = function (data, eventName) {
    console.log( data, eventName );
};


// add the function to the list of subscribers for a particular topic
// IMPORTANT FOR UNSUBSCRIPTION: we're keeping the returned token,
// in order to be able to unsubscribe from the topic later on

var token = events.subscribe('Event Name', mySubscriber);


- UN-SUBSCRIBE
---------------------------------------------

// unsubscribe this subscriber from a topic
events.unsubscribe('Event Name', token);

// unsubscribe ALL subscribers from a topic
events.unsubscribeAll('Event Name');


- PUBLISH
---------------------------------------------
events.publish('Event Name', 'a string');
events.publish('Event Name', {'key':'value'} );
events.publish('Event Name', ['test','a','b','c'] );
events.publish('Event Name', [{'color':'blue'},{'text':'hello'}] );


- ERROR HANDELING
---------------------------------------------

// isPublished is a returned boolean that represents if any subscribers was registered for this topic
var isPublished = events.publish('Event Name');

// isUnsubscribed is a returned boolean that represents if any subscribers was unregistered for this topic
var isUnsubscribed = events.unsubscribe('Event Name', token);

*/

/*--------------------------------------------
- PubSub JS
---------------------------------------------*/
const events = (function(w, d, undefined) {

	'use strict';

	let unSubscriptionId = -1;
	const theEvents = {},
		subscribe = function(eventName, func) {

			// if not eventname present -> Notify and return
			if (!eventName) {
				console.error("events.subscribe() expects an eventName as argument!");
				return false;
			}

			// if not eventname exists -> add the eventname
			if (!_eventNameExists(eventName)) { theEvents[eventName] = []; }

			// add function to eventName and return unsubscription token
			const token = (++unSubscriptionId).toString();
			theEvents[eventName].push({ token: token, func: func });
			return token;

		},
		publish = function(eventName, data) {

			// if not eventname present -> Notify and return
			if (!eventName) {
				console.error("events.publish() expects an eventName as argument!");
				return false;
			}

			// if not eventname exists -> return
			if (!_eventNameExists(eventName)) { return false; }

			// Queque the execution
			// so it would not prevent subsequent code from executing
			setTimeout(function() {
				const theSubscribers = theEvents[eventName],
					index = theSubscribers ? theSubscribers.length : 0;

				// execute subscribers with data and eventname as arguments
				while (index--) { theSubscribers[index].func(data, eventName); }

			}, 0);

			// return publish succes
			return true;

		},
		unsubscribe = function(eventName, token) {

			// if not eventname present -> Notify and return
			if (!eventName) {
				console.error("events.unsubscribe() expects an eventName as argument!");
				return false;
			}

			// if not unsubscription token exists -> Notify and return
			if (!token) {
				console.error("events.unsubscribe('" + eventName + "') expects a 'unsubscription token' as argument!");
				return false;
			}

			// if not eventname exists -> return
			if (!_eventNameExists(eventName)) { return false; }

			// get the subscribers
			const theSubscribers = theEvents[eventName],
				index = theSubscribers ? theSubscribers.length : 0;

			// remove subscripion based on token
			while (index--) {
				if (theEvents[eventName][index].token === token) {
					theEvents[eventName].splice(index, 1);

					// return unsubscribe succes
					return true;
				}
			}

			// retrun false if token/subscriber not found in eventName
			return false;

		},
		unsubscribeAll = function(eventName) {

			// if not eventname present -> Notify and return
			if (!eventName) {
				console.error("events.unsubscribeAll() expects an eventName as argument!");
				return false;
			}

			// if not eventname exists -> return
			if (!_eventNameExists(eventName)) { return false; }

			// remove all subscribers from eventName
			theEvents[eventName] = [];

			// return unsubscribeAll succes
			return true;

		},
		_eventNameExists = function(eventName) {
			return !!theEvents[eventName];
		};

	return {
		subscribe: subscribe,
		publish: publish,
		unsubscribe: unsubscribe,
		unsubscribeAll: unsubscribeAll
	};

}(window, window.document));
