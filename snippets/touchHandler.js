const touchHandler = (function(w, d, undefined) {

	'use strict';

	const watch = function(touchelement, theOptions, theCallback) {

		/* SETTINGS
		---------------------------------------------*/

		// touch element
		const element = touchelement;

		// gesture mesurement variables
		let elapsedTime = undefined;
		let startTime = undefined;
		let gestureType = undefined;
		let gestureDirection = undefined;
		let gesturePhase = undefined;
		let touchDistance = 0;

		// touch mesurement variables
		let touchStartPointX = undefined;
		let touchStartPointY = undefined;
		let touchDistanceX = undefined;
		let touchDistanceY = undefined;
		let touchEndPointX = undefined;
		let touchEndPointY = undefined;

		// options
		const threshold = theOptions.treshold || 150; //required min distance traveled to be considered swipe
		const allowedTime = theOptions.allowedTime || 200; // maximum time allowed to travel that distance
		const restraint = theOptions.restraint || 100; // maximum distance allowed at the same time in perpendicular direction
		const passiveEventListener = theOptions.passiveEventListener || true;
		const preventScrolling = theOptions.preventScrolling || false; // prevent scrolling when inside target element

		// callback
		const callback = theCallback || function(event, gestureDirection, gesturePhase, gestureType, touchDistance) { };

		// bind events
		element.addEventListener('touchstart', handleTouch, { passive: passiveEventListener });
		element.addEventListener('touchmove', handleTouch, { passive: !preventScrolling });
		element.addEventListener('touchend', handleTouch, { passive: passiveEventListener });
		element.addEventListener('touchcancel', handleTouch, { passive: passiveEventListener });
		element.addEventListener('mousedown', handleTouch, false);


		/* TOUCH HANDLER
		---------------------------------------------*/
		function handleTouch(event) {

			// bind mouse events
			if (event.type === 'mousedown') {
				element.addEventListener('mousemove', handleTouch, false);
				element.addEventListener('mouseup', handleTouch, false);
				d.addEventListener('mouseleave', handleTouch, false);
			}

			// initial touch
			const theTouchObject = event.changedTouches ? event.changedTouches[0] : event;

			// define touch action
			switch (event.type) {
				case "touchstart":
				case "mousedown":
					onTouchStart(event);
					break;
				case "touchmove":
				case "mousemove":
					onTouchMove(event);
					break;
				case "touchend":
				case "mouseup":
					onTouchEnd(event);
					break;
				case "touchcancel":
				case "mouseleave":
					onTouchCancel(event);
					break;
				default:
					return;
			}

			/* TOUCH START HANDLER
			---------------------------------------------*/
			function onTouchStart(event) {

				// touch data
				touchStartPointX = parseInt(theTouchObject.screenX);
				touchStartPointY = parseInt(theTouchObject.screenY);

				// reset
				touchDistanceX = 0;
				touchDistanceY = 0;

				// set state
				gestureDirection = false;
				gesturePhase = 'touchstart';
				gestureType = false;
				touchDistance = 0;

				// record time when finger first makes contact with surface
				startTime = new Date().getTime();

				// fire callback function with params dir="none", phase="start", swipetype="none" etc
				callback(event, gestureDirection, gesturePhase, gestureType, touchDistance);

				// prevent it?
				!passiveEventListener && event.preventDefault();
			}

			/* TOUCH MOVE HANDLER
			---------------------------------------------*/
			function onTouchMove(event) {

				// touch data
				touchDistanceX = parseInt(theTouchObject.screenX) - touchStartPointX;
				touchDistanceY = parseInt(theTouchObject.screenY) - touchStartPointY;

				// set state
				gesturePhase = 'touchmove';
				gestureType = false;

				// horizontal swipe
				if (Math.abs(touchDistanceX) > Math.abs(touchDistanceY)) {
					touchDistance = touchDistanceX;
					gestureDirection = (touchDistance < 0) ? 'left' : 'right'
					callback(event, gestureDirection, gesturePhase, gestureType, touchDistance);
				}

				// vertical swipe
				else {
					touchDistance = touchDistanceY;
					gestureDirection = (touchDistance < 0) ? 'up' : 'down'
					callback(event, gestureDirection, gesturePhase, gestureType, touchDistance);
				}

				// prevent it?
				// prevent scrolling when inside target element
				preventScrolling && event.preventDefault();
			}

			/* TOUCH END HANLER
			---------------------------------------------*/
			function onTouchEnd(event) {

				// touch data
				touchEndPointX = parseInt(theTouchObject.screenX);
				touchEndPointY = parseInt(theTouchObject.screenY);

				// get time elapsed
				elapsedTime = new Date().getTime() - startTime;

				// set state
				gesturePhase = 'touchend';
				gestureType = false;

				// check that elapsed time is within specified,
				// horizontal/vertical dist traveled >= threshold,
				// and horizontal/vertical dist traveled <= restraint

				// console.log(elapsedTime, allowedTime);
				// console.log(gestureDirection, gesturePhase, gestureType, touchDistance);

				// valid gesture
				if (elapsedTime <= allowedTime) {

					// horizontal swipe
					if (Math.abs(touchDistanceX) >= threshold && Math.abs(touchDistanceY) <= restraint) {
						gestureType = (touchDistanceX < 0) ? 'swipeLeft' : 'swipeRight';
						gestureDirection = (touchDistanceX < 0) ? 'left' : 'right';
					}

					// vertical swipe
					if (Math.abs(touchDistanceY) >= threshold && Math.abs(touchDistanceX) <= restraint) {
						gestureType = (touchDistanceY < 0) ? 'swipeUp' : 'swipeDown';
						gestureDirection = (touchDistanceX < 0) ? 'up' : 'down';
					}

					// tap
					if (touchDistanceX === touchDistanceY) {
						gestureType = 'tap';
						gestureDirection = false
					}
				}

				// Fire callback function with params dir="left|right|up|down", phase="end", swipetype=dir etc:
				callback(event, gestureDirection, gesturePhase, gestureType, touchDistance)

				// prevent it?
				!passiveEventListener && event.preventDefault();

				// remove event for mouse
				if (event.type === 'mouseup') {
					element.removeEventListener('mousemove', handleTouch, false);
					element.removeEventListener('mouseup', handleTouch, false);
					d.removeEventListener('mouseleave', handleTouch, false);
				}
			}

			function onTouchCancel(event) {
				// Fire callback function
				callback(event, false, 'touchcancel', false, 0);

				// remove event for mouse
				if (event.type === 'mouseleave') {
					element.removeEventListener('mousemove', handleTouch, false);
					element.removeEventListener('mouseup', handleTouch, false);
					d.removeEventListener('mouseleave', handleTouch, false);
				}

				// prevent it?
				event.preventDefault && event.preventDefault();
			}
		}
	};

	return {
		watch: watch
	};

}(window, window.document));
