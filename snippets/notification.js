const notification = (function(w, d, undefined) {

	"use strict";

	// Notifications API: https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API
	// example: notification.show('De titel', {icon: 'icon.png', body: 'body text content'});

	/*
	notification.show('De titel', { // The title of the notification
		"body": "Did you make a $1,000,000 purchase at Dr. Evil...", // The body string of the notification
		"dir": "auto", // The text direction of the notification - auto, ltr, rtl
		"lang": 'nl-BE', // The language code of the notification
		"icon": "icon.png", // The URL of the image used as an icon of the notification
		"image": "icon.png", // The URL of an image to be displayed as part of the notification
		"requireInteraction": true, // a Boolean indicating that a notification should remain active until the user clicks or dismisses it, rather than closing automatically.
		"vibrate": [200, 100, 200, 100, 200, 100, 400], // Specifies a vibration pattern (ms) for devices with vibration hardware to emit.
		"tag": "request", // The ID of the notification
		"data": "Visitor got the notification", // Returns a structured clone of the notification’s data.
		"badge": "icon.png", // The URL of the image used to represent the notification when there is not enough space to display the notification itself.
	});
	*/

	const state = {
		permissionStatus: undefined
	},
		ask = function(cb) {

			Notification.requestPermission(function(result) {
				switch (result) {
					case 'granted':
						state.permissionStatus = true;
						cb && cb();
						break;
					case 'denied':
						state.permissionStatus = false;
						break;
					default:
						state.permissionStatus = undefined;
						break;
				}
			});
		},
		show = function(title, options) {

			var theOptions = options || false,
				theTitle = title || false;


			/* Browser support
			---------------------------------------------*/
			if (!("Notification" in window)) { return; }

			/* No Title - No Options
			---------------------------------------------*/
			if (!title || !theOptions) { return; }

			/* Denied Permission
			---------------------------------------------*/
			if (state.permissionStatus === false) { return; }

			/* Undefined Permission
			---------------------------------------------*/
			if (state.permissionStatus === undefined) {
				ask(function() {
					show(theTitle, theOptions);
				});
			}

			/* Granted Permission
			---------------------------------------------*/
			if (state.permissionStatus === true) {
				new Notification(theTitle, theOptions);
			}
		};

	return {
		show: show
	}

}(window, window.document));
