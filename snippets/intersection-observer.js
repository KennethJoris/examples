const observer = (function(w, d, undefined) {

	"use strict";

	/**
	 * observer.watch(theElement, theFunction, theOptions);
	 * 
	 * https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API
	 * 
	 * @param {String} theElement Selector for querySelectorAll.
	 * @param {Function} theFunction Callback funtion with entries & observer as arg.
	 * @param {Object} theOptions Options passed to IntersectionObserver (optional).
	 * 
	 * TheOptions keys:
	 * @param {String} theOptions.root IntersectionObserver root element - default: null.
	 * @param {String} theOptions.rootMargin IntersectionObserver rootMargin in pixels - default: '0px'.
	 * @param {Integer or Array} theOptions.threshold IntersectionObserver threshold - default: [0].
	 * -> Integer: Builds an array of thresholds steps - ex. 2 would become [0, 0.5, 1]
	 * -> Array: [0, 0.25, 0.5, 0.75, 1]
	 * 
	 * POLYFILL: https://github.com/w3c/IntersectionObserver/blob/master/polyfill/intersection-observer.js
	 * 
	 * Example:
	 * 
	 	observer.watch('.element', function(entries, observer) {

			entries.forEach(function(entry) {
				var ratio = entry.intersectionRatio;

				if (entry.isIntersecting) {
					entry.target.classList.add('inview');
					entry.target.style.opacity = ratio;
					// observer.unobserve(entry.target);

				} else {
					entry.target.classList.remove('inview');
					entry.target.style.opacity = ratio;
				}
			});

		}, {threshold: 10, rootMargin: '10px 0px', root: null});
	 */

	const watch = function(elements, handleIntersectFunc, options) {

		/* The Variables
		------------------*/
		let options = options || {},
			theElements = elements || undefined,
			selectorType = theElements.charAt(0),
			selectorName = theElements.substring(1),
			theCallback = handleIntersectFunc || undefined,
			theOptions = {
				root: d.querySelector(options.root) || null,
				rootMargin: options.rootMargin || '0px',
				threshold: options.threshold || [0]
			};


		/* Define threshold as steps
		-----------------------------*/
		if (!Array.isArray(theOptions.threshold)) {
			theOptions.threshold = buildThresholdList(parseInt(theOptions.threshold));
		}


		/* Create IntersectionObserver
		-------------------------------*/
		theElements = (selectorType == '#') ? [d.getElementById(selectorName)] : [].slice.call(d.querySelectorAll(theElements));
		createObserver(theElements, theCallback, theOptions);


	},
		createObserver = function(theElements, theCallback, theOptions) {

			/* Init IntersectionObserver with callback
			-------------------------------------------*/
			const observer = new IntersectionObserver(function(entries) {
				theCallback && theCallback(entries, observer);
			}, theOptions);


			/* Observe elements
			-------------------------------*/
			theElements.forEach(function(el) {
				observer.observe(el);
			});

		},
		buildThresholdList = function(steps) {

			/* Initial value
			---------------------*/
			const thresholds = [0];


			/* Add steps
			---------------------*/
			for (var i = 1.0; i <= steps; i++) {
				var ratio = i / steps;
				thresholds.push(ratio);
			}

			return thresholds;

		};

	return {
		watch: watch
	}

}(window, window.document));
