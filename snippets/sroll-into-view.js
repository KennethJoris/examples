const scrollIntoViewHandler = (function(w, d, undefined) {

	'use strict';

	const s = {
		selectors: {
			theHtml: 'html',
			theElements: '.grid__item'
		},
		classes: {
			scrolledInView: 'js_scrolled-in-view'
		},
		options: {
			activeViewportRatio: 0.1,
			showActiveViewportZone: true
		},
		theScrollTimer: null,
		theNewScrollPosition: null,
		theOldScrollPosition: null,
		theActiveViewportZone: null,
		theCurrentElement: null,
		theResizeTimer: null,
		theWindowHeight: null,
		ticking: false

	},
		els = {},
		init = function() {

			// define elements
			els.theElements = document.querySelectorAll(s.selectors.theElements);

			// no elements
			if (!els.theElements) { return; }

			// define rest of elements
			els.theHtml = document.querySelector(s.selectors.theHtml);

			// define settings
			s.theOldScrollPosition = w.pageYOffset;
			s.theWindowHeight = w.innerHeight;
			s.theElementsAmount = els.theElements.length;
			s.theActiveViewportZone = (-1 * s.theWindowHeight) * s.options.activeViewportRatio;

			// show the active viewport zone
			if (s.options.showActiveViewportZone) { showActiveViewportZone(); }

			// bind events
			w.addEventListener('scroll', scrollIt);
			w.addEventListener('resize', resizeIt);
			w.addEventListener('load', scrolledInView);

		},
		resizeIt = function() {

			clearTimeout(s.theResizeTimer);
			s.theResizeTimer = setTimeout(checkResize, 100);

		},
		checkResize = function() {

			// update variables
			s.theWindowHeight = w.innerHeight;

		},
		scrollIt = function() {

			// Get the new scroll position
			s.theNewScrollPosition = w.scrollY;
			requestTick();

		},
		requestTick = function() {

			// Trottle with requestAnimationFrame
			if (!s.ticking) {
				requestAnimationFrame(updateTick);
			}
			s.ticking = true;

		},
		updateTick = function() {

			// reset the tick so we can
			// capture the next onScroll
			s.ticking = false;

			// call in view script
			scrolledInView();

		},
		scrolledInView = function() {

			// Replace the new scroll position
			s.theOldScrollPosition = s.theNewScrollPosition;

			// the elements loop
			for (let i = 0; i < s.theElementsAmount; i++) {

				// get element and offsetTop
				const el = els.theElements[i],
					theOffsetTop = (el.getBoundingClientRect().top - s.theWindowHeight);

				// add / remove class
				theOffsetTop < s.theActiveViewportZone ? el.classList.add(s.classes.scrolledInView) : el.classList.remove(s.classes.scrolledInView);

			}
		},
		showActiveViewportZone = function() {
			// create element and css
			const el = document.createElement('div'),
				css = { bottom: (s.theActiveViewportZone * -1) + 'px', background: '#bada55', position: 'fixed', zIndex: '999999999', height: '0.1rem', left: '0', right: '0', textAlign: 'center' };

			// set textual feedback and set css
			el.innerHTML = '<small style="font-size:10px;background:rgba(186,218,85,0.5);">scrollIntoViewHandler ActiveViewportZone</small>';
			for (let styleDef in css) { el.style[styleDef] = css[styleDef]; }

			// render
			document.body.appendChild(el);
		}

	return {
		init: init
	};

}(window, window.document));
