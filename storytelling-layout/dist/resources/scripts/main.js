"use strict";

(function (w, d, undefined) {
  "use strict";

  var raf = requestAnimationFrame || mozRequestAnimationFrame || webkitRequestAnimationFrame || msRequestAnimationFrame,
      init = function init() {
    w.grid.init();
    w.menu.init();
    w.scrollHandler.init();
    d.body.classList.add('window-is-loaded');
    d.documentElement.classList.remove('no-js');
  }; // when all is loaded


  raf ? raf(function () {
    w.setTimeout(init, 0);
  }) : w.addEventListener('load', init);
})(window, window.document);"use strict";

/*

Js Animate

duration – the total animation time in ms.
timing – the function to calculate animation progress. Gets a time fraction from 0 to 1, returns the animation progress, usually from 0 to 1.
draw – the function to draw the animation.


example:

animateIt({
	duration: 1000,
	timing(timeFraction) {
		return timeFraction;
	},
	draw(progress) {
		elem.style.width = progress * 100 + '%';
	}
});

Different timing functions: https://github.com/Genenenenaam/easing-utils

*/
var animateIt = function animateIt(_ref) {
  var timing = _ref.timing,
      draw = _ref.draw,
      duration = _ref.duration,
      animationStart = _ref.animationStart,
      animationIteration = _ref.animationIteration,
      animationEnd = _ref.animationEnd;
  var start = performance.now();
  /*--------------------------------------------
  - Callback
  ---------------------------------------------*/
  // On start

  animationStart && animationStart();
  /*--------------------------------------------
  - Animation
  ---------------------------------------------*/

  requestAnimationFrame(function animate(time) {
    // timeFraction goes from 0 to 1
    var timeFraction = (time - start) / duration;
    if (timeFraction > 1) timeFraction = 1;
    if (timeFraction < 0) timeFraction = 0; // calculate the current animation state

    var progress = timing(timeFraction);
    draw(progress); // draw it

    if (timeFraction < 1) {
      requestAnimationFrame(animate);
    }
    /*--------------------------------------------
    - Callbacks
    ---------------------------------------------*/
    // On iteration


    if (timeFraction > 0 && timeFraction < 1) {
      animationIteration && animationIteration();
    } // On end


    if (timeFraction >= 1) {
      animationEnd && animationEnd();
    }
  });
};"use strict";

var scrollHandler = function (w, d, $, undefined) {
  'use strict';

  var s = {
    selectors: {
      theHtml: 'html'
    },
    classes: {
      scrollUp: 'js_scrolling-up',
      scrollDown: 'js_scrolling-down',
      scrollDeadZone: 'js_scrolling-deadzone'
    },
    theScrollTimer: null,
    theNewScrollPosition: null,
    theOldScrollPosition: null,
    theScrollDeadzone: null,
    theCurrentScrollDirection: null,
    theCurrentBreakpoint: null,
    ticking: false
  },
      els = {},
      init = function init() {
    // define elements
    els.theHtml = document.querySelector(s.selectors.theHtml); // no elements

    if (!els.theHtml) {
      return;
    } // define settings


    s.theOldScrollPosition = w.pageYOffset;
    s.theScrollDeadzone = 100; // scroll setup

    w.addEventListener('scroll', scrollIt);
  },
      scrollIt = function scrollIt() {
    // Get the new scroll position
    s.theNewScrollPosition = window.scrollY;
    requestTick();
  },
      requestTick = function requestTick() {
    // Trottle with requestAnimationFrame
    if (!s.ticking) {
      requestAnimationFrame(update);
    }

    s.ticking = true;
  },
      update = function update() {
    // reset the tick so we can
    // capture the next onScroll
    s.ticking = false; // Define the action	

    if (s.theNewScrollPosition < s.theOldScrollPosition && s.theNewScrollPosition > s.theScrollDeadzone) {
      // Scrolling up
      els.theHtml.classList.add(s.classes.scrollUp);
      els.theHtml.classList.remove(s.classes.scrollDown);
      els.theHtml.classList.remove(s.classes.scrollDeadZone); // set the current scroll positon

      s.theCurrentScrollDirection = "up";
    } else if (s.theNewScrollPosition > s.theOldScrollPosition && s.theNewScrollPosition > s.theScrollDeadzone) {
      // Scrolling down
      els.theHtml.classList.add(s.classes.scrollDown);
      els.theHtml.classList.remove(s.classes.scrollUp);
      els.theHtml.classList.remove(s.classes.scrollDeadZone); // set the current scroll positon

      s.theCurrentScrollDirection = "down";
    } else if (s.theNewScrollPosition < s.theScrollDeadzone) {
      // Scrolling within deadzone
      els.theHtml.classList.add(s.classes.scrollDeadZone);
      els.theHtml.classList.remove(s.classes.scrollUp);
      els.theHtml.classList.remove(s.classes.scrollDown); // set the current scroll positon

      s.theCurrentScrollDirection = "deadzone";
    } // Replace the new scroll position


    s.theOldScrollPosition = s.theNewScrollPosition; // get the current breakpoint
    // s.theCurrentBreakpoint = w.getComputedStyle(d.querySelector('body'), ':before').getPropertyValue('content').replace(/\"/g, '');
  };

  return {
    init: init
  };
}(window, window.document, window.jQuery);"use strict";

var touchHandler = function (w, d, undefined) {
  'use strict';

  var watch = function watch(touchelement, theOptions, theCallback) {
    /* SETTINGS
    ---------------------------------------------*/
    // touch element
    var element = touchelement; // gesture mesurement variables

    var elapsedTime = undefined;
    var startTime = undefined;
    var gestureType = undefined;
    var gestureDirection = undefined;
    var gesturePhase = undefined;
    var touchDistance = 0; // touch mesurement variables

    var touchStartPointX = undefined;
    var touchStartPointY = undefined;
    var touchDistanceX = undefined;
    var touchDistanceY = undefined;
    var touchEndPointX = undefined;
    var touchEndPointY = undefined; // options

    var threshold = theOptions.treshold || 150; //required min distance traveled to be considered swipe

    var allowedTime = theOptions.allowedTime || 200; // maximum time allowed to travel that distance

    var restraint = theOptions.restraint || 100; // maximum distance allowed at the same time in perpendicular direction

    var passiveEventListener = theOptions.passiveEventListener || true;
    var preventScrolling = theOptions.preventScrolling || false; // prevent scrolling when inside target element
    // callback

    var callback = theCallback || function (event, gestureDirection, gesturePhase, gestureType, touchDistance) {}; // bind events


    element.addEventListener('touchstart', handleTouch, {
      passive: passiveEventListener
    });
    element.addEventListener('touchmove', handleTouch, {
      passive: !preventScrolling
    });
    element.addEventListener('touchend', handleTouch, {
      passive: passiveEventListener
    });
    element.addEventListener('touchcancel', handleTouch, {
      passive: passiveEventListener
    });
    element.addEventListener('mousedown', handleTouch, false);
    /* TOUCH HANDLER
    ---------------------------------------------*/

    function handleTouch(event) {
      // bind mouse events
      if (event.type === 'mousedown') {
        element.addEventListener('mousemove', handleTouch, false);
        element.addEventListener('mouseup', handleTouch, false);
        d.addEventListener('mouseleave', handleTouch, false);
      } // initial touch


      var theTouchObject = event.changedTouches ? event.changedTouches[0] : event; // define touch action

      switch (event.type) {
        case "touchstart":
        case "mousedown":
          onTouchStart(event);
          break;

        case "touchmove":
        case "mousemove":
          onTouchMove(event);
          break;

        case "touchend":
        case "mouseup":
          onTouchEnd(event);
          break;

        case "touchcancel":
        case "mouseleave":
          onTouchCancel(event);
          break;

        default:
          return;
      }
      /* TOUCH START HANDLER
      ---------------------------------------------*/


      function onTouchStart(event) {
        // touch data
        touchStartPointX = parseInt(theTouchObject.screenX);
        touchStartPointY = parseInt(theTouchObject.screenY); // reset

        touchDistanceX = 0;
        touchDistanceY = 0; // set state

        gestureDirection = false;
        gesturePhase = 'touchstart';
        gestureType = false;
        touchDistance = 0; // record time when finger first makes contact with surface

        startTime = new Date().getTime(); // fire callback function with params dir="none", phase="start", swipetype="none" etc

        callback(event, gestureDirection, gesturePhase, gestureType, touchDistance); // prevent it?

        !passiveEventListener && event.preventDefault();
      }
      /* TOUCH MOVE HANDLER
      ---------------------------------------------*/


      function onTouchMove(event) {
        // touch data
        touchDistanceX = parseInt(theTouchObject.screenX) - touchStartPointX;
        touchDistanceY = parseInt(theTouchObject.screenY) - touchStartPointY; // set state

        gesturePhase = 'touchmove';
        gestureType = false; // horizontal swipe

        if (Math.abs(touchDistanceX) > Math.abs(touchDistanceY)) {
          touchDistance = touchDistanceX;
          gestureDirection = touchDistance < 0 ? 'left' : 'right';
          callback(event, gestureDirection, gesturePhase, gestureType, touchDistance);
        } // vertical swipe
        else {
            touchDistance = touchDistanceY;
            gestureDirection = touchDistance < 0 ? 'up' : 'down';
            callback(event, gestureDirection, gesturePhase, gestureType, touchDistance);
          } // prevent it?
        // prevent scrolling when inside target element


        preventScrolling && event.preventDefault();
      }
      /* TOUCH END HANLER
      ---------------------------------------------*/


      function onTouchEnd(event) {
        // touch data
        touchEndPointX = parseInt(theTouchObject.screenX);
        touchEndPointY = parseInt(theTouchObject.screenY); // get time elapsed

        elapsedTime = new Date().getTime() - startTime; // set state

        gesturePhase = 'touchend';
        gestureType = false; // check that elapsed time is within specified,
        // horizontal/vertical dist traveled >= threshold,
        // and horizontal/vertical dist traveled <= restraint
        // console.log(elapsedTime, allowedTime);
        // console.log(gestureDirection, gesturePhase, gestureType, touchDistance);
        // valid gesture

        if (elapsedTime <= allowedTime) {
          // horizontal swipe
          if (Math.abs(touchDistanceX) >= threshold && Math.abs(touchDistanceY) <= restraint) {
            gestureType = touchDistanceX < 0 ? 'swipeLeft' : 'swipeRight';
            gestureDirection = touchDistanceX < 0 ? 'left' : 'right';
          } // vertical swipe


          if (Math.abs(touchDistanceY) >= threshold && Math.abs(touchDistanceX) <= restraint) {
            gestureType = touchDistanceY < 0 ? 'swipeUp' : 'swipeDown';
            gestureDirection = touchDistanceX < 0 ? 'up' : 'down';
          } // tap


          if (touchDistanceX === touchDistanceY) {
            gestureType = 'tap';
            gestureDirection = false;
          }
        } // Fire callback function with params dir="left|right|up|down", phase="end", swipetype=dir etc:


        callback(event, gestureDirection, gesturePhase, gestureType, touchDistance); // prevent it?

        !passiveEventListener && event.preventDefault(); // remove event for mouse

        if (event.type === 'mouseup') {
          element.removeEventListener('mousemove', handleTouch, false);
          element.removeEventListener('mouseup', handleTouch, false);
          d.removeEventListener('mouseleave', handleTouch, false);
        }
      }

      function onTouchCancel(event) {
        // Fire callback function
        callback(event, false, 'touchcancel', false, 0); // remove event for mouse

        if (event.type === 'mouseleave') {
          element.removeEventListener('mousemove', handleTouch, false);
          element.removeEventListener('mouseup', handleTouch, false);
          d.removeEventListener('mouseleave', handleTouch, false);
        } // prevent it?


        event.preventDefault && event.preventDefault();
      }
    }
  };

  return {
    watch: watch
  };
}(window, window.document);"use strict";

/*

Handle destop layout

*/
var grid = function (w, d, undefined) {
  'use strict';

  var s = {
    selectors: {
      theGrid: '.js-grid',
      theGridItems: '.js-grid__item',
      theGridItemSelector: '.js-grid__item-selector',
      theGridItemFocus: '.js-grid__item-focus',
      theGridItemMenu: '.js-grid__item__menu',
      theGridItemNextLink: '.js-grid__item__link__next',
      theBody: 'body'
    },
    classes: {
      isActiveGridItem: 'js-grid__item--active',
      openMenu: 'js-menu--isOpen'
    },
    state: {
      currentGridStyles: null,
      currentPageIndex: 0,
      breakpoint: null,
      shouldAnimate: false,
      eventsDidBind: false,
      allowPageChange: true,
      gridItemsAmount: 0,
      cssGap: 32,
      vw: null,
      vh: null
    },
    grid: {
      itemsPerRow: 3
    }
  },
      els = {},
      init = function init() {
    // define elements
    els.theGrid = d.querySelector(s.selectors.theGrid);
    els.theGridItems = d.querySelectorAll(s.selectors.theGridItems);
    els.theGridItemSelector = d.querySelectorAll(s.selectors.theGridItemSelector);
    els.theBody = d.querySelector(s.selectors.theBody); // set state

    s.state.gridItemsAmount = els.theGridItems.length; // bind events

    w.addEventListener('resize', resizeIt);
    w.addEventListener('popstate', getHistory);
    w.addEventListener('keydown', handleKeyDown); // touch support

    var options = {
      threshold: 100,
      allowedTime: 400
    };
    w.touchHandler.watch(els.theGrid, options, onTouch); // not the fancy layout - buzz of

    checkBreakpoint(); // Set animation value's

    setAnimationValues(); // Scroll to active page on pageload

    getActivePageOnLoad();
  },
      setAnimationValues = function setAnimationValues() {
    // main Grid default position
    els.theGrid.setAttribute('data-top', 0);
    els.theGrid.setAttribute('data-left', 0); // Grid items default positions

    var theRow = 0,
        theCol = 0;

    for (var index = 0; index < s.state.gridItemsAmount; index++) {
      // define values
      var top = theRow;
      var left = theCol; // set values

      els.theGridItems[index].setAttribute('data-top', top);
      els.theGridItems[index].setAttribute('data-left', left); // add count

      theCol++; // increment row / reset col

      if (theCol >= s.grid.itemsPerRow) {
        theRow++;
        theCol = 0;
      }
    }
  },
      resizeIt = function resizeIt() {
    clearTimeout(s.theResizeTimer);
    s.theResizeTimer = setTimeout(checkBreakpoint, 100);
  },
      checkBreakpoint = function checkBreakpoint() {
    // Set state
    s.state.vw = window.innerWidth;
    s.state.vh = window.innerHeight; // Get breakpoint

    s.state.breakpoint = w.getComputedStyle(els.theBody, ':before').getPropertyValue('content').replace(/\"/g, ''); // set shouldAnimate state

    setShouldAnimate(); // init animation events

    setEventListeners();
  },
      setShouldAnimate = function setShouldAnimate() {
    if (s.state.breakpoint === 'xs' || s.state.breakpoint === 'sm' || s.state.breakpoint === 'md') {
      s.state.shouldAnimate = false;
    } else {
      s.state.shouldAnimate = true;
    }
  },
      setEventListeners = function setEventListeners() {
    if (s.state.shouldAnimate && !s.state.eventsDidBind) {
      for (var index = 0; index < s.state.gridItemsAmount; index++) {
        els.theGridItems[index].addEventListener('click', handleGridItemClick);
      }

      s.state.eventsDidBind = true;
    } else if (!s.state.shouldAnimate && s.state.eventsDidBind) {
      for (var _index = 0; _index < s.state.gridItemsAmount; _index++) {
        els.theGridItems[_index].removeEventListener('click', handleGridItemClick);
      }

      s.state.eventsDidBind = false;
    }
  },
      handleGridItemClick = function handleGridItemClick(event) {
    var theTarget = event.target,
        theTargetClasses = theTarget.classList,
        theGridItem = this,
        theGridItemIndex = Array.prototype.indexOf.call(theGridItem.parentElement.children, theGridItem);

    switch (true) {
      // Focus element
      case theTargetClasses.contains(s.selectors.theGridItemFocus.replace('.', '')):
        event.preventDefault();
        handleFocus(theGridItemIndex);
        break;
      // Selector element

      case theTargetClasses.contains(s.selectors.theGridItemSelector.replace('.', '')):
        event.preventDefault();
        handleSelector();
        break;
      // Menu element

      case theTargetClasses.contains(s.selectors.theGridItemMenu.replace('.', '')):
        event.preventDefault();
        handleMenu();
        break;
      // Next link element

      case theTargetClasses.contains(s.selectors.theGridItemNextLink.replace('.', '')):
        event.preventDefault();
        handleNext(theGridItemIndex);
        break;

      default:
        break;
    }
  },
      handleNext = function handleNext(elementIndex) {
    var nextGridItemIndex = elementIndex + 1 >= s.state.gridItemsAmount ? 0 : elementIndex + 1;
    animateGrid(nextGridItemIndex);
  },
      handleFocus = function handleFocus(elementIndex) {
    animateGrid(elementIndex);
  },
      handleSelector = function handleSelector() {
    handleMenu();
  },
      handleMenu = function handleMenu() {
    els.theBody.classList.contains(s.classes.openMenu) ? els.theBody.classList.remove(s.classes.openMenu) : els.theBody.classList.add(s.classes.openMenu);
  },
      handleKeyDown = function handleKeyDown(event) {
    // Do nothing if page change is still animating
    // Do nothing if the we're not animating at all
    // Do nothing if the event was already processed
    if (!s.state.allowPageChange || !s.state.shouldAnimate || event.defaultPrevented) {
      return;
    }

    var nextPage = undefined;
    var action = undefined;

    switch (event.key) {
      case "Down":
      case "ArrowDown":
        action = 3;
        break;

      case "Up":
      case "ArrowUp":
        action = -3;
        break;

      case "Left":
      case "ArrowLeft":
        action = -1;
        break;

      case "Right":
      case "ArrowRight":
        action = 1;
        break;

      default:
        return;
    } // calculate next page index


    if (s.state.currentPageIndex + action < 0) {
      nextPage = s.state.gridItemsAmount + (s.state.currentPageIndex + action);
    } else if (s.state.currentPageIndex + action >= s.state.gridItemsAmount) {
      nextPage = s.state.currentPageIndex + action - s.state.gridItemsAmount;
    } else {
      nextPage = s.state.currentPageIndex + action;
    } // navigate to selected page


    animateGrid(nextPage); // Cancel the default action to avoid it being handled twice

    event.preventDefault && event.preventDefault();
  },
      animateGrid = function animateGrid(elementIndex, excludeFromHistory) {
    // deny pagechange
    s.state.allowPageChange = false; // animate data

    var gridItem = els.theGridItems[elementIndex],
        gap = s.state.cssGap * 2 + 'px',
        toTop = gridItem.getAttribute('data-top'),
        toLeft = gridItem.getAttribute('data-left'),
        fromTop = els.theGrid.getAttribute('data-top'),
        fromLeft = els.theGrid.getAttribute('data-left');
    animateIt({
      duration: Math.min(700, Math.max(450, s.state.vw / 2)),
      timing: function timing(timeFraction) {
        // easeInOutCubic
        return timeFraction < 0.5 ? 4 * timeFraction * timeFraction * timeFraction : (timeFraction - 1) * (2 * timeFraction - 2) * (2 * timeFraction - 2) + 1;
      },
      draw: function draw(progress) {
        // round progress to x decimals
        var progressFloor = Math.floor(progress * 1000) / 1000,
            // calculate animation from and to - current top/left vs target top/left position
        topValue = -fromTop + -(fromTop - toTop) * progressFloor * -1,
            leftValue = -fromLeft + -(fromLeft - toLeft) * progressFloor * -1,
            // animation templates
        animationTemplateTop = "top: calc(".concat(topValue, " * (100vh - ").concat(gap, ") )"),
            animationTemplateLeft = "left: calc(".concat(leftValue, " * (100vw - ").concat(gap, ") )"); // animate it

        els.theGrid.setAttribute('style', animationTemplateTop + '; ' + animationTemplateLeft + '; transition: transform .4s ease-in;'); // save styles

        s.state.currentGridStyles = animationTemplateTop + '; ' + animationTemplateLeft + '; ';
      },
      animationEnd: function animationEnd() {
        // set/remove classes
        for (var index = 0; index < s.state.gridItemsAmount; index++) {
          els.theGridItems[index].classList.remove(s.classes.isActiveGridItem);
        }

        els.theGridItems[elementIndex].classList.add(s.classes.isActiveGridItem); // allow pagechange

        s.state.allowPageChange = true;
      }
    }); // set 'from' position for the next animation

    els.theGrid.setAttribute('data-top', toTop);
    els.theGrid.setAttribute('data-left', toLeft); // setHistory

    !excludeFromHistory && setHistory(elementIndex); // set state

    s.state.currentPageIndex = elementIndex;
  },
      setHistory = function setHistory(pageIndex) {
    history.pushState(pageIndex, null, "?pagina=".concat(pageIndex));
  },
      getHistory = function getHistory() {
    var elementIndex = w.history.state || 0;
    elementIndex ? animateGrid(elementIndex, true) : animateGrid(elementIndex);
  },
      getActivePageOnLoad = function getActivePageOnLoad() {
    if (w.history.state) {
      var elementIndex = w.history.state;
      animateGrid(elementIndex);
    }
  },
      onTouch = function onTouch(event, gestureDirection, gesturePhase, gestureType, touchDistance) {
    // touch move animation
    handleTouchMoveAnimation(gestureDirection, gesturePhase, gestureType, touchDistance); // mobile - handle menu open and close

    handleMenuSwipes(gestureType); // story telling layout - handle swipe gestures

    handleSwipeGestures(gestureType);
  },
      handleTouchMoveAnimation = function handleTouchMoveAnimation(gestureDirection, gesturePhase, gestureType, touchDistance) {
    // variables
    var currentGridStyles = s.state.currentGridStyles || '';
    var axis = 'X'; // set animation on touchmove

    if (s.state.shouldAnimate && gesturePhase === 'touchmove') {
      // define translate axis
      switch (gestureDirection) {
        case 'left':
        case 'right':
          axis = 'X';
          break;

        case 'up':
        case 'down':
          axis = 'Y';
          break;

        default:
          break;
      } // apply translate


      var animationTemplate = "transform: translate".concat(axis, "(").concat(touchDistance / 10, "px)");
      els.theGrid.setAttribute('style', currentGridStyles + animationTemplate);
    } // end animation on touchend or touchcancel - reset translate


    if (s.state.shouldAnimate && (gesturePhase === 'touchend' || gesturePhase === 'touchcancel')) {
      var _animationTemplate = "transform: translate".concat(axis, "(0deg); transition: transform .4s ease-in;");

      els.theGrid.setAttribute('style', currentGridStyles + _animationTemplate);
    }
  },
      handleMenuSwipes = function handleMenuSwipes(gestureType) {
    if (!s.state.shouldAnimate) {
      switch (gestureType) {
        case 'swipeRight':
          els.theBody.classList.add(s.classes.openMenu);
          break;

        case 'swipeLeft':
          els.theBody.classList.remove(s.classes.openMenu);
          break;

        default:
          return;
      }
    }
  },
      handleSwipeGestures = function handleSwipeGestures(gestureType) {
    if (s.state.shouldAnimate && gestureType && gestureType !== 'tap') {
      // simulate keydown event
      var simulatedEvent = {
        key: 'swipeLeft'
      };

      switch (gestureType) {
        case 'swipeUp':
          simulatedEvent.key = 'Down';
          break;

        case 'swipeRight':
          simulatedEvent.key = 'Left';
          break;

        case 'swipeDown':
          simulatedEvent.key = 'Up';
          break;

        case 'swipeLeft':
          simulatedEvent.key = 'Right';
          break;

        default:
          return;
      } // simulate it


      handleKeyDown(simulatedEvent);
    }
  };

  return {
    init: init,
    handleMenu: handleMenu,
    animateGrid: animateGrid
  };
}(window, window.document);"use strict";

/*

Handle Menu events

*/
var menu = function (w, d, undefined) {
  'use strict';

  var s = {
    selectors: {
      theMenu: '.js-menu',
      theMenuTrigger: '.js-button--menu',
      theNavLinks: '.js-navigation__link',
      theBody: 'body'
    },
    state: {
      breakpoint: null,
      breakpointChanged: false,
      isFancyLayout: false,
      menuTriggerAmount: 0,
      linkItemsAmount: 0
    },
    classes: {
      openMenu: 'js-menu--isOpen'
    }
  },
      els = {},
      init = function init() {
    // define elements
    els.theMenu = d.querySelector(s.selectors.theMenu);
    els.theMenuTrigger = d.querySelectorAll(s.selectors.theMenuTrigger);
    els.theNavLinks = d.querySelectorAll(s.selectors.theNavLinks);
    els.theBody = d.querySelector(s.selectors.theBody); // set state

    s.state.menuTriggerAmount = els.theMenuTrigger.length;
    s.state.linkItemsAmount = els.theNavLinks.length; // the bind
    // Toggel menu triggers

    for (var index = 0; index < s.state.menuTriggerAmount; index++) {
      els.theMenuTrigger[index].addEventListener('click', function () {
        w.grid.handleMenu();
      });
    } // Menu link Items


    for (var _index = 0; _index < s.state.linkItemsAmount; _index++) {
      els.theNavLinks[_index].addEventListener('click', handleMenuLinkClick);
    } // touch support


    var options = {
      threshold: 100,
      allowedTime: 400
    };
    w.touchHandler.watch(els.theMenu, options, onTouch); // Resize

    w.addEventListener('resize', resizeIt);
    checkBreakpoint();
  },
      resizeIt = function resizeIt() {
    clearTimeout(s.theResizeTimer);
    s.theResizeTimer = setTimeout(checkBreakpoint, 100);
  },
      checkBreakpoint = function checkBreakpoint() {
    // Get breakpoint
    s.state.breakpoint = w.getComputedStyle(els.theBody, ':before').getPropertyValue('content').replace(/\"/g, ''); // set isFancyLayout state

    if (s.state.breakpoint === 'xs' || s.state.breakpoint === 'sm' || s.state.breakpoint === 'md') {
      s.state.isFancyLayout = false;
    } else {
      s.state.isFancyLayout = true;
    }
  },
      handleMenuLinkClick = function handleMenuLinkClick(event) {
    // variables
    var element = this,
        theTarget = element.getAttribute("href"),
        theGridItem = d.querySelector(theTarget),
        theGridItemIndex = Array.prototype.indexOf.call(theGridItem.parentElement.children, theGridItem); // fancy layout

    if (s.state.isFancyLayout) {
      event.preventDefault();
      w.grid.animateGrid(theGridItemIndex);
    }
  },
      onTouch = function onTouch(event, gestureDirection, gesturePhase, gestureType, touchDistance) {
    if (gestureType === 'swipeLeft') {
      els.theBody.classList.remove(s.classes.openMenu);
    }
  };

  return {
    init: init
  };
}(window, window.document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiLCJhbmltYXRlLmpzIiwic2Nyb2xsaGFuZGxlci5qcyIsInRvdWNoaGFuZGxlci5qcyIsImdyaWQuanMiLCJtZW51LmpzIl0sIm5hbWVzIjpbInciLCJkIiwidW5kZWZpbmVkIiwicmFmIiwicmVxdWVzdEFuaW1hdGlvbkZyYW1lIiwibW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lIiwid2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lIiwibXNSZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJpbml0IiwiZ3JpZCIsIm1lbnUiLCJzY3JvbGxIYW5kbGVyIiwiYm9keSIsImNsYXNzTGlzdCIsImFkZCIsImRvY3VtZW50RWxlbWVudCIsInJlbW92ZSIsInNldFRpbWVvdXQiLCJhZGRFdmVudExpc3RlbmVyIiwid2luZG93IiwiZG9jdW1lbnQiLCJhbmltYXRlSXQiLCJ0aW1pbmciLCJkcmF3IiwiZHVyYXRpb24iLCJhbmltYXRpb25TdGFydCIsImFuaW1hdGlvbkl0ZXJhdGlvbiIsImFuaW1hdGlvbkVuZCIsInN0YXJ0IiwicGVyZm9ybWFuY2UiLCJub3ciLCJhbmltYXRlIiwidGltZSIsInRpbWVGcmFjdGlvbiIsInByb2dyZXNzIiwiJCIsInMiLCJzZWxlY3RvcnMiLCJ0aGVIdG1sIiwiY2xhc3NlcyIsInNjcm9sbFVwIiwic2Nyb2xsRG93biIsInNjcm9sbERlYWRab25lIiwidGhlU2Nyb2xsVGltZXIiLCJ0aGVOZXdTY3JvbGxQb3NpdGlvbiIsInRoZU9sZFNjcm9sbFBvc2l0aW9uIiwidGhlU2Nyb2xsRGVhZHpvbmUiLCJ0aGVDdXJyZW50U2Nyb2xsRGlyZWN0aW9uIiwidGhlQ3VycmVudEJyZWFrcG9pbnQiLCJ0aWNraW5nIiwiZWxzIiwicXVlcnlTZWxlY3RvciIsInBhZ2VZT2Zmc2V0Iiwic2Nyb2xsSXQiLCJzY3JvbGxZIiwicmVxdWVzdFRpY2siLCJ1cGRhdGUiLCJqUXVlcnkiLCJ0b3VjaEhhbmRsZXIiLCJ3YXRjaCIsInRvdWNoZWxlbWVudCIsInRoZU9wdGlvbnMiLCJ0aGVDYWxsYmFjayIsImVsZW1lbnQiLCJlbGFwc2VkVGltZSIsInN0YXJ0VGltZSIsImdlc3R1cmVUeXBlIiwiZ2VzdHVyZURpcmVjdGlvbiIsImdlc3R1cmVQaGFzZSIsInRvdWNoRGlzdGFuY2UiLCJ0b3VjaFN0YXJ0UG9pbnRYIiwidG91Y2hTdGFydFBvaW50WSIsInRvdWNoRGlzdGFuY2VYIiwidG91Y2hEaXN0YW5jZVkiLCJ0b3VjaEVuZFBvaW50WCIsInRvdWNoRW5kUG9pbnRZIiwidGhyZXNob2xkIiwidHJlc2hvbGQiLCJhbGxvd2VkVGltZSIsInJlc3RyYWludCIsInBhc3NpdmVFdmVudExpc3RlbmVyIiwicHJldmVudFNjcm9sbGluZyIsImNhbGxiYWNrIiwiZXZlbnQiLCJoYW5kbGVUb3VjaCIsInBhc3NpdmUiLCJ0eXBlIiwidGhlVG91Y2hPYmplY3QiLCJjaGFuZ2VkVG91Y2hlcyIsIm9uVG91Y2hTdGFydCIsIm9uVG91Y2hNb3ZlIiwib25Ub3VjaEVuZCIsIm9uVG91Y2hDYW5jZWwiLCJwYXJzZUludCIsInNjcmVlblgiLCJzY3JlZW5ZIiwiRGF0ZSIsImdldFRpbWUiLCJwcmV2ZW50RGVmYXVsdCIsIk1hdGgiLCJhYnMiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwidGhlR3JpZCIsInRoZUdyaWRJdGVtcyIsInRoZUdyaWRJdGVtU2VsZWN0b3IiLCJ0aGVHcmlkSXRlbUZvY3VzIiwidGhlR3JpZEl0ZW1NZW51IiwidGhlR3JpZEl0ZW1OZXh0TGluayIsInRoZUJvZHkiLCJpc0FjdGl2ZUdyaWRJdGVtIiwib3Blbk1lbnUiLCJzdGF0ZSIsImN1cnJlbnRHcmlkU3R5bGVzIiwiY3VycmVudFBhZ2VJbmRleCIsImJyZWFrcG9pbnQiLCJzaG91bGRBbmltYXRlIiwiZXZlbnRzRGlkQmluZCIsImFsbG93UGFnZUNoYW5nZSIsImdyaWRJdGVtc0Ftb3VudCIsImNzc0dhcCIsInZ3IiwidmgiLCJpdGVtc1BlclJvdyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJsZW5ndGgiLCJyZXNpemVJdCIsImdldEhpc3RvcnkiLCJoYW5kbGVLZXlEb3duIiwib3B0aW9ucyIsIm9uVG91Y2giLCJjaGVja0JyZWFrcG9pbnQiLCJzZXRBbmltYXRpb25WYWx1ZXMiLCJnZXRBY3RpdmVQYWdlT25Mb2FkIiwic2V0QXR0cmlidXRlIiwidGhlUm93IiwidGhlQ29sIiwiaW5kZXgiLCJ0b3AiLCJsZWZ0IiwiY2xlYXJUaW1lb3V0IiwidGhlUmVzaXplVGltZXIiLCJpbm5lcldpZHRoIiwiaW5uZXJIZWlnaHQiLCJnZXRDb21wdXRlZFN0eWxlIiwiZ2V0UHJvcGVydHlWYWx1ZSIsInJlcGxhY2UiLCJzZXRTaG91bGRBbmltYXRlIiwic2V0RXZlbnRMaXN0ZW5lcnMiLCJoYW5kbGVHcmlkSXRlbUNsaWNrIiwidGhlVGFyZ2V0IiwidGFyZ2V0IiwidGhlVGFyZ2V0Q2xhc3NlcyIsInRoZUdyaWRJdGVtIiwidGhlR3JpZEl0ZW1JbmRleCIsIkFycmF5IiwicHJvdG90eXBlIiwiaW5kZXhPZiIsImNhbGwiLCJwYXJlbnRFbGVtZW50IiwiY2hpbGRyZW4iLCJjb250YWlucyIsImhhbmRsZUZvY3VzIiwiaGFuZGxlU2VsZWN0b3IiLCJoYW5kbGVNZW51IiwiaGFuZGxlTmV4dCIsImVsZW1lbnRJbmRleCIsIm5leHRHcmlkSXRlbUluZGV4IiwiYW5pbWF0ZUdyaWQiLCJkZWZhdWx0UHJldmVudGVkIiwibmV4dFBhZ2UiLCJhY3Rpb24iLCJrZXkiLCJleGNsdWRlRnJvbUhpc3RvcnkiLCJncmlkSXRlbSIsImdhcCIsInRvVG9wIiwiZ2V0QXR0cmlidXRlIiwidG9MZWZ0IiwiZnJvbVRvcCIsImZyb21MZWZ0IiwibWluIiwibWF4IiwicHJvZ3Jlc3NGbG9vciIsImZsb29yIiwidG9wVmFsdWUiLCJsZWZ0VmFsdWUiLCJhbmltYXRpb25UZW1wbGF0ZVRvcCIsImFuaW1hdGlvblRlbXBsYXRlTGVmdCIsInNldEhpc3RvcnkiLCJwYWdlSW5kZXgiLCJoaXN0b3J5IiwicHVzaFN0YXRlIiwiaGFuZGxlVG91Y2hNb3ZlQW5pbWF0aW9uIiwiaGFuZGxlTWVudVN3aXBlcyIsImhhbmRsZVN3aXBlR2VzdHVyZXMiLCJheGlzIiwiYW5pbWF0aW9uVGVtcGxhdGUiLCJzaW11bGF0ZWRFdmVudCIsInRoZU1lbnUiLCJ0aGVNZW51VHJpZ2dlciIsInRoZU5hdkxpbmtzIiwiYnJlYWtwb2ludENoYW5nZWQiLCJpc0ZhbmN5TGF5b3V0IiwibWVudVRyaWdnZXJBbW91bnQiLCJsaW5rSXRlbXNBbW91bnQiLCJoYW5kbGVNZW51TGlua0NsaWNrIl0sIm1hcHBpbmdzIjoiOztBQUFDLFdBQVNBLENBQVQsRUFBWUMsQ0FBWixFQUFlQyxTQUFmLEVBQTBCO0FBRTFCOztBQUVBLE1BQU1DLEdBQUcsR0FBR0MscUJBQXFCLElBQUlDLHdCQUF6QixJQUFxREMsMkJBQXJELElBQW9GQyx1QkFBaEc7QUFBQSxNQUNDQyxJQUFJLEdBQUcsU0FBUEEsSUFBTyxHQUFXO0FBRWpCUixLQUFDLENBQUNTLElBQUYsQ0FBT0QsSUFBUDtBQUNBUixLQUFDLENBQUNVLElBQUYsQ0FBT0YsSUFBUDtBQUNBUixLQUFDLENBQUNXLGFBQUYsQ0FBZ0JILElBQWhCO0FBRUFQLEtBQUMsQ0FBQ1csSUFBRixDQUFPQyxTQUFQLENBQWlCQyxHQUFqQixDQUFxQixrQkFBckI7QUFDQWIsS0FBQyxDQUFDYyxlQUFGLENBQWtCRixTQUFsQixDQUE0QkcsTUFBNUIsQ0FBbUMsT0FBbkM7QUFFQSxHQVZGLENBSjBCLENBZ0IxQjs7O0FBQ0FiLEtBQUcsR0FBR0EsR0FBRyxDQUFDLFlBQVc7QUFBRUgsS0FBQyxDQUFDaUIsVUFBRixDQUFhVCxJQUFiLEVBQW1CLENBQW5CO0FBQXdCLEdBQXRDLENBQU4sR0FBZ0RSLENBQUMsQ0FBQ2tCLGdCQUFGLENBQW1CLE1BQW5CLEVBQTJCVixJQUEzQixDQUFuRDtBQUVBLENBbkJBLEVBbUJDVyxNQW5CRCxFQW1CU0EsTUFBTSxDQUFDQyxRQW5CaEIsQ0FBRCxDOztBQ0FBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkEsSUFBTUMsU0FBUyxHQUFHLFNBQVpBLFNBQVksT0FBZ0Y7QUFBQSxNQUE5RUMsTUFBOEUsUUFBOUVBLE1BQThFO0FBQUEsTUFBdEVDLElBQXNFLFFBQXRFQSxJQUFzRTtBQUFBLE1BQWhFQyxRQUFnRSxRQUFoRUEsUUFBZ0U7QUFBQSxNQUF0REMsY0FBc0QsUUFBdERBLGNBQXNEO0FBQUEsTUFBdENDLGtCQUFzQyxRQUF0Q0Esa0JBQXNDO0FBQUEsTUFBbEJDLFlBQWtCLFFBQWxCQSxZQUFrQjtBQUVqRyxNQUFNQyxLQUFLLEdBQUdDLFdBQVcsQ0FBQ0MsR0FBWixFQUFkO0FBRUE7OztBQUdBOztBQUNBTCxnQkFBYyxJQUFJQSxjQUFjLEVBQWhDO0FBRUE7Ozs7QUFHQXJCLHVCQUFxQixDQUFDLFNBQVMyQixPQUFULENBQWlCQyxJQUFqQixFQUF1QjtBQUM1QztBQUNBLFFBQUlDLFlBQVksR0FBRyxDQUFDRCxJQUFJLEdBQUdKLEtBQVIsSUFBaUJKLFFBQXBDO0FBQ0EsUUFBSVMsWUFBWSxHQUFHLENBQW5CLEVBQXNCQSxZQUFZLEdBQUcsQ0FBZjtBQUN0QixRQUFJQSxZQUFZLEdBQUcsQ0FBbkIsRUFBc0JBLFlBQVksR0FBRyxDQUFmLENBSnNCLENBTTVDOztBQUNBLFFBQU1DLFFBQVEsR0FBR1osTUFBTSxDQUFDVyxZQUFELENBQXZCO0FBRUFWLFFBQUksQ0FBQ1csUUFBRCxDQUFKLENBVDRDLENBUzVCOztBQUVoQixRQUFJRCxZQUFZLEdBQUcsQ0FBbkIsRUFBc0I7QUFDckI3QiwyQkFBcUIsQ0FBQzJCLE9BQUQsQ0FBckI7QUFDQTtBQUVEOzs7QUFHQTs7O0FBQ0EsUUFBSUUsWUFBWSxHQUFHLENBQWYsSUFBb0JBLFlBQVksR0FBRyxDQUF2QyxFQUEwQztBQUN6Q1Asd0JBQWtCLElBQUlBLGtCQUFrQixFQUF4QztBQUNBLEtBckIyQyxDQXVCNUM7OztBQUNBLFFBQUlPLFlBQVksSUFBSSxDQUFwQixFQUF1QjtBQUN0Qk4sa0JBQVksSUFBSUEsWUFBWSxFQUE1QjtBQUNBO0FBRUQsR0E1Qm9CLENBQXJCO0FBNkJBLENBMUNELEM7O0FDekJBLElBQU1oQixhQUFhLEdBQUksVUFBVVgsQ0FBVixFQUFhQyxDQUFiLEVBQWdCa0MsQ0FBaEIsRUFBbUJqQyxTQUFuQixFQUE4QjtBQUVqRDs7QUFFQSxNQUFNa0MsQ0FBQyxHQUFHO0FBQ0ZDLGFBQVMsRUFBRTtBQUNQQyxhQUFPLEVBQUU7QUFERixLQURUO0FBSUZDLFdBQU8sRUFBRTtBQUNMQyxjQUFRLEVBQUUsaUJBREw7QUFFTEMsZ0JBQVUsRUFBRSxtQkFGUDtBQUdMQyxvQkFBYyxFQUFFO0FBSFgsS0FKUDtBQVNGQyxrQkFBYyxFQUFFLElBVGQ7QUFVRkMsd0JBQW9CLEVBQUUsSUFWcEI7QUFXRkMsd0JBQW9CLEVBQUUsSUFYcEI7QUFZRkMscUJBQWlCLEVBQUUsSUFaakI7QUFhRkMsNkJBQXlCLEVBQUUsSUFiekI7QUFjRkMsd0JBQW9CLEVBQUUsSUFkcEI7QUFlRkMsV0FBTyxFQUFFO0FBZlAsR0FBVjtBQUFBLE1Ba0JJQyxHQUFHLEdBQUcsRUFsQlY7QUFBQSxNQW1CSTFDLElBQUksR0FBRyxTQUFQQSxJQUFPLEdBQVk7QUFFZjtBQUNBMEMsT0FBRyxDQUFDWixPQUFKLEdBQWNsQixRQUFRLENBQUMrQixhQUFULENBQXVCZixDQUFDLENBQUNDLFNBQUYsQ0FBWUMsT0FBbkMsQ0FBZCxDQUhlLENBS2Y7O0FBQ0EsUUFBSSxDQUFDWSxHQUFHLENBQUNaLE9BQVQsRUFBa0I7QUFBRTtBQUFTLEtBTmQsQ0FRZjs7O0FBQ0FGLEtBQUMsQ0FBQ1Msb0JBQUYsR0FBeUI3QyxDQUFDLENBQUNvRCxXQUEzQjtBQUNBaEIsS0FBQyxDQUFDVSxpQkFBRixHQUFzQixHQUF0QixDQVZlLENBWWY7O0FBQ0E5QyxLQUFDLENBQUNrQixnQkFBRixDQUFtQixRQUFuQixFQUE2Qm1DLFFBQTdCO0FBRUgsR0FsQ0w7QUFBQSxNQW1DSUEsUUFBUSxHQUFHLFNBQVhBLFFBQVcsR0FBWTtBQUVuQjtBQUNBakIsS0FBQyxDQUFDUSxvQkFBRixHQUF5QnpCLE1BQU0sQ0FBQ21DLE9BQWhDO0FBQ0FDLGVBQVc7QUFFZCxHQXpDTDtBQUFBLE1BMENJQSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxHQUFZO0FBRXRCO0FBQ0EsUUFBSSxDQUFDbkIsQ0FBQyxDQUFDYSxPQUFQLEVBQWdCO0FBQ1o3QywyQkFBcUIsQ0FBQ29ELE1BQUQsQ0FBckI7QUFDSDs7QUFDRHBCLEtBQUMsQ0FBQ2EsT0FBRixHQUFZLElBQVo7QUFFSCxHQWxETDtBQUFBLE1BbURJTyxNQUFNLEdBQUcsU0FBVEEsTUFBUyxHQUFZO0FBRWpCO0FBQ0E7QUFDQXBCLEtBQUMsQ0FBQ2EsT0FBRixHQUFZLEtBQVosQ0FKaUIsQ0FNakI7O0FBQ0EsUUFBSWIsQ0FBQyxDQUFDUSxvQkFBRixHQUF5QlIsQ0FBQyxDQUFDUyxvQkFBM0IsSUFBbURULENBQUMsQ0FBQ1Esb0JBQUYsR0FBeUJSLENBQUMsQ0FBQ1UsaUJBQWxGLEVBQXFHO0FBRWpHO0FBQ0FJLFNBQUcsQ0FBQ1osT0FBSixDQUFZekIsU0FBWixDQUFzQkMsR0FBdEIsQ0FBMEJzQixDQUFDLENBQUNHLE9BQUYsQ0FBVUMsUUFBcEM7QUFDQVUsU0FBRyxDQUFDWixPQUFKLENBQVl6QixTQUFaLENBQXNCRyxNQUF0QixDQUE2Qm9CLENBQUMsQ0FBQ0csT0FBRixDQUFVRSxVQUF2QztBQUNBUyxTQUFHLENBQUNaLE9BQUosQ0FBWXpCLFNBQVosQ0FBc0JHLE1BQXRCLENBQTZCb0IsQ0FBQyxDQUFDRyxPQUFGLENBQVVHLGNBQXZDLEVBTGlHLENBT2pHOztBQUNBTixPQUFDLENBQUNXLHlCQUFGLEdBQThCLElBQTlCO0FBRUgsS0FWRCxNQVVPLElBQUlYLENBQUMsQ0FBQ1Esb0JBQUYsR0FBeUJSLENBQUMsQ0FBQ1Msb0JBQTNCLElBQW1EVCxDQUFDLENBQUNRLG9CQUFGLEdBQXlCUixDQUFDLENBQUNVLGlCQUFsRixFQUFxRztBQUV4RztBQUNBSSxTQUFHLENBQUNaLE9BQUosQ0FBWXpCLFNBQVosQ0FBc0JDLEdBQXRCLENBQTBCc0IsQ0FBQyxDQUFDRyxPQUFGLENBQVVFLFVBQXBDO0FBQ0FTLFNBQUcsQ0FBQ1osT0FBSixDQUFZekIsU0FBWixDQUFzQkcsTUFBdEIsQ0FBNkJvQixDQUFDLENBQUNHLE9BQUYsQ0FBVUMsUUFBdkM7QUFDQVUsU0FBRyxDQUFDWixPQUFKLENBQVl6QixTQUFaLENBQXNCRyxNQUF0QixDQUE2Qm9CLENBQUMsQ0FBQ0csT0FBRixDQUFVRyxjQUF2QyxFQUx3RyxDQU94Rzs7QUFDQU4sT0FBQyxDQUFDVyx5QkFBRixHQUE4QixNQUE5QjtBQUVILEtBVk0sTUFVQSxJQUFJWCxDQUFDLENBQUNRLG9CQUFGLEdBQXlCUixDQUFDLENBQUNVLGlCQUEvQixFQUFrRDtBQUVyRDtBQUNBSSxTQUFHLENBQUNaLE9BQUosQ0FBWXpCLFNBQVosQ0FBc0JDLEdBQXRCLENBQTBCc0IsQ0FBQyxDQUFDRyxPQUFGLENBQVVHLGNBQXBDO0FBQ0FRLFNBQUcsQ0FBQ1osT0FBSixDQUFZekIsU0FBWixDQUFzQkcsTUFBdEIsQ0FBNkJvQixDQUFDLENBQUNHLE9BQUYsQ0FBVUMsUUFBdkM7QUFDQVUsU0FBRyxDQUFDWixPQUFKLENBQVl6QixTQUFaLENBQXNCRyxNQUF0QixDQUE2Qm9CLENBQUMsQ0FBQ0csT0FBRixDQUFVRSxVQUF2QyxFQUxxRCxDQU9yRDs7QUFDQUwsT0FBQyxDQUFDVyx5QkFBRixHQUE4QixVQUE5QjtBQUNILEtBcENnQixDQXNDakI7OztBQUNBWCxLQUFDLENBQUNTLG9CQUFGLEdBQXlCVCxDQUFDLENBQUNRLG9CQUEzQixDQXZDaUIsQ0F5Q2pCO0FBQ0E7QUFFSCxHQS9GTDs7QUFpR0EsU0FBTztBQUNIcEMsUUFBSSxFQUFFQTtBQURILEdBQVA7QUFJSCxDQXpHc0IsQ0F5R3JCVyxNQXpHcUIsRUF5R2JBLE1BQU0sQ0FBQ0MsUUF6R00sRUF5R0lELE1BQU0sQ0FBQ3NDLE1BekdYLENBQXZCLEM7O0FDQUEsSUFBTUMsWUFBWSxHQUFJLFVBQVMxRCxDQUFULEVBQVlDLENBQVosRUFBZUMsU0FBZixFQUEwQjtBQUUvQzs7QUFFQSxNQUFNeUQsS0FBSyxHQUFHLFNBQVJBLEtBQVEsQ0FBU0MsWUFBVCxFQUF1QkMsVUFBdkIsRUFBbUNDLFdBQW5DLEVBQWdEO0FBRTdEOztBQUdBO0FBQ0EsUUFBTUMsT0FBTyxHQUFHSCxZQUFoQixDQU42RCxDQVE3RDs7QUFDQSxRQUFJSSxXQUFXLEdBQUc5RCxTQUFsQjtBQUNBLFFBQUkrRCxTQUFTLEdBQUcvRCxTQUFoQjtBQUNBLFFBQUlnRSxXQUFXLEdBQUdoRSxTQUFsQjtBQUNBLFFBQUlpRSxnQkFBZ0IsR0FBR2pFLFNBQXZCO0FBQ0EsUUFBSWtFLFlBQVksR0FBR2xFLFNBQW5CO0FBQ0EsUUFBSW1FLGFBQWEsR0FBRyxDQUFwQixDQWQ2RCxDQWdCN0Q7O0FBQ0EsUUFBSUMsZ0JBQWdCLEdBQUdwRSxTQUF2QjtBQUNBLFFBQUlxRSxnQkFBZ0IsR0FBR3JFLFNBQXZCO0FBQ0EsUUFBSXNFLGNBQWMsR0FBR3RFLFNBQXJCO0FBQ0EsUUFBSXVFLGNBQWMsR0FBR3ZFLFNBQXJCO0FBQ0EsUUFBSXdFLGNBQWMsR0FBR3hFLFNBQXJCO0FBQ0EsUUFBSXlFLGNBQWMsR0FBR3pFLFNBQXJCLENBdEI2RCxDQXdCN0Q7O0FBQ0EsUUFBTTBFLFNBQVMsR0FBR2YsVUFBVSxDQUFDZ0IsUUFBWCxJQUF1QixHQUF6QyxDQXpCNkQsQ0F5QmY7O0FBQzlDLFFBQU1DLFdBQVcsR0FBR2pCLFVBQVUsQ0FBQ2lCLFdBQVgsSUFBMEIsR0FBOUMsQ0ExQjZELENBMEJWOztBQUNuRCxRQUFNQyxTQUFTLEdBQUdsQixVQUFVLENBQUNrQixTQUFYLElBQXdCLEdBQTFDLENBM0I2RCxDQTJCZDs7QUFDL0MsUUFBTUMsb0JBQW9CLEdBQUduQixVQUFVLENBQUNtQixvQkFBWCxJQUFtQyxJQUFoRTtBQUNBLFFBQU1DLGdCQUFnQixHQUFHcEIsVUFBVSxDQUFDb0IsZ0JBQVgsSUFBK0IsS0FBeEQsQ0E3QjZELENBNkJFO0FBRS9EOztBQUNBLFFBQU1DLFFBQVEsR0FBR3BCLFdBQVcsSUFBSSxVQUFTcUIsS0FBVCxFQUFnQmhCLGdCQUFoQixFQUFrQ0MsWUFBbEMsRUFBZ0RGLFdBQWhELEVBQTZERyxhQUE3RCxFQUE0RSxDQUFHLENBQS9HLENBaEM2RCxDQWtDN0Q7OztBQUNBTixXQUFPLENBQUM3QyxnQkFBUixDQUF5QixZQUF6QixFQUF1Q2tFLFdBQXZDLEVBQW9EO0FBQUVDLGFBQU8sRUFBRUw7QUFBWCxLQUFwRDtBQUNBakIsV0FBTyxDQUFDN0MsZ0JBQVIsQ0FBeUIsV0FBekIsRUFBc0NrRSxXQUF0QyxFQUFtRDtBQUFFQyxhQUFPLEVBQUUsQ0FBQ0o7QUFBWixLQUFuRDtBQUNBbEIsV0FBTyxDQUFDN0MsZ0JBQVIsQ0FBeUIsVUFBekIsRUFBcUNrRSxXQUFyQyxFQUFrRDtBQUFFQyxhQUFPLEVBQUVMO0FBQVgsS0FBbEQ7QUFDQWpCLFdBQU8sQ0FBQzdDLGdCQUFSLENBQXlCLGFBQXpCLEVBQXdDa0UsV0FBeEMsRUFBcUQ7QUFBRUMsYUFBTyxFQUFFTDtBQUFYLEtBQXJEO0FBQ0FqQixXQUFPLENBQUM3QyxnQkFBUixDQUF5QixXQUF6QixFQUFzQ2tFLFdBQXRDLEVBQW1ELEtBQW5EO0FBR0E7OztBQUVBLGFBQVNBLFdBQVQsQ0FBcUJELEtBQXJCLEVBQTRCO0FBRTNCO0FBQ0EsVUFBSUEsS0FBSyxDQUFDRyxJQUFOLEtBQWUsV0FBbkIsRUFBZ0M7QUFDL0J2QixlQUFPLENBQUM3QyxnQkFBUixDQUF5QixXQUF6QixFQUFzQ2tFLFdBQXRDLEVBQW1ELEtBQW5EO0FBQ0FyQixlQUFPLENBQUM3QyxnQkFBUixDQUF5QixTQUF6QixFQUFvQ2tFLFdBQXBDLEVBQWlELEtBQWpEO0FBQ0FuRixTQUFDLENBQUNpQixnQkFBRixDQUFtQixZQUFuQixFQUFpQ2tFLFdBQWpDLEVBQThDLEtBQTlDO0FBQ0EsT0FQMEIsQ0FTM0I7OztBQUNBLFVBQU1HLGNBQWMsR0FBR0osS0FBSyxDQUFDSyxjQUFOLEdBQXVCTCxLQUFLLENBQUNLLGNBQU4sQ0FBcUIsQ0FBckIsQ0FBdkIsR0FBaURMLEtBQXhFLENBVjJCLENBWTNCOztBQUNBLGNBQVFBLEtBQUssQ0FBQ0csSUFBZDtBQUNDLGFBQUssWUFBTDtBQUNBLGFBQUssV0FBTDtBQUNDRyxzQkFBWSxDQUFDTixLQUFELENBQVo7QUFDQTs7QUFDRCxhQUFLLFdBQUw7QUFDQSxhQUFLLFdBQUw7QUFDQ08scUJBQVcsQ0FBQ1AsS0FBRCxDQUFYO0FBQ0E7O0FBQ0QsYUFBSyxVQUFMO0FBQ0EsYUFBSyxTQUFMO0FBQ0NRLG9CQUFVLENBQUNSLEtBQUQsQ0FBVjtBQUNBOztBQUNELGFBQUssYUFBTDtBQUNBLGFBQUssWUFBTDtBQUNDUyx1QkFBYSxDQUFDVCxLQUFELENBQWI7QUFDQTs7QUFDRDtBQUNDO0FBbEJGO0FBcUJBOzs7O0FBRUEsZUFBU00sWUFBVCxDQUFzQk4sS0FBdEIsRUFBNkI7QUFFNUI7QUFDQWIsd0JBQWdCLEdBQUd1QixRQUFRLENBQUNOLGNBQWMsQ0FBQ08sT0FBaEIsQ0FBM0I7QUFDQXZCLHdCQUFnQixHQUFHc0IsUUFBUSxDQUFDTixjQUFjLENBQUNRLE9BQWhCLENBQTNCLENBSjRCLENBTTVCOztBQUNBdkIsc0JBQWMsR0FBRyxDQUFqQjtBQUNBQyxzQkFBYyxHQUFHLENBQWpCLENBUjRCLENBVTVCOztBQUNBTix3QkFBZ0IsR0FBRyxLQUFuQjtBQUNBQyxvQkFBWSxHQUFHLFlBQWY7QUFDQUYsbUJBQVcsR0FBRyxLQUFkO0FBQ0FHLHFCQUFhLEdBQUcsQ0FBaEIsQ0FkNEIsQ0FnQjVCOztBQUNBSixpQkFBUyxHQUFHLElBQUkrQixJQUFKLEdBQVdDLE9BQVgsRUFBWixDQWpCNEIsQ0FtQjVCOztBQUNBZixnQkFBUSxDQUFDQyxLQUFELEVBQVFoQixnQkFBUixFQUEwQkMsWUFBMUIsRUFBd0NGLFdBQXhDLEVBQXFERyxhQUFyRCxDQUFSLENBcEI0QixDQXNCNUI7O0FBQ0EsU0FBQ1csb0JBQUQsSUFBeUJHLEtBQUssQ0FBQ2UsY0FBTixFQUF6QjtBQUNBO0FBRUQ7Ozs7QUFFQSxlQUFTUixXQUFULENBQXFCUCxLQUFyQixFQUE0QjtBQUUzQjtBQUNBWCxzQkFBYyxHQUFHcUIsUUFBUSxDQUFDTixjQUFjLENBQUNPLE9BQWhCLENBQVIsR0FBbUN4QixnQkFBcEQ7QUFDQUcsc0JBQWMsR0FBR29CLFFBQVEsQ0FBQ04sY0FBYyxDQUFDUSxPQUFoQixDQUFSLEdBQW1DeEIsZ0JBQXBELENBSjJCLENBTTNCOztBQUNBSCxvQkFBWSxHQUFHLFdBQWY7QUFDQUYsbUJBQVcsR0FBRyxLQUFkLENBUjJCLENBVTNCOztBQUNBLFlBQUlpQyxJQUFJLENBQUNDLEdBQUwsQ0FBUzVCLGNBQVQsSUFBMkIyQixJQUFJLENBQUNDLEdBQUwsQ0FBUzNCLGNBQVQsQ0FBL0IsRUFBeUQ7QUFDeERKLHVCQUFhLEdBQUdHLGNBQWhCO0FBQ0FMLDBCQUFnQixHQUFJRSxhQUFhLEdBQUcsQ0FBakIsR0FBc0IsTUFBdEIsR0FBK0IsT0FBbEQ7QUFDQWEsa0JBQVEsQ0FBQ0MsS0FBRCxFQUFRaEIsZ0JBQVIsRUFBMEJDLFlBQTFCLEVBQXdDRixXQUF4QyxFQUFxREcsYUFBckQsQ0FBUjtBQUNBLFNBSkQsQ0FNQTtBQU5BLGFBT0s7QUFDSkEseUJBQWEsR0FBR0ksY0FBaEI7QUFDQU4sNEJBQWdCLEdBQUlFLGFBQWEsR0FBRyxDQUFqQixHQUFzQixJQUF0QixHQUE2QixNQUFoRDtBQUNBYSxvQkFBUSxDQUFDQyxLQUFELEVBQVFoQixnQkFBUixFQUEwQkMsWUFBMUIsRUFBd0NGLFdBQXhDLEVBQXFERyxhQUFyRCxDQUFSO0FBQ0EsV0F0QjBCLENBd0IzQjtBQUNBOzs7QUFDQVksd0JBQWdCLElBQUlFLEtBQUssQ0FBQ2UsY0FBTixFQUFwQjtBQUNBO0FBRUQ7Ozs7QUFFQSxlQUFTUCxVQUFULENBQW9CUixLQUFwQixFQUEyQjtBQUUxQjtBQUNBVCxzQkFBYyxHQUFHbUIsUUFBUSxDQUFDTixjQUFjLENBQUNPLE9BQWhCLENBQXpCO0FBQ0FuQixzQkFBYyxHQUFHa0IsUUFBUSxDQUFDTixjQUFjLENBQUNRLE9BQWhCLENBQXpCLENBSjBCLENBTTFCOztBQUNBL0IsbUJBQVcsR0FBRyxJQUFJZ0MsSUFBSixHQUFXQyxPQUFYLEtBQXVCaEMsU0FBckMsQ0FQMEIsQ0FTMUI7O0FBQ0FHLG9CQUFZLEdBQUcsVUFBZjtBQUNBRixtQkFBVyxHQUFHLEtBQWQsQ0FYMEIsQ0FhMUI7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBOztBQUNBLFlBQUlGLFdBQVcsSUFBSWMsV0FBbkIsRUFBZ0M7QUFFL0I7QUFDQSxjQUFJcUIsSUFBSSxDQUFDQyxHQUFMLENBQVM1QixjQUFULEtBQTRCSSxTQUE1QixJQUF5Q3VCLElBQUksQ0FBQ0MsR0FBTCxDQUFTM0IsY0FBVCxLQUE0Qk0sU0FBekUsRUFBb0Y7QUFDbkZiLHVCQUFXLEdBQUlNLGNBQWMsR0FBRyxDQUFsQixHQUF1QixXQUF2QixHQUFxQyxZQUFuRDtBQUNBTCw0QkFBZ0IsR0FBSUssY0FBYyxHQUFHLENBQWxCLEdBQXVCLE1BQXZCLEdBQWdDLE9BQW5EO0FBQ0EsV0FOOEIsQ0FRL0I7OztBQUNBLGNBQUkyQixJQUFJLENBQUNDLEdBQUwsQ0FBUzNCLGNBQVQsS0FBNEJHLFNBQTVCLElBQXlDdUIsSUFBSSxDQUFDQyxHQUFMLENBQVM1QixjQUFULEtBQTRCTyxTQUF6RSxFQUFvRjtBQUNuRmIsdUJBQVcsR0FBSU8sY0FBYyxHQUFHLENBQWxCLEdBQXVCLFNBQXZCLEdBQW1DLFdBQWpEO0FBQ0FOLDRCQUFnQixHQUFJSyxjQUFjLEdBQUcsQ0FBbEIsR0FBdUIsSUFBdkIsR0FBOEIsTUFBakQ7QUFDQSxXQVo4QixDQWMvQjs7O0FBQ0EsY0FBSUEsY0FBYyxLQUFLQyxjQUF2QixFQUF1QztBQUN0Q1AsdUJBQVcsR0FBRyxLQUFkO0FBQ0FDLDRCQUFnQixHQUFHLEtBQW5CO0FBQ0E7QUFDRCxTQXhDeUIsQ0EwQzFCOzs7QUFDQWUsZ0JBQVEsQ0FBQ0MsS0FBRCxFQUFRaEIsZ0JBQVIsRUFBMEJDLFlBQTFCLEVBQXdDRixXQUF4QyxFQUFxREcsYUFBckQsQ0FBUixDQTNDMEIsQ0E2QzFCOztBQUNBLFNBQUNXLG9CQUFELElBQXlCRyxLQUFLLENBQUNlLGNBQU4sRUFBekIsQ0E5QzBCLENBZ0QxQjs7QUFDQSxZQUFJZixLQUFLLENBQUNHLElBQU4sS0FBZSxTQUFuQixFQUE4QjtBQUM3QnZCLGlCQUFPLENBQUNzQyxtQkFBUixDQUE0QixXQUE1QixFQUF5Q2pCLFdBQXpDLEVBQXNELEtBQXREO0FBQ0FyQixpQkFBTyxDQUFDc0MsbUJBQVIsQ0FBNEIsU0FBNUIsRUFBdUNqQixXQUF2QyxFQUFvRCxLQUFwRDtBQUNBbkYsV0FBQyxDQUFDb0csbUJBQUYsQ0FBc0IsWUFBdEIsRUFBb0NqQixXQUFwQyxFQUFpRCxLQUFqRDtBQUNBO0FBQ0Q7O0FBRUQsZUFBU1EsYUFBVCxDQUF1QlQsS0FBdkIsRUFBOEI7QUFDN0I7QUFDQUQsZ0JBQVEsQ0FBQ0MsS0FBRCxFQUFRLEtBQVIsRUFBZSxhQUFmLEVBQThCLEtBQTlCLEVBQXFDLENBQXJDLENBQVIsQ0FGNkIsQ0FJN0I7O0FBQ0EsWUFBSUEsS0FBSyxDQUFDRyxJQUFOLEtBQWUsWUFBbkIsRUFBaUM7QUFDaEN2QixpQkFBTyxDQUFDc0MsbUJBQVIsQ0FBNEIsV0FBNUIsRUFBeUNqQixXQUF6QyxFQUFzRCxLQUF0RDtBQUNBckIsaUJBQU8sQ0FBQ3NDLG1CQUFSLENBQTRCLFNBQTVCLEVBQXVDakIsV0FBdkMsRUFBb0QsS0FBcEQ7QUFDQW5GLFdBQUMsQ0FBQ29HLG1CQUFGLENBQXNCLFlBQXRCLEVBQW9DakIsV0FBcEMsRUFBaUQsS0FBakQ7QUFDQSxTQVQ0QixDQVc3Qjs7O0FBQ0FELGFBQUssQ0FBQ2UsY0FBTixJQUF3QmYsS0FBSyxDQUFDZSxjQUFOLEVBQXhCO0FBQ0E7QUFDRDtBQUNELEdBbE5EOztBQW9OQSxTQUFPO0FBQ052QyxTQUFLLEVBQUVBO0FBREQsR0FBUDtBQUlBLENBNU5xQixDQTROcEJ4QyxNQTVOb0IsRUE0TlpBLE1BQU0sQ0FBQ0MsUUE1TkssQ0FBdEIsQzs7QUNBQTs7Ozs7QUFNQSxJQUFNWCxJQUFJLEdBQUksVUFBU1QsQ0FBVCxFQUFZQyxDQUFaLEVBQWVDLFNBQWYsRUFBMEI7QUFFdkM7O0FBRUEsTUFBTWtDLENBQUMsR0FBRztBQUNUQyxhQUFTLEVBQUU7QUFDVmlFLGFBQU8sRUFBRSxVQURDO0FBRVZDLGtCQUFZLEVBQUUsZ0JBRko7QUFHVkMseUJBQW1CLEVBQUUseUJBSFg7QUFJVkMsc0JBQWdCLEVBQUUsc0JBSlI7QUFLVkMscUJBQWUsRUFBRSxzQkFMUDtBQU1WQyx5QkFBbUIsRUFBRSw0QkFOWDtBQU9WQyxhQUFPLEVBQUU7QUFQQyxLQURGO0FBVVRyRSxXQUFPLEVBQUU7QUFDUnNFLHNCQUFnQixFQUFFLHVCQURWO0FBRVJDLGNBQVEsRUFBRTtBQUZGLEtBVkE7QUFjVEMsU0FBSyxFQUFFO0FBQ05DLHVCQUFpQixFQUFFLElBRGI7QUFFTkMsc0JBQWdCLEVBQUUsQ0FGWjtBQUdOQyxnQkFBVSxFQUFFLElBSE47QUFJTkMsbUJBQWEsRUFBRSxLQUpUO0FBS05DLG1CQUFhLEVBQUUsS0FMVDtBQU1OQyxxQkFBZSxFQUFFLElBTlg7QUFPTkMscUJBQWUsRUFBRSxDQVBYO0FBUU5DLFlBQU0sRUFBRSxFQVJGO0FBU05DLFFBQUUsRUFBRSxJQVRFO0FBVU5DLFFBQUUsRUFBRTtBQVZFLEtBZEU7QUEwQlRoSCxRQUFJLEVBQUU7QUFDTGlILGlCQUFXLEVBQUU7QUFEUjtBQTFCRyxHQUFWO0FBQUEsTUE4QkN4RSxHQUFHLEdBQUcsRUE5QlA7QUFBQSxNQStCQzFDLElBQUksR0FBRyxTQUFQQSxJQUFPLEdBQVc7QUFFakI7QUFDQTBDLE9BQUcsQ0FBQ29ELE9BQUosR0FBY3JHLENBQUMsQ0FBQ2tELGFBQUYsQ0FBZ0JmLENBQUMsQ0FBQ0MsU0FBRixDQUFZaUUsT0FBNUIsQ0FBZDtBQUNBcEQsT0FBRyxDQUFDcUQsWUFBSixHQUFtQnRHLENBQUMsQ0FBQzBILGdCQUFGLENBQW1CdkYsQ0FBQyxDQUFDQyxTQUFGLENBQVlrRSxZQUEvQixDQUFuQjtBQUNBckQsT0FBRyxDQUFDc0QsbUJBQUosR0FBMEJ2RyxDQUFDLENBQUMwSCxnQkFBRixDQUFtQnZGLENBQUMsQ0FBQ0MsU0FBRixDQUFZbUUsbUJBQS9CLENBQTFCO0FBQ0F0RCxPQUFHLENBQUMwRCxPQUFKLEdBQWMzRyxDQUFDLENBQUNrRCxhQUFGLENBQWdCZixDQUFDLENBQUNDLFNBQUYsQ0FBWXVFLE9BQTVCLENBQWQsQ0FOaUIsQ0FRakI7O0FBQ0F4RSxLQUFDLENBQUMyRSxLQUFGLENBQVFPLGVBQVIsR0FBMEJwRSxHQUFHLENBQUNxRCxZQUFKLENBQWlCcUIsTUFBM0MsQ0FUaUIsQ0FXakI7O0FBQ0E1SCxLQUFDLENBQUNrQixnQkFBRixDQUFtQixRQUFuQixFQUE2QjJHLFFBQTdCO0FBQ0E3SCxLQUFDLENBQUNrQixnQkFBRixDQUFtQixVQUFuQixFQUErQjRHLFVBQS9CO0FBQ0E5SCxLQUFDLENBQUNrQixnQkFBRixDQUFtQixTQUFuQixFQUE4QjZHLGFBQTlCLEVBZGlCLENBZ0JqQjs7QUFDQSxRQUFNQyxPQUFPLEdBQUc7QUFBRXBELGVBQVMsRUFBRSxHQUFiO0FBQWtCRSxpQkFBVyxFQUFFO0FBQS9CLEtBQWhCO0FBQ0E5RSxLQUFDLENBQUMwRCxZQUFGLENBQWVDLEtBQWYsQ0FBcUJULEdBQUcsQ0FBQ29ELE9BQXpCLEVBQWtDMEIsT0FBbEMsRUFBMkNDLE9BQTNDLEVBbEJpQixDQW9CakI7O0FBQ0FDLG1CQUFlLEdBckJFLENBdUJqQjs7QUFDQUMsc0JBQWtCLEdBeEJELENBMEJqQjs7QUFDQUMsdUJBQW1CO0FBRW5CLEdBNURGO0FBQUEsTUE2RENELGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsR0FBVztBQUUvQjtBQUNBakYsT0FBRyxDQUFDb0QsT0FBSixDQUFZK0IsWUFBWixDQUF5QixVQUF6QixFQUFxQyxDQUFyQztBQUNBbkYsT0FBRyxDQUFDb0QsT0FBSixDQUFZK0IsWUFBWixDQUF5QixXQUF6QixFQUFzQyxDQUF0QyxFQUorQixDQU0vQjs7QUFDQSxRQUFJQyxNQUFNLEdBQUcsQ0FBYjtBQUFBLFFBQ0NDLE1BQU0sR0FBRyxDQURWOztBQUdBLFNBQUssSUFBSUMsS0FBSyxHQUFHLENBQWpCLEVBQW9CQSxLQUFLLEdBQUdwRyxDQUFDLENBQUMyRSxLQUFGLENBQVFPLGVBQXBDLEVBQXFEa0IsS0FBSyxFQUExRCxFQUE4RDtBQUM3RDtBQUNBLFVBQUlDLEdBQUcsR0FBR0gsTUFBVjtBQUNBLFVBQUlJLElBQUksR0FBR0gsTUFBWCxDQUg2RCxDQUs3RDs7QUFDQXJGLFNBQUcsQ0FBQ3FELFlBQUosQ0FBaUJpQyxLQUFqQixFQUF3QkgsWUFBeEIsQ0FBcUMsVUFBckMsRUFBaURJLEdBQWpEO0FBQ0F2RixTQUFHLENBQUNxRCxZQUFKLENBQWlCaUMsS0FBakIsRUFBd0JILFlBQXhCLENBQXFDLFdBQXJDLEVBQWtESyxJQUFsRCxFQVA2RCxDQVM3RDs7QUFDQUgsWUFBTSxHQVZ1RCxDQVc3RDs7QUFDQSxVQUFJQSxNQUFNLElBQUluRyxDQUFDLENBQUMzQixJQUFGLENBQU9pSCxXQUFyQixFQUFrQztBQUFFWSxjQUFNO0FBQUlDLGNBQU0sR0FBRyxDQUFUO0FBQWE7QUFDM0Q7QUFDRCxHQXJGRjtBQUFBLE1Bc0ZDVixRQUFRLEdBQUcsU0FBWEEsUUFBVyxHQUFXO0FBRXJCYyxnQkFBWSxDQUFDdkcsQ0FBQyxDQUFDd0csY0FBSCxDQUFaO0FBQ0F4RyxLQUFDLENBQUN3RyxjQUFGLEdBQW1CM0gsVUFBVSxDQUFDaUgsZUFBRCxFQUFrQixHQUFsQixDQUE3QjtBQUVBLEdBM0ZGO0FBQUEsTUE0RkNBLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsR0FBVztBQUU1QjtBQUNBOUYsS0FBQyxDQUFDMkUsS0FBRixDQUFRUyxFQUFSLEdBQWFyRyxNQUFNLENBQUMwSCxVQUFwQjtBQUNBekcsS0FBQyxDQUFDMkUsS0FBRixDQUFRVSxFQUFSLEdBQWF0RyxNQUFNLENBQUMySCxXQUFwQixDQUo0QixDQU01Qjs7QUFDQTFHLEtBQUMsQ0FBQzJFLEtBQUYsQ0FBUUcsVUFBUixHQUFxQmxILENBQUMsQ0FBQytJLGdCQUFGLENBQW1CN0YsR0FBRyxDQUFDMEQsT0FBdkIsRUFBZ0MsU0FBaEMsRUFBMkNvQyxnQkFBM0MsQ0FBNEQsU0FBNUQsRUFBdUVDLE9BQXZFLENBQStFLEtBQS9FLEVBQXNGLEVBQXRGLENBQXJCLENBUDRCLENBUzVCOztBQUNBQyxvQkFBZ0IsR0FWWSxDQVk1Qjs7QUFDQUMscUJBQWlCO0FBRWpCLEdBM0dGO0FBQUEsTUE0R0NELGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FBVztBQUU3QixRQUFJOUcsQ0FBQyxDQUFDMkUsS0FBRixDQUFRRyxVQUFSLEtBQXVCLElBQXZCLElBQStCOUUsQ0FBQyxDQUFDMkUsS0FBRixDQUFRRyxVQUFSLEtBQXVCLElBQXRELElBQThEOUUsQ0FBQyxDQUFDMkUsS0FBRixDQUFRRyxVQUFSLEtBQXVCLElBQXpGLEVBQStGO0FBQzlGOUUsT0FBQyxDQUFDMkUsS0FBRixDQUFRSSxhQUFSLEdBQXdCLEtBQXhCO0FBQ0EsS0FGRCxNQUVPO0FBQ04vRSxPQUFDLENBQUMyRSxLQUFGLENBQVFJLGFBQVIsR0FBd0IsSUFBeEI7QUFDQTtBQUVELEdBcEhGO0FBQUEsTUFxSENnQyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLEdBQVc7QUFFOUIsUUFBSS9HLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUUksYUFBUixJQUF5QixDQUFDL0UsQ0FBQyxDQUFDMkUsS0FBRixDQUFRSyxhQUF0QyxFQUFxRDtBQUNwRCxXQUFLLElBQUlvQixLQUFLLEdBQUcsQ0FBakIsRUFBb0JBLEtBQUssR0FBR3BHLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUU8sZUFBcEMsRUFBcURrQixLQUFLLEVBQTFELEVBQThEO0FBQzdEdEYsV0FBRyxDQUFDcUQsWUFBSixDQUFpQmlDLEtBQWpCLEVBQXdCdEgsZ0JBQXhCLENBQXlDLE9BQXpDLEVBQWtEa0ksbUJBQWxEO0FBQ0E7O0FBQ0RoSCxPQUFDLENBQUMyRSxLQUFGLENBQVFLLGFBQVIsR0FBd0IsSUFBeEI7QUFFQSxLQU5ELE1BTU8sSUFBSSxDQUFDaEYsQ0FBQyxDQUFDMkUsS0FBRixDQUFRSSxhQUFULElBQTBCL0UsQ0FBQyxDQUFDMkUsS0FBRixDQUFRSyxhQUF0QyxFQUFxRDtBQUMzRCxXQUFLLElBQUlvQixNQUFLLEdBQUcsQ0FBakIsRUFBb0JBLE1BQUssR0FBR3BHLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUU8sZUFBcEMsRUFBcURrQixNQUFLLEVBQTFELEVBQThEO0FBQzdEdEYsV0FBRyxDQUFDcUQsWUFBSixDQUFpQmlDLE1BQWpCLEVBQXdCbkMsbUJBQXhCLENBQTRDLE9BQTVDLEVBQXFEK0MsbUJBQXJEO0FBQ0E7O0FBQ0RoSCxPQUFDLENBQUMyRSxLQUFGLENBQVFLLGFBQVIsR0FBd0IsS0FBeEI7QUFDQTtBQUVELEdBcElGO0FBQUEsTUFxSUNnQyxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQVNqRSxLQUFULEVBQWdCO0FBRXJDLFFBQUlrRSxTQUFTLEdBQUdsRSxLQUFLLENBQUNtRSxNQUF0QjtBQUFBLFFBQ0NDLGdCQUFnQixHQUFHRixTQUFTLENBQUN4SSxTQUQ5QjtBQUFBLFFBRUMySSxXQUFXLEdBQUcsSUFGZjtBQUFBLFFBR0NDLGdCQUFnQixHQUFHQyxLQUFLLENBQUNDLFNBQU4sQ0FBZ0JDLE9BQWhCLENBQXdCQyxJQUF4QixDQUE2QkwsV0FBVyxDQUFDTSxhQUFaLENBQTBCQyxRQUF2RCxFQUFpRVAsV0FBakUsQ0FIcEI7O0FBS0EsWUFBUSxJQUFSO0FBQ0M7QUFDQSxXQUFLRCxnQkFBZ0IsQ0FBQ1MsUUFBakIsQ0FBMEI1SCxDQUFDLENBQUNDLFNBQUYsQ0FBWW9FLGdCQUFaLENBQTZCd0MsT0FBN0IsQ0FBcUMsR0FBckMsRUFBMEMsRUFBMUMsQ0FBMUIsQ0FBTDtBQUNDOUQsYUFBSyxDQUFDZSxjQUFOO0FBQ0ErRCxtQkFBVyxDQUFDUixnQkFBRCxDQUFYO0FBQ0E7QUFDRDs7QUFDQSxXQUFLRixnQkFBZ0IsQ0FBQ1MsUUFBakIsQ0FBMEI1SCxDQUFDLENBQUNDLFNBQUYsQ0FBWW1FLG1CQUFaLENBQWdDeUMsT0FBaEMsQ0FBd0MsR0FBeEMsRUFBNkMsRUFBN0MsQ0FBMUIsQ0FBTDtBQUNDOUQsYUFBSyxDQUFDZSxjQUFOO0FBQ0FnRSxzQkFBYztBQUNkO0FBQ0Q7O0FBQ0EsV0FBS1gsZ0JBQWdCLENBQUNTLFFBQWpCLENBQTBCNUgsQ0FBQyxDQUFDQyxTQUFGLENBQVlxRSxlQUFaLENBQTRCdUMsT0FBNUIsQ0FBb0MsR0FBcEMsRUFBeUMsRUFBekMsQ0FBMUIsQ0FBTDtBQUNDOUQsYUFBSyxDQUFDZSxjQUFOO0FBQ0FpRSxrQkFBVTtBQUNWO0FBQ0Q7O0FBQ0EsV0FBS1osZ0JBQWdCLENBQUNTLFFBQWpCLENBQTBCNUgsQ0FBQyxDQUFDQyxTQUFGLENBQVlzRSxtQkFBWixDQUFnQ3NDLE9BQWhDLENBQXdDLEdBQXhDLEVBQTZDLEVBQTdDLENBQTFCLENBQUw7QUFDQzlELGFBQUssQ0FBQ2UsY0FBTjtBQUNBa0Usa0JBQVUsQ0FBQ1gsZ0JBQUQsQ0FBVjtBQUNBOztBQUNEO0FBQ0M7QUF0QkY7QUF5QkEsR0FyS0Y7QUFBQSxNQXNLQ1csVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBU0MsWUFBVCxFQUF1QjtBQUVuQyxRQUFJQyxpQkFBaUIsR0FBS0QsWUFBWSxHQUFHLENBQWhCLElBQXNCakksQ0FBQyxDQUFDMkUsS0FBRixDQUFRTyxlQUEvQixHQUFrRCxDQUFsRCxHQUFzRCtDLFlBQVksR0FBRyxDQUE3RjtBQUNBRSxlQUFXLENBQUNELGlCQUFELENBQVg7QUFFQSxHQTNLRjtBQUFBLE1BNEtDTCxXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFTSSxZQUFULEVBQXVCO0FBRXBDRSxlQUFXLENBQUNGLFlBQUQsQ0FBWDtBQUVBLEdBaExGO0FBQUEsTUFpTENILGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsR0FBVztBQUUzQkMsY0FBVTtBQUVWLEdBckxGO0FBQUEsTUFzTENBLFVBQVUsR0FBRyxTQUFiQSxVQUFhLEdBQVc7QUFFdkJqSCxPQUFHLENBQUMwRCxPQUFKLENBQVkvRixTQUFaLENBQXNCbUosUUFBdEIsQ0FBK0I1SCxDQUFDLENBQUNHLE9BQUYsQ0FBVXVFLFFBQXpDLElBQ0c1RCxHQUFHLENBQUMwRCxPQUFKLENBQVkvRixTQUFaLENBQXNCRyxNQUF0QixDQUE2Qm9CLENBQUMsQ0FBQ0csT0FBRixDQUFVdUUsUUFBdkMsQ0FESCxHQUVHNUQsR0FBRyxDQUFDMEQsT0FBSixDQUFZL0YsU0FBWixDQUFzQkMsR0FBdEIsQ0FBMEJzQixDQUFDLENBQUNHLE9BQUYsQ0FBVXVFLFFBQXBDLENBRkg7QUFJQSxHQTVMRjtBQUFBLE1BNkxDaUIsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFTNUMsS0FBVCxFQUFnQjtBQUUvQjtBQUNBO0FBQ0E7QUFDQSxRQUFJLENBQUMvQyxDQUFDLENBQUMyRSxLQUFGLENBQVFNLGVBQVQsSUFBNEIsQ0FBQ2pGLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUUksYUFBckMsSUFBc0RoQyxLQUFLLENBQUNxRixnQkFBaEUsRUFBa0Y7QUFBRTtBQUFTOztBQUU3RixRQUFJQyxRQUFRLEdBQUd2SyxTQUFmO0FBQ0EsUUFBSXdLLE1BQU0sR0FBR3hLLFNBQWI7O0FBRUEsWUFBUWlGLEtBQUssQ0FBQ3dGLEdBQWQ7QUFDQyxXQUFLLE1BQUw7QUFDQSxXQUFLLFdBQUw7QUFDQ0QsY0FBTSxHQUFHLENBQVQ7QUFDQTs7QUFDRCxXQUFLLElBQUw7QUFDQSxXQUFLLFNBQUw7QUFDQ0EsY0FBTSxHQUFHLENBQUMsQ0FBVjtBQUNBOztBQUNELFdBQUssTUFBTDtBQUNBLFdBQUssV0FBTDtBQUNDQSxjQUFNLEdBQUcsQ0FBQyxDQUFWO0FBQ0E7O0FBQ0QsV0FBSyxPQUFMO0FBQ0EsV0FBSyxZQUFMO0FBQ0NBLGNBQU0sR0FBRyxDQUFUO0FBQ0E7O0FBQ0Q7QUFDQztBQWxCRixLQVYrQixDQStCL0I7OztBQUNBLFFBQUl0SSxDQUFDLENBQUMyRSxLQUFGLENBQVFFLGdCQUFSLEdBQTJCeUQsTUFBM0IsR0FBb0MsQ0FBeEMsRUFBMkM7QUFDMUNELGNBQVEsR0FBR3JJLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUU8sZUFBUixJQUEyQmxGLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUUUsZ0JBQVIsR0FBMkJ5RCxNQUF0RCxDQUFYO0FBQ0EsS0FGRCxNQUVPLElBQUl0SSxDQUFDLENBQUMyRSxLQUFGLENBQVFFLGdCQUFSLEdBQTJCeUQsTUFBM0IsSUFBcUN0SSxDQUFDLENBQUMyRSxLQUFGLENBQVFPLGVBQWpELEVBQWtFO0FBQ3hFbUQsY0FBUSxHQUFJckksQ0FBQyxDQUFDMkUsS0FBRixDQUFRRSxnQkFBUixHQUEyQnlELE1BQTVCLEdBQXNDdEksQ0FBQyxDQUFDMkUsS0FBRixDQUFRTyxlQUF6RDtBQUNBLEtBRk0sTUFFQTtBQUNObUQsY0FBUSxHQUFHckksQ0FBQyxDQUFDMkUsS0FBRixDQUFRRSxnQkFBUixHQUEyQnlELE1BQXRDO0FBQ0EsS0F0QzhCLENBd0MvQjs7O0FBQ0FILGVBQVcsQ0FBQ0UsUUFBRCxDQUFYLENBekMrQixDQTJDL0I7O0FBQ0F0RixTQUFLLENBQUNlLGNBQU4sSUFBd0JmLEtBQUssQ0FBQ2UsY0FBTixFQUF4QjtBQUVBLEdBM09GO0FBQUEsTUE0T0NxRSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFTRixZQUFULEVBQXVCTyxrQkFBdkIsRUFBMkM7QUFFeEQ7QUFDQXhJLEtBQUMsQ0FBQzJFLEtBQUYsQ0FBUU0sZUFBUixHQUEwQixLQUExQixDQUh3RCxDQUt4RDs7QUFDQSxRQUFJd0QsUUFBUSxHQUFHM0gsR0FBRyxDQUFDcUQsWUFBSixDQUFpQjhELFlBQWpCLENBQWY7QUFBQSxRQUNDUyxHQUFHLEdBQUkxSSxDQUFDLENBQUMyRSxLQUFGLENBQVFRLE1BQVIsR0FBaUIsQ0FBbEIsR0FBdUIsSUFEOUI7QUFBQSxRQUdDd0QsS0FBSyxHQUFHRixRQUFRLENBQUNHLFlBQVQsQ0FBc0IsVUFBdEIsQ0FIVDtBQUFBLFFBSUNDLE1BQU0sR0FBR0osUUFBUSxDQUFDRyxZQUFULENBQXNCLFdBQXRCLENBSlY7QUFBQSxRQUtDRSxPQUFPLEdBQUdoSSxHQUFHLENBQUNvRCxPQUFKLENBQVkwRSxZQUFaLENBQXlCLFVBQXpCLENBTFg7QUFBQSxRQU1DRyxRQUFRLEdBQUdqSSxHQUFHLENBQUNvRCxPQUFKLENBQVkwRSxZQUFaLENBQXlCLFdBQXpCLENBTlo7QUFRQTNKLGFBQVMsQ0FBQztBQUNURyxjQUFRLEVBQUUyRSxJQUFJLENBQUNpRixHQUFMLENBQVMsR0FBVCxFQUFjakYsSUFBSSxDQUFDa0YsR0FBTCxDQUFTLEdBQVQsRUFBY2pKLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUVMsRUFBUixHQUFhLENBQTNCLENBQWQsQ0FERDtBQUVUbEcsWUFGUyxrQkFFRlcsWUFGRSxFQUVZO0FBQ3BCO0FBQ0EsZUFBT0EsWUFBWSxHQUFHLEdBQWYsR0FBcUIsSUFBSUEsWUFBSixHQUFtQkEsWUFBbkIsR0FBa0NBLFlBQXZELEdBQXNFLENBQUNBLFlBQVksR0FBRyxDQUFoQixLQUFzQixJQUFJQSxZQUFKLEdBQW1CLENBQXpDLEtBQStDLElBQUlBLFlBQUosR0FBbUIsQ0FBbEUsSUFBdUUsQ0FBcEo7QUFDQSxPQUxRO0FBTVRWLFVBTlMsZ0JBTUpXLFFBTkksRUFNTTtBQUVkO0FBQ0EsWUFBSW9KLGFBQWEsR0FBR25GLElBQUksQ0FBQ29GLEtBQUwsQ0FBV3JKLFFBQVEsR0FBRyxJQUF0QixJQUE4QixJQUFsRDtBQUFBLFlBRUM7QUFDQXNKLGdCQUFRLEdBQUksQ0FBQ04sT0FBRixHQUFjLEVBQUVBLE9BQU8sR0FBR0gsS0FBWixJQUFxQk8sYUFBdEIsR0FBdUMsQ0FBQyxDQUhqRTtBQUFBLFlBSUNHLFNBQVMsR0FBSSxDQUFDTixRQUFGLEdBQWUsRUFBRUEsUUFBUSxHQUFHRixNQUFiLElBQXVCSyxhQUF4QixHQUF5QyxDQUFDLENBSnJFO0FBQUEsWUFNQztBQUNBSSw0QkFBb0IsdUJBQWdCRixRQUFoQix5QkFBdUNWLEdBQXZDLFFBUHJCO0FBQUEsWUFRQ2EscUJBQXFCLHdCQUFpQkYsU0FBakIseUJBQXlDWCxHQUF6QyxRQVJ0QixDQUhjLENBYWQ7O0FBQ0E1SCxXQUFHLENBQUNvRCxPQUFKLENBQVkrQixZQUFaLENBQXlCLE9BQXpCLEVBQWtDcUQsb0JBQW9CLEdBQUcsSUFBdkIsR0FBOEJDLHFCQUE5QixHQUFzRCxzQ0FBeEYsRUFkYyxDQWdCZDs7QUFDQXZKLFNBQUMsQ0FBQzJFLEtBQUYsQ0FBUUMsaUJBQVIsR0FBNEIwRSxvQkFBb0IsR0FBRyxJQUF2QixHQUE4QkMscUJBQTlCLEdBQXNELElBQWxGO0FBQ0EsT0F4QlE7QUF5QlRoSyxrQkF6QlMsMEJBeUJNO0FBQ2Q7QUFDQSxhQUFLLElBQUk2RyxLQUFLLEdBQUcsQ0FBakIsRUFBb0JBLEtBQUssR0FBR3BHLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUU8sZUFBcEMsRUFBcURrQixLQUFLLEVBQTFELEVBQThEO0FBQUV0RixhQUFHLENBQUNxRCxZQUFKLENBQWlCaUMsS0FBakIsRUFBd0IzSCxTQUF4QixDQUFrQ0csTUFBbEMsQ0FBeUNvQixDQUFDLENBQUNHLE9BQUYsQ0FBVXNFLGdCQUFuRDtBQUF1RTs7QUFDdkkzRCxXQUFHLENBQUNxRCxZQUFKLENBQWlCOEQsWUFBakIsRUFBK0J4SixTQUEvQixDQUF5Q0MsR0FBekMsQ0FBNkNzQixDQUFDLENBQUNHLE9BQUYsQ0FBVXNFLGdCQUF2RCxFQUhjLENBS2Q7O0FBQ0F6RSxTQUFDLENBQUMyRSxLQUFGLENBQVFNLGVBQVIsR0FBMEIsSUFBMUI7QUFDQTtBQWhDUSxLQUFELENBQVQsQ0Fkd0QsQ0FpRHhEOztBQUNBbkUsT0FBRyxDQUFDb0QsT0FBSixDQUFZK0IsWUFBWixDQUF5QixVQUF6QixFQUFxQzBDLEtBQXJDO0FBQ0E3SCxPQUFHLENBQUNvRCxPQUFKLENBQVkrQixZQUFaLENBQXlCLFdBQXpCLEVBQXNDNEMsTUFBdEMsRUFuRHdELENBcUR4RDs7QUFDQSxLQUFDTCxrQkFBRCxJQUF1QmdCLFVBQVUsQ0FBQ3ZCLFlBQUQsQ0FBakMsQ0F0RHdELENBd0R4RDs7QUFDQWpJLEtBQUMsQ0FBQzJFLEtBQUYsQ0FBUUUsZ0JBQVIsR0FBMkJvRCxZQUEzQjtBQUVBLEdBdlNGO0FBQUEsTUF3U0N1QixVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFTQyxTQUFULEVBQW9CO0FBRWhDQyxXQUFPLENBQUNDLFNBQVIsQ0FBa0JGLFNBQWxCLEVBQTZCLElBQTdCLG9CQUE4Q0EsU0FBOUM7QUFFQSxHQTVTRjtBQUFBLE1BNlNDL0QsVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBVztBQUV2QixRQUFNdUMsWUFBWSxHQUFHckssQ0FBQyxDQUFDOEwsT0FBRixDQUFVL0UsS0FBVixJQUFtQixDQUF4QztBQUNBc0QsZ0JBQVksR0FBR0UsV0FBVyxDQUFDRixZQUFELEVBQWUsSUFBZixDQUFkLEdBQXFDRSxXQUFXLENBQUNGLFlBQUQsQ0FBNUQ7QUFFQSxHQWxURjtBQUFBLE1BbVRDakMsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixHQUFXO0FBRWhDLFFBQUlwSSxDQUFDLENBQUM4TCxPQUFGLENBQVUvRSxLQUFkLEVBQXFCO0FBQ3BCLFVBQU1zRCxZQUFZLEdBQUdySyxDQUFDLENBQUM4TCxPQUFGLENBQVUvRSxLQUEvQjtBQUNBd0QsaUJBQVcsQ0FBQ0YsWUFBRCxDQUFYO0FBQ0E7QUFFRCxHQTFURjtBQUFBLE1BMlRDcEMsT0FBTyxHQUFHLFNBQVZBLE9BQVUsQ0FBUzlDLEtBQVQsRUFBZ0JoQixnQkFBaEIsRUFBa0NDLFlBQWxDLEVBQWdERixXQUFoRCxFQUE2REcsYUFBN0QsRUFBNEU7QUFFckY7QUFDQTJILDRCQUF3QixDQUFDN0gsZ0JBQUQsRUFBbUJDLFlBQW5CLEVBQWlDRixXQUFqQyxFQUE4Q0csYUFBOUMsQ0FBeEIsQ0FIcUYsQ0FLckY7O0FBQ0E0SCxvQkFBZ0IsQ0FBQy9ILFdBQUQsQ0FBaEIsQ0FOcUYsQ0FRckY7O0FBQ0FnSSx1QkFBbUIsQ0FBQ2hJLFdBQUQsQ0FBbkI7QUFFQSxHQXRVRjtBQUFBLE1BdVVDOEgsd0JBQXdCLEdBQUcsU0FBM0JBLHdCQUEyQixDQUFTN0gsZ0JBQVQsRUFBMkJDLFlBQTNCLEVBQXlDRixXQUF6QyxFQUFzREcsYUFBdEQsRUFBcUU7QUFFL0Y7QUFDQSxRQUFNMkMsaUJBQWlCLEdBQUc1RSxDQUFDLENBQUMyRSxLQUFGLENBQVFDLGlCQUFSLElBQTZCLEVBQXZEO0FBQ0EsUUFBSW1GLElBQUksR0FBRyxHQUFYLENBSitGLENBTS9GOztBQUNBLFFBQUkvSixDQUFDLENBQUMyRSxLQUFGLENBQVFJLGFBQVIsSUFBeUIvQyxZQUFZLEtBQUssV0FBOUMsRUFBMkQ7QUFFMUQ7QUFDQSxjQUFRRCxnQkFBUjtBQUNDLGFBQUssTUFBTDtBQUNBLGFBQUssT0FBTDtBQUNDZ0ksY0FBSSxHQUFHLEdBQVA7QUFDQTs7QUFDRCxhQUFLLElBQUw7QUFDQSxhQUFLLE1BQUw7QUFDQ0EsY0FBSSxHQUFHLEdBQVA7QUFDQTs7QUFDRDtBQUNDO0FBVkYsT0FIMEQsQ0FnQjFEOzs7QUFDQSxVQUFJQyxpQkFBaUIsaUNBQTBCRCxJQUExQixjQUFrQzlILGFBQWEsR0FBRyxFQUFsRCxRQUFyQjtBQUNBbkIsU0FBRyxDQUFDb0QsT0FBSixDQUFZK0IsWUFBWixDQUF5QixPQUF6QixFQUFrQ3JCLGlCQUFpQixHQUFHb0YsaUJBQXREO0FBQ0EsS0ExQjhGLENBNEIvRjs7O0FBQ0EsUUFBSWhLLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUUksYUFBUixLQUEwQi9DLFlBQVksS0FBSyxVQUFqQixJQUErQkEsWUFBWSxLQUFLLGFBQTFFLENBQUosRUFBOEY7QUFDN0YsVUFBSWdJLGtCQUFpQixpQ0FBMEJELElBQTFCLCtDQUFyQjs7QUFDQWpKLFNBQUcsQ0FBQ29ELE9BQUosQ0FBWStCLFlBQVosQ0FBeUIsT0FBekIsRUFBa0NyQixpQkFBaUIsR0FBR29GLGtCQUF0RDtBQUNBO0FBRUQsR0F6V0Y7QUFBQSxNQTBXQ0gsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUFTL0gsV0FBVCxFQUFzQjtBQUV4QyxRQUFJLENBQUM5QixDQUFDLENBQUMyRSxLQUFGLENBQVFJLGFBQWIsRUFBNEI7QUFDM0IsY0FBUWpELFdBQVI7QUFDQyxhQUFLLFlBQUw7QUFDQ2hCLGFBQUcsQ0FBQzBELE9BQUosQ0FBWS9GLFNBQVosQ0FBc0JDLEdBQXRCLENBQTBCc0IsQ0FBQyxDQUFDRyxPQUFGLENBQVV1RSxRQUFwQztBQUNBOztBQUNELGFBQUssV0FBTDtBQUNDNUQsYUFBRyxDQUFDMEQsT0FBSixDQUFZL0YsU0FBWixDQUFzQkcsTUFBdEIsQ0FBNkJvQixDQUFDLENBQUNHLE9BQUYsQ0FBVXVFLFFBQXZDO0FBQ0E7O0FBQ0Q7QUFDQztBQVJGO0FBVUE7QUFFRCxHQXpYRjtBQUFBLE1BMFhDb0YsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFTaEksV0FBVCxFQUFzQjtBQUUzQyxRQUFJOUIsQ0FBQyxDQUFDMkUsS0FBRixDQUFRSSxhQUFSLElBQXlCakQsV0FBekIsSUFBd0NBLFdBQVcsS0FBSyxLQUE1RCxFQUFtRTtBQUVsRTtBQUNBLFVBQUltSSxjQUFjLEdBQUc7QUFDcEIxQixXQUFHLEVBQUU7QUFEZSxPQUFyQjs7QUFJQSxjQUFRekcsV0FBUjtBQUNDLGFBQUssU0FBTDtBQUNDbUksd0JBQWMsQ0FBQzFCLEdBQWYsR0FBcUIsTUFBckI7QUFDQTs7QUFDRCxhQUFLLFlBQUw7QUFDQzBCLHdCQUFjLENBQUMxQixHQUFmLEdBQXFCLE1BQXJCO0FBQ0E7O0FBQ0QsYUFBSyxXQUFMO0FBQ0MwQix3QkFBYyxDQUFDMUIsR0FBZixHQUFxQixJQUFyQjtBQUNBOztBQUNELGFBQUssV0FBTDtBQUNDMEIsd0JBQWMsQ0FBQzFCLEdBQWYsR0FBcUIsT0FBckI7QUFDQTs7QUFDRDtBQUNDO0FBZEYsT0FQa0UsQ0F3QmxFOzs7QUFDQTVDLG1CQUFhLENBQUNzRSxjQUFELENBQWI7QUFDQTtBQUVELEdBeFpGOztBQTBaQSxTQUFPO0FBQ043TCxRQUFJLEVBQUVBLElBREE7QUFFTjJKLGNBQVUsRUFBRUEsVUFGTjtBQUdOSSxlQUFXLEVBQUVBO0FBSFAsR0FBUDtBQU1BLENBcGFhLENBb2FacEosTUFwYVksRUFvYUpBLE1BQU0sQ0FBQ0MsUUFwYUgsQ0FBZCxDOztBQ05BOzs7OztBQU1BLElBQU1WLElBQUksR0FBSSxVQUFTVixDQUFULEVBQVlDLENBQVosRUFBZUMsU0FBZixFQUEwQjtBQUV2Qzs7QUFFQSxNQUFNa0MsQ0FBQyxHQUFHO0FBQ1RDLGFBQVMsRUFBRTtBQUNWaUssYUFBTyxFQUFFLFVBREM7QUFFVkMsb0JBQWMsRUFBRSxrQkFGTjtBQUdWQyxpQkFBVyxFQUFFLHNCQUhIO0FBSVY1RixhQUFPLEVBQUU7QUFKQyxLQURGO0FBT1RHLFNBQUssRUFBRTtBQUNORyxnQkFBVSxFQUFFLElBRE47QUFFTnVGLHVCQUFpQixFQUFFLEtBRmI7QUFHTkMsbUJBQWEsRUFBRSxLQUhUO0FBSU5DLHVCQUFpQixFQUFFLENBSmI7QUFLTkMscUJBQWUsRUFBRTtBQUxYLEtBUEU7QUFjVHJLLFdBQU8sRUFBRTtBQUNSdUUsY0FBUSxFQUFFO0FBREY7QUFkQSxHQUFWO0FBQUEsTUFrQkM1RCxHQUFHLEdBQUcsRUFsQlA7QUFBQSxNQW1CQzFDLElBQUksR0FBRyxTQUFQQSxJQUFPLEdBQVc7QUFFakI7QUFDQTBDLE9BQUcsQ0FBQ29KLE9BQUosR0FBY3JNLENBQUMsQ0FBQ2tELGFBQUYsQ0FBZ0JmLENBQUMsQ0FBQ0MsU0FBRixDQUFZaUssT0FBNUIsQ0FBZDtBQUNBcEosT0FBRyxDQUFDcUosY0FBSixHQUFxQnRNLENBQUMsQ0FBQzBILGdCQUFGLENBQW1CdkYsQ0FBQyxDQUFDQyxTQUFGLENBQVlrSyxjQUEvQixDQUFyQjtBQUNBckosT0FBRyxDQUFDc0osV0FBSixHQUFrQnZNLENBQUMsQ0FBQzBILGdCQUFGLENBQW1CdkYsQ0FBQyxDQUFDQyxTQUFGLENBQVltSyxXQUEvQixDQUFsQjtBQUNBdEosT0FBRyxDQUFDMEQsT0FBSixHQUFjM0csQ0FBQyxDQUFDa0QsYUFBRixDQUFnQmYsQ0FBQyxDQUFDQyxTQUFGLENBQVl1RSxPQUE1QixDQUFkLENBTmlCLENBUWpCOztBQUNBeEUsS0FBQyxDQUFDMkUsS0FBRixDQUFRNEYsaUJBQVIsR0FBNEJ6SixHQUFHLENBQUNxSixjQUFKLENBQW1CM0UsTUFBL0M7QUFDQXhGLEtBQUMsQ0FBQzJFLEtBQUYsQ0FBUTZGLGVBQVIsR0FBMEIxSixHQUFHLENBQUNzSixXQUFKLENBQWdCNUUsTUFBMUMsQ0FWaUIsQ0FZakI7QUFDQTs7QUFDQSxTQUFLLElBQUlZLEtBQUssR0FBRyxDQUFqQixFQUFvQkEsS0FBSyxHQUFHcEcsQ0FBQyxDQUFDMkUsS0FBRixDQUFRNEYsaUJBQXBDLEVBQXVEbkUsS0FBSyxFQUE1RCxFQUFnRTtBQUMvRHRGLFNBQUcsQ0FBQ3FKLGNBQUosQ0FBbUIvRCxLQUFuQixFQUEwQnRILGdCQUExQixDQUEyQyxPQUEzQyxFQUFvRCxZQUFNO0FBQUVsQixTQUFDLENBQUNTLElBQUYsQ0FBTzBKLFVBQVA7QUFBc0IsT0FBbEY7QUFDQSxLQWhCZ0IsQ0FpQmpCOzs7QUFDQSxTQUFLLElBQUkzQixNQUFLLEdBQUcsQ0FBakIsRUFBb0JBLE1BQUssR0FBR3BHLENBQUMsQ0FBQzJFLEtBQUYsQ0FBUTZGLGVBQXBDLEVBQXFEcEUsTUFBSyxFQUExRCxFQUE4RDtBQUM3RHRGLFNBQUcsQ0FBQ3NKLFdBQUosQ0FBZ0JoRSxNQUFoQixFQUF1QnRILGdCQUF2QixDQUF3QyxPQUF4QyxFQUFpRDJMLG1CQUFqRDtBQUNBLEtBcEJnQixDQXNCakI7OztBQUNBLFFBQU03RSxPQUFPLEdBQUc7QUFBRXBELGVBQVMsRUFBRSxHQUFiO0FBQWtCRSxpQkFBVyxFQUFFO0FBQS9CLEtBQWhCO0FBQ0E5RSxLQUFDLENBQUMwRCxZQUFGLENBQWVDLEtBQWYsQ0FBcUJULEdBQUcsQ0FBQ29KLE9BQXpCLEVBQWtDdEUsT0FBbEMsRUFBMkNDLE9BQTNDLEVBeEJpQixDQTBCakI7O0FBQ0FqSSxLQUFDLENBQUNrQixnQkFBRixDQUFtQixRQUFuQixFQUE2QjJHLFFBQTdCO0FBQ0FLLG1CQUFlO0FBRWYsR0FqREY7QUFBQSxNQWtEQ0wsUUFBUSxHQUFHLFNBQVhBLFFBQVcsR0FBVztBQUVyQmMsZ0JBQVksQ0FBQ3ZHLENBQUMsQ0FBQ3dHLGNBQUgsQ0FBWjtBQUNBeEcsS0FBQyxDQUFDd0csY0FBRixHQUFtQjNILFVBQVUsQ0FBQ2lILGVBQUQsRUFBa0IsR0FBbEIsQ0FBN0I7QUFFQSxHQXZERjtBQUFBLE1Bd0RDQSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLEdBQVc7QUFFNUI7QUFDQTlGLEtBQUMsQ0FBQzJFLEtBQUYsQ0FBUUcsVUFBUixHQUFxQmxILENBQUMsQ0FBQytJLGdCQUFGLENBQW1CN0YsR0FBRyxDQUFDMEQsT0FBdkIsRUFBZ0MsU0FBaEMsRUFBMkNvQyxnQkFBM0MsQ0FBNEQsU0FBNUQsRUFBdUVDLE9BQXZFLENBQStFLEtBQS9FLEVBQXNGLEVBQXRGLENBQXJCLENBSDRCLENBSzVCOztBQUNBLFFBQUk3RyxDQUFDLENBQUMyRSxLQUFGLENBQVFHLFVBQVIsS0FBdUIsSUFBdkIsSUFBK0I5RSxDQUFDLENBQUMyRSxLQUFGLENBQVFHLFVBQVIsS0FBdUIsSUFBdEQsSUFBOEQ5RSxDQUFDLENBQUMyRSxLQUFGLENBQVFHLFVBQVIsS0FBdUIsSUFBekYsRUFBK0Y7QUFDOUY5RSxPQUFDLENBQUMyRSxLQUFGLENBQVEyRixhQUFSLEdBQXdCLEtBQXhCO0FBQ0EsS0FGRCxNQUVPO0FBQ050SyxPQUFDLENBQUMyRSxLQUFGLENBQVEyRixhQUFSLEdBQXdCLElBQXhCO0FBQ0E7QUFDRCxHQW5FRjtBQUFBLE1Bb0VDRyxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQVMxSCxLQUFULEVBQWdCO0FBRXJDO0FBQ0EsUUFBSXBCLE9BQU8sR0FBRyxJQUFkO0FBQUEsUUFDQ3NGLFNBQVMsR0FBR3RGLE9BQU8sQ0FBQ2lILFlBQVIsQ0FBcUIsTUFBckIsQ0FEYjtBQUFBLFFBRUN4QixXQUFXLEdBQUd2SixDQUFDLENBQUNrRCxhQUFGLENBQWdCa0csU0FBaEIsQ0FGZjtBQUFBLFFBR0NJLGdCQUFnQixHQUFHQyxLQUFLLENBQUNDLFNBQU4sQ0FBZ0JDLE9BQWhCLENBQXdCQyxJQUF4QixDQUE2QkwsV0FBVyxDQUFDTSxhQUFaLENBQTBCQyxRQUF2RCxFQUFpRVAsV0FBakUsQ0FIcEIsQ0FIcUMsQ0FTckM7O0FBQ0EsUUFBSXBILENBQUMsQ0FBQzJFLEtBQUYsQ0FBUTJGLGFBQVosRUFBMkI7QUFDMUJ2SCxXQUFLLENBQUNlLGNBQU47QUFDQWxHLE9BQUMsQ0FBQ1MsSUFBRixDQUFPOEosV0FBUCxDQUFtQmQsZ0JBQW5CO0FBQ0E7QUFDRCxHQWxGRjtBQUFBLE1BbUZDeEIsT0FBTyxHQUFHLFNBQVZBLE9BQVUsQ0FBUzlDLEtBQVQsRUFBZ0JoQixnQkFBaEIsRUFBa0NDLFlBQWxDLEVBQWdERixXQUFoRCxFQUE2REcsYUFBN0QsRUFBNEU7QUFFckYsUUFBSUgsV0FBVyxLQUFLLFdBQXBCLEVBQWlDO0FBQ2hDaEIsU0FBRyxDQUFDMEQsT0FBSixDQUFZL0YsU0FBWixDQUFzQkcsTUFBdEIsQ0FBNkJvQixDQUFDLENBQUNHLE9BQUYsQ0FBVXVFLFFBQXZDO0FBQ0E7QUFDRCxHQXhGRjs7QUEwRkEsU0FBTztBQUNOdEcsUUFBSSxFQUFFQTtBQURBLEdBQVA7QUFJQSxDQWxHYSxDQWtHWlcsTUFsR1ksRUFrR0pBLE1BQU0sQ0FBQ0MsUUFsR0gsQ0FBZCIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKHcsIGQsIHVuZGVmaW5lZCkge1xuXG5cdFwidXNlIHN0cmljdFwiO1xuXG5cdGNvbnN0IHJhZiA9IHJlcXVlc3RBbmltYXRpb25GcmFtZSB8fCBtb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHwgd2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8IG1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lLFxuXHRcdGluaXQgPSBmdW5jdGlvbigpIHtcblxuXHRcdFx0dy5ncmlkLmluaXQoKTtcblx0XHRcdHcubWVudS5pbml0KCk7XG5cdFx0XHR3LnNjcm9sbEhhbmRsZXIuaW5pdCgpO1xuXG5cdFx0XHRkLmJvZHkuY2xhc3NMaXN0LmFkZCgnd2luZG93LWlzLWxvYWRlZCcpO1xuXHRcdFx0ZC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnbm8tanMnKTtcblxuXHRcdH07XG5cblx0Ly8gd2hlbiBhbGwgaXMgbG9hZGVkXG5cdHJhZiA/IHJhZihmdW5jdGlvbigpIHsgdy5zZXRUaW1lb3V0KGluaXQsIDApOyB9KSA6IHcuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsIGluaXQpO1xuXG59KHdpbmRvdywgd2luZG93LmRvY3VtZW50KSk7XG4iLCIvKlxuXG5KcyBBbmltYXRlXG5cbmR1cmF0aW9uIOKAkyB0aGUgdG90YWwgYW5pbWF0aW9uIHRpbWUgaW4gbXMuXG50aW1pbmcg4oCTIHRoZSBmdW5jdGlvbiB0byBjYWxjdWxhdGUgYW5pbWF0aW9uIHByb2dyZXNzLiBHZXRzIGEgdGltZSBmcmFjdGlvbiBmcm9tIDAgdG8gMSwgcmV0dXJucyB0aGUgYW5pbWF0aW9uIHByb2dyZXNzLCB1c3VhbGx5IGZyb20gMCB0byAxLlxuZHJhdyDigJMgdGhlIGZ1bmN0aW9uIHRvIGRyYXcgdGhlIGFuaW1hdGlvbi5cblxuXG5leGFtcGxlOlxuXG5hbmltYXRlSXQoe1xuXHRkdXJhdGlvbjogMTAwMCxcblx0dGltaW5nKHRpbWVGcmFjdGlvbikge1xuXHRcdHJldHVybiB0aW1lRnJhY3Rpb247XG5cdH0sXG5cdGRyYXcocHJvZ3Jlc3MpIHtcblx0XHRlbGVtLnN0eWxlLndpZHRoID0gcHJvZ3Jlc3MgKiAxMDAgKyAnJSc7XG5cdH1cbn0pO1xuXG5EaWZmZXJlbnQgdGltaW5nIGZ1bmN0aW9uczogaHR0cHM6Ly9naXRodWIuY29tL0dlbmVuZW5lbmFhbS9lYXNpbmctdXRpbHNcblxuKi9cblxuY29uc3QgYW5pbWF0ZUl0ID0gKHt0aW1pbmcsIGRyYXcsIGR1cmF0aW9uLCBhbmltYXRpb25TdGFydCwgYW5pbWF0aW9uSXRlcmF0aW9uLCBhbmltYXRpb25FbmR9KSA9PiB7XG5cblx0Y29uc3Qgc3RhcnQgPSBwZXJmb3JtYW5jZS5ub3coKTtcblxuXHQvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC0gQ2FsbGJhY2tcblx0LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblx0Ly8gT24gc3RhcnRcblx0YW5pbWF0aW9uU3RhcnQgJiYgYW5pbWF0aW9uU3RhcnQoKTtcblxuXHQvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC0gQW5pbWF0aW9uXG5cdC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG5cdHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbiBhbmltYXRlKHRpbWUpIHtcblx0XHQvLyB0aW1lRnJhY3Rpb24gZ29lcyBmcm9tIDAgdG8gMVxuXHRcdGxldCB0aW1lRnJhY3Rpb24gPSAodGltZSAtIHN0YXJ0KSAvIGR1cmF0aW9uO1xuXHRcdGlmICh0aW1lRnJhY3Rpb24gPiAxKSB0aW1lRnJhY3Rpb24gPSAxO1xuXHRcdGlmICh0aW1lRnJhY3Rpb24gPCAwKSB0aW1lRnJhY3Rpb24gPSAwO1xuXG5cdFx0Ly8gY2FsY3VsYXRlIHRoZSBjdXJyZW50IGFuaW1hdGlvbiBzdGF0ZVxuXHRcdGNvbnN0IHByb2dyZXNzID0gdGltaW5nKHRpbWVGcmFjdGlvbik7XG5cblx0XHRkcmF3KHByb2dyZXNzKTsgLy8gZHJhdyBpdFxuXG5cdFx0aWYgKHRpbWVGcmFjdGlvbiA8IDEpIHtcblx0XHRcdHJlcXVlc3RBbmltYXRpb25GcmFtZShhbmltYXRlKTtcblx0XHR9XG5cblx0XHQvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0LSBDYWxsYmFja3Ncblx0XHQtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuXHRcdC8vIE9uIGl0ZXJhdGlvblxuXHRcdGlmICh0aW1lRnJhY3Rpb24gPiAwICYmIHRpbWVGcmFjdGlvbiA8IDEpIHtcblx0XHRcdGFuaW1hdGlvbkl0ZXJhdGlvbiAmJiBhbmltYXRpb25JdGVyYXRpb24oKTtcblx0XHR9XG5cblx0XHQvLyBPbiBlbmRcblx0XHRpZiAodGltZUZyYWN0aW9uID49IDEpIHtcblx0XHRcdGFuaW1hdGlvbkVuZCAmJiBhbmltYXRpb25FbmQoKTtcblx0XHR9XG5cblx0fSk7XG59OyIsImNvbnN0IHNjcm9sbEhhbmRsZXIgPSAoZnVuY3Rpb24gKHcsIGQsICQsIHVuZGVmaW5lZCkge1xuXG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgY29uc3QgcyA9IHtcbiAgICAgICAgICAgIHNlbGVjdG9yczoge1xuICAgICAgICAgICAgICAgIHRoZUh0bWw6ICdodG1sJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNsYXNzZXM6IHtcbiAgICAgICAgICAgICAgICBzY3JvbGxVcDogJ2pzX3Njcm9sbGluZy11cCcsXG4gICAgICAgICAgICAgICAgc2Nyb2xsRG93bjogJ2pzX3Njcm9sbGluZy1kb3duJyxcbiAgICAgICAgICAgICAgICBzY3JvbGxEZWFkWm9uZTogJ2pzX3Njcm9sbGluZy1kZWFkem9uZSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0aGVTY3JvbGxUaW1lcjogbnVsbCxcbiAgICAgICAgICAgIHRoZU5ld1Njcm9sbFBvc2l0aW9uOiBudWxsLFxuICAgICAgICAgICAgdGhlT2xkU2Nyb2xsUG9zaXRpb246IG51bGwsXG4gICAgICAgICAgICB0aGVTY3JvbGxEZWFkem9uZTogbnVsbCxcbiAgICAgICAgICAgIHRoZUN1cnJlbnRTY3JvbGxEaXJlY3Rpb246IG51bGwsXG4gICAgICAgICAgICB0aGVDdXJyZW50QnJlYWtwb2ludDogbnVsbCxcbiAgICAgICAgICAgIHRpY2tpbmc6IGZhbHNlLFxuXG4gICAgICAgIH0sXG4gICAgICAgIGVscyA9IHt9LFxuICAgICAgICBpbml0ID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgICAgICAvLyBkZWZpbmUgZWxlbWVudHNcbiAgICAgICAgICAgIGVscy50aGVIdG1sID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzLnNlbGVjdG9ycy50aGVIdG1sKTtcblxuICAgICAgICAgICAgLy8gbm8gZWxlbWVudHNcbiAgICAgICAgICAgIGlmICghZWxzLnRoZUh0bWwpIHsgcmV0dXJuOyB9XG5cbiAgICAgICAgICAgIC8vIGRlZmluZSBzZXR0aW5nc1xuICAgICAgICAgICAgcy50aGVPbGRTY3JvbGxQb3NpdGlvbiA9IHcucGFnZVlPZmZzZXQ7XG4gICAgICAgICAgICBzLnRoZVNjcm9sbERlYWR6b25lID0gMTAwO1xuXG4gICAgICAgICAgICAvLyBzY3JvbGwgc2V0dXBcbiAgICAgICAgICAgIHcuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgc2Nyb2xsSXQpO1xuXG4gICAgICAgIH0sXG4gICAgICAgIHNjcm9sbEl0ID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgICAgICAvLyBHZXQgdGhlIG5ldyBzY3JvbGwgcG9zaXRpb25cbiAgICAgICAgICAgIHMudGhlTmV3U2Nyb2xsUG9zaXRpb24gPSB3aW5kb3cuc2Nyb2xsWTtcbiAgICAgICAgICAgIHJlcXVlc3RUaWNrKCk7XG5cbiAgICAgICAgfSxcbiAgICAgICAgcmVxdWVzdFRpY2sgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgIC8vIFRyb3R0bGUgd2l0aCByZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICAgICAgICAgIGlmICghcy50aWNraW5nKSB7XG4gICAgICAgICAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKHVwZGF0ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzLnRpY2tpbmcgPSB0cnVlO1xuXG4gICAgICAgIH0sXG4gICAgICAgIHVwZGF0ZSA9IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICAgICAgLy8gcmVzZXQgdGhlIHRpY2sgc28gd2UgY2FuXG4gICAgICAgICAgICAvLyBjYXB0dXJlIHRoZSBuZXh0IG9uU2Nyb2xsXG4gICAgICAgICAgICBzLnRpY2tpbmcgPSBmYWxzZTtcblxuICAgICAgICAgICAgLy8gRGVmaW5lIHRoZSBhY3Rpb25cdFxuICAgICAgICAgICAgaWYgKHMudGhlTmV3U2Nyb2xsUG9zaXRpb24gPCBzLnRoZU9sZFNjcm9sbFBvc2l0aW9uICYmIHMudGhlTmV3U2Nyb2xsUG9zaXRpb24gPiBzLnRoZVNjcm9sbERlYWR6b25lKSB7XG5cbiAgICAgICAgICAgICAgICAvLyBTY3JvbGxpbmcgdXBcbiAgICAgICAgICAgICAgICBlbHMudGhlSHRtbC5jbGFzc0xpc3QuYWRkKHMuY2xhc3Nlcy5zY3JvbGxVcCk7XG4gICAgICAgICAgICAgICAgZWxzLnRoZUh0bWwuY2xhc3NMaXN0LnJlbW92ZShzLmNsYXNzZXMuc2Nyb2xsRG93bik7XG4gICAgICAgICAgICAgICAgZWxzLnRoZUh0bWwuY2xhc3NMaXN0LnJlbW92ZShzLmNsYXNzZXMuc2Nyb2xsRGVhZFpvbmUpO1xuXG4gICAgICAgICAgICAgICAgLy8gc2V0IHRoZSBjdXJyZW50IHNjcm9sbCBwb3NpdG9uXG4gICAgICAgICAgICAgICAgcy50aGVDdXJyZW50U2Nyb2xsRGlyZWN0aW9uID0gXCJ1cFwiO1xuXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHMudGhlTmV3U2Nyb2xsUG9zaXRpb24gPiBzLnRoZU9sZFNjcm9sbFBvc2l0aW9uICYmIHMudGhlTmV3U2Nyb2xsUG9zaXRpb24gPiBzLnRoZVNjcm9sbERlYWR6b25lKSB7XG5cbiAgICAgICAgICAgICAgICAvLyBTY3JvbGxpbmcgZG93blxuICAgICAgICAgICAgICAgIGVscy50aGVIdG1sLmNsYXNzTGlzdC5hZGQocy5jbGFzc2VzLnNjcm9sbERvd24pO1xuICAgICAgICAgICAgICAgIGVscy50aGVIdG1sLmNsYXNzTGlzdC5yZW1vdmUocy5jbGFzc2VzLnNjcm9sbFVwKTtcbiAgICAgICAgICAgICAgICBlbHMudGhlSHRtbC5jbGFzc0xpc3QucmVtb3ZlKHMuY2xhc3Nlcy5zY3JvbGxEZWFkWm9uZSk7XG5cbiAgICAgICAgICAgICAgICAvLyBzZXQgdGhlIGN1cnJlbnQgc2Nyb2xsIHBvc2l0b25cbiAgICAgICAgICAgICAgICBzLnRoZUN1cnJlbnRTY3JvbGxEaXJlY3Rpb24gPSBcImRvd25cIjtcblxuICAgICAgICAgICAgfSBlbHNlIGlmIChzLnRoZU5ld1Njcm9sbFBvc2l0aW9uIDwgcy50aGVTY3JvbGxEZWFkem9uZSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIC8vIFNjcm9sbGluZyB3aXRoaW4gZGVhZHpvbmVcbiAgICAgICAgICAgICAgICBlbHMudGhlSHRtbC5jbGFzc0xpc3QuYWRkKHMuY2xhc3Nlcy5zY3JvbGxEZWFkWm9uZSk7XG4gICAgICAgICAgICAgICAgZWxzLnRoZUh0bWwuY2xhc3NMaXN0LnJlbW92ZShzLmNsYXNzZXMuc2Nyb2xsVXApO1xuICAgICAgICAgICAgICAgIGVscy50aGVIdG1sLmNsYXNzTGlzdC5yZW1vdmUocy5jbGFzc2VzLnNjcm9sbERvd24pO1xuXG4gICAgICAgICAgICAgICAgLy8gc2V0IHRoZSBjdXJyZW50IHNjcm9sbCBwb3NpdG9uXG4gICAgICAgICAgICAgICAgcy50aGVDdXJyZW50U2Nyb2xsRGlyZWN0aW9uID0gXCJkZWFkem9uZVwiO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBSZXBsYWNlIHRoZSBuZXcgc2Nyb2xsIHBvc2l0aW9uXG4gICAgICAgICAgICBzLnRoZU9sZFNjcm9sbFBvc2l0aW9uID0gcy50aGVOZXdTY3JvbGxQb3NpdGlvbjtcblxuICAgICAgICAgICAgLy8gZ2V0IHRoZSBjdXJyZW50IGJyZWFrcG9pbnRcbiAgICAgICAgICAgIC8vIHMudGhlQ3VycmVudEJyZWFrcG9pbnQgPSB3LmdldENvbXB1dGVkU3R5bGUoZC5xdWVyeVNlbGVjdG9yKCdib2R5JyksICc6YmVmb3JlJykuZ2V0UHJvcGVydHlWYWx1ZSgnY29udGVudCcpLnJlcGxhY2UoL1xcXCIvZywgJycpO1xuXG4gICAgICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICAgIGluaXQ6IGluaXRcbiAgICB9O1xuXG59KHdpbmRvdywgd2luZG93LmRvY3VtZW50LCB3aW5kb3cualF1ZXJ5KSk7IiwiY29uc3QgdG91Y2hIYW5kbGVyID0gKGZ1bmN0aW9uKHcsIGQsIHVuZGVmaW5lZCkge1xuXG5cdCd1c2Ugc3RyaWN0JztcblxuXHRjb25zdCB3YXRjaCA9IGZ1bmN0aW9uKHRvdWNoZWxlbWVudCwgdGhlT3B0aW9ucywgdGhlQ2FsbGJhY2spIHtcblxuXHRcdC8qIFNFVFRJTkdTXG5cdFx0LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblxuXHRcdC8vIHRvdWNoIGVsZW1lbnRcblx0XHRjb25zdCBlbGVtZW50ID0gdG91Y2hlbGVtZW50O1xuXG5cdFx0Ly8gZ2VzdHVyZSBtZXN1cmVtZW50IHZhcmlhYmxlc1xuXHRcdGxldCBlbGFwc2VkVGltZSA9IHVuZGVmaW5lZDtcblx0XHRsZXQgc3RhcnRUaW1lID0gdW5kZWZpbmVkO1xuXHRcdGxldCBnZXN0dXJlVHlwZSA9IHVuZGVmaW5lZDtcblx0XHRsZXQgZ2VzdHVyZURpcmVjdGlvbiA9IHVuZGVmaW5lZDtcblx0XHRsZXQgZ2VzdHVyZVBoYXNlID0gdW5kZWZpbmVkO1xuXHRcdGxldCB0b3VjaERpc3RhbmNlID0gMDtcblxuXHRcdC8vIHRvdWNoIG1lc3VyZW1lbnQgdmFyaWFibGVzXG5cdFx0bGV0IHRvdWNoU3RhcnRQb2ludFggPSB1bmRlZmluZWQ7XG5cdFx0bGV0IHRvdWNoU3RhcnRQb2ludFkgPSB1bmRlZmluZWQ7XG5cdFx0bGV0IHRvdWNoRGlzdGFuY2VYID0gdW5kZWZpbmVkO1xuXHRcdGxldCB0b3VjaERpc3RhbmNlWSA9IHVuZGVmaW5lZDtcblx0XHRsZXQgdG91Y2hFbmRQb2ludFggPSB1bmRlZmluZWQ7XG5cdFx0bGV0IHRvdWNoRW5kUG9pbnRZID0gdW5kZWZpbmVkO1xuXG5cdFx0Ly8gb3B0aW9uc1xuXHRcdGNvbnN0IHRocmVzaG9sZCA9IHRoZU9wdGlvbnMudHJlc2hvbGQgfHwgMTUwOyAvL3JlcXVpcmVkIG1pbiBkaXN0YW5jZSB0cmF2ZWxlZCB0byBiZSBjb25zaWRlcmVkIHN3aXBlXG5cdFx0Y29uc3QgYWxsb3dlZFRpbWUgPSB0aGVPcHRpb25zLmFsbG93ZWRUaW1lIHx8IDIwMDsgLy8gbWF4aW11bSB0aW1lIGFsbG93ZWQgdG8gdHJhdmVsIHRoYXQgZGlzdGFuY2Vcblx0XHRjb25zdCByZXN0cmFpbnQgPSB0aGVPcHRpb25zLnJlc3RyYWludCB8fCAxMDA7IC8vIG1heGltdW0gZGlzdGFuY2UgYWxsb3dlZCBhdCB0aGUgc2FtZSB0aW1lIGluIHBlcnBlbmRpY3VsYXIgZGlyZWN0aW9uXG5cdFx0Y29uc3QgcGFzc2l2ZUV2ZW50TGlzdGVuZXIgPSB0aGVPcHRpb25zLnBhc3NpdmVFdmVudExpc3RlbmVyIHx8IHRydWU7XG5cdFx0Y29uc3QgcHJldmVudFNjcm9sbGluZyA9IHRoZU9wdGlvbnMucHJldmVudFNjcm9sbGluZyB8fCBmYWxzZTsgLy8gcHJldmVudCBzY3JvbGxpbmcgd2hlbiBpbnNpZGUgdGFyZ2V0IGVsZW1lbnRcblxuXHRcdC8vIGNhbGxiYWNrXG5cdFx0Y29uc3QgY2FsbGJhY2sgPSB0aGVDYWxsYmFjayB8fCBmdW5jdGlvbihldmVudCwgZ2VzdHVyZURpcmVjdGlvbiwgZ2VzdHVyZVBoYXNlLCBnZXN0dXJlVHlwZSwgdG91Y2hEaXN0YW5jZSkgeyB9O1xuXG5cdFx0Ly8gYmluZCBldmVudHNcblx0XHRlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoc3RhcnQnLCBoYW5kbGVUb3VjaCwgeyBwYXNzaXZlOiBwYXNzaXZlRXZlbnRMaXN0ZW5lciB9KTtcblx0XHRlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNobW92ZScsIGhhbmRsZVRvdWNoLCB7IHBhc3NpdmU6ICFwcmV2ZW50U2Nyb2xsaW5nIH0pO1xuXHRcdGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBoYW5kbGVUb3VjaCwgeyBwYXNzaXZlOiBwYXNzaXZlRXZlbnRMaXN0ZW5lciB9KTtcblx0XHRlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoY2FuY2VsJywgaGFuZGxlVG91Y2gsIHsgcGFzc2l2ZTogcGFzc2l2ZUV2ZW50TGlzdGVuZXIgfSk7XG5cdFx0ZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCBoYW5kbGVUb3VjaCwgZmFsc2UpO1xuXG5cblx0XHQvKiBUT1VDSCBIQU5ETEVSXG5cdFx0LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblx0XHRmdW5jdGlvbiBoYW5kbGVUb3VjaChldmVudCkge1xuXG5cdFx0XHQvLyBiaW5kIG1vdXNlIGV2ZW50c1xuXHRcdFx0aWYgKGV2ZW50LnR5cGUgPT09ICdtb3VzZWRvd24nKSB7XG5cdFx0XHRcdGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgaGFuZGxlVG91Y2gsIGZhbHNlKTtcblx0XHRcdFx0ZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZXVwJywgaGFuZGxlVG91Y2gsIGZhbHNlKTtcblx0XHRcdFx0ZC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWxlYXZlJywgaGFuZGxlVG91Y2gsIGZhbHNlKTtcblx0XHRcdH1cblxuXHRcdFx0Ly8gaW5pdGlhbCB0b3VjaFxuXHRcdFx0Y29uc3QgdGhlVG91Y2hPYmplY3QgPSBldmVudC5jaGFuZ2VkVG91Y2hlcyA/IGV2ZW50LmNoYW5nZWRUb3VjaGVzWzBdIDogZXZlbnQ7XG5cblx0XHRcdC8vIGRlZmluZSB0b3VjaCBhY3Rpb25cblx0XHRcdHN3aXRjaCAoZXZlbnQudHlwZSkge1xuXHRcdFx0XHRjYXNlIFwidG91Y2hzdGFydFwiOlxuXHRcdFx0XHRjYXNlIFwibW91c2Vkb3duXCI6XG5cdFx0XHRcdFx0b25Ub3VjaFN0YXJ0KGV2ZW50KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSBcInRvdWNobW92ZVwiOlxuXHRcdFx0XHRjYXNlIFwibW91c2Vtb3ZlXCI6XG5cdFx0XHRcdFx0b25Ub3VjaE1vdmUoZXZlbnQpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlIFwidG91Y2hlbmRcIjpcblx0XHRcdFx0Y2FzZSBcIm1vdXNldXBcIjpcblx0XHRcdFx0XHRvblRvdWNoRW5kKGV2ZW50KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSBcInRvdWNoY2FuY2VsXCI6XG5cdFx0XHRcdGNhc2UgXCJtb3VzZWxlYXZlXCI6XG5cdFx0XHRcdFx0b25Ub3VjaENhbmNlbChldmVudCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXG5cdFx0XHQvKiBUT1VDSCBTVEFSVCBIQU5ETEVSXG5cdFx0XHQtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuXHRcdFx0ZnVuY3Rpb24gb25Ub3VjaFN0YXJ0KGV2ZW50KSB7XG5cblx0XHRcdFx0Ly8gdG91Y2ggZGF0YVxuXHRcdFx0XHR0b3VjaFN0YXJ0UG9pbnRYID0gcGFyc2VJbnQodGhlVG91Y2hPYmplY3Quc2NyZWVuWCk7XG5cdFx0XHRcdHRvdWNoU3RhcnRQb2ludFkgPSBwYXJzZUludCh0aGVUb3VjaE9iamVjdC5zY3JlZW5ZKTtcblxuXHRcdFx0XHQvLyByZXNldFxuXHRcdFx0XHR0b3VjaERpc3RhbmNlWCA9IDA7XG5cdFx0XHRcdHRvdWNoRGlzdGFuY2VZID0gMDtcblxuXHRcdFx0XHQvLyBzZXQgc3RhdGVcblx0XHRcdFx0Z2VzdHVyZURpcmVjdGlvbiA9IGZhbHNlO1xuXHRcdFx0XHRnZXN0dXJlUGhhc2UgPSAndG91Y2hzdGFydCc7XG5cdFx0XHRcdGdlc3R1cmVUeXBlID0gZmFsc2U7XG5cdFx0XHRcdHRvdWNoRGlzdGFuY2UgPSAwO1xuXG5cdFx0XHRcdC8vIHJlY29yZCB0aW1lIHdoZW4gZmluZ2VyIGZpcnN0IG1ha2VzIGNvbnRhY3Qgd2l0aCBzdXJmYWNlXG5cdFx0XHRcdHN0YXJ0VGltZSA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuXG5cdFx0XHRcdC8vIGZpcmUgY2FsbGJhY2sgZnVuY3Rpb24gd2l0aCBwYXJhbXMgZGlyPVwibm9uZVwiLCBwaGFzZT1cInN0YXJ0XCIsIHN3aXBldHlwZT1cIm5vbmVcIiBldGNcblx0XHRcdFx0Y2FsbGJhY2soZXZlbnQsIGdlc3R1cmVEaXJlY3Rpb24sIGdlc3R1cmVQaGFzZSwgZ2VzdHVyZVR5cGUsIHRvdWNoRGlzdGFuY2UpO1xuXG5cdFx0XHRcdC8vIHByZXZlbnQgaXQ/XG5cdFx0XHRcdCFwYXNzaXZlRXZlbnRMaXN0ZW5lciAmJiBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0fVxuXG5cdFx0XHQvKiBUT1VDSCBNT1ZFIEhBTkRMRVJcblx0XHRcdC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG5cdFx0XHRmdW5jdGlvbiBvblRvdWNoTW92ZShldmVudCkge1xuXG5cdFx0XHRcdC8vIHRvdWNoIGRhdGFcblx0XHRcdFx0dG91Y2hEaXN0YW5jZVggPSBwYXJzZUludCh0aGVUb3VjaE9iamVjdC5zY3JlZW5YKSAtIHRvdWNoU3RhcnRQb2ludFg7XG5cdFx0XHRcdHRvdWNoRGlzdGFuY2VZID0gcGFyc2VJbnQodGhlVG91Y2hPYmplY3Quc2NyZWVuWSkgLSB0b3VjaFN0YXJ0UG9pbnRZO1xuXG5cdFx0XHRcdC8vIHNldCBzdGF0ZVxuXHRcdFx0XHRnZXN0dXJlUGhhc2UgPSAndG91Y2htb3ZlJztcblx0XHRcdFx0Z2VzdHVyZVR5cGUgPSBmYWxzZTtcblxuXHRcdFx0XHQvLyBob3Jpem9udGFsIHN3aXBlXG5cdFx0XHRcdGlmIChNYXRoLmFicyh0b3VjaERpc3RhbmNlWCkgPiBNYXRoLmFicyh0b3VjaERpc3RhbmNlWSkpIHtcblx0XHRcdFx0XHR0b3VjaERpc3RhbmNlID0gdG91Y2hEaXN0YW5jZVg7XG5cdFx0XHRcdFx0Z2VzdHVyZURpcmVjdGlvbiA9ICh0b3VjaERpc3RhbmNlIDwgMCkgPyAnbGVmdCcgOiAncmlnaHQnXG5cdFx0XHRcdFx0Y2FsbGJhY2soZXZlbnQsIGdlc3R1cmVEaXJlY3Rpb24sIGdlc3R1cmVQaGFzZSwgZ2VzdHVyZVR5cGUsIHRvdWNoRGlzdGFuY2UpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly8gdmVydGljYWwgc3dpcGVcblx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0dG91Y2hEaXN0YW5jZSA9IHRvdWNoRGlzdGFuY2VZO1xuXHRcdFx0XHRcdGdlc3R1cmVEaXJlY3Rpb24gPSAodG91Y2hEaXN0YW5jZSA8IDApID8gJ3VwJyA6ICdkb3duJ1xuXHRcdFx0XHRcdGNhbGxiYWNrKGV2ZW50LCBnZXN0dXJlRGlyZWN0aW9uLCBnZXN0dXJlUGhhc2UsIGdlc3R1cmVUeXBlLCB0b3VjaERpc3RhbmNlKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vIHByZXZlbnQgaXQ/XG5cdFx0XHRcdC8vIHByZXZlbnQgc2Nyb2xsaW5nIHdoZW4gaW5zaWRlIHRhcmdldCBlbGVtZW50XG5cdFx0XHRcdHByZXZlbnRTY3JvbGxpbmcgJiYgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdH1cblxuXHRcdFx0LyogVE9VQ0ggRU5EIEhBTkxFUlxuXHRcdFx0LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblx0XHRcdGZ1bmN0aW9uIG9uVG91Y2hFbmQoZXZlbnQpIHtcblxuXHRcdFx0XHQvLyB0b3VjaCBkYXRhXG5cdFx0XHRcdHRvdWNoRW5kUG9pbnRYID0gcGFyc2VJbnQodGhlVG91Y2hPYmplY3Quc2NyZWVuWCk7XG5cdFx0XHRcdHRvdWNoRW5kUG9pbnRZID0gcGFyc2VJbnQodGhlVG91Y2hPYmplY3Quc2NyZWVuWSk7XG5cblx0XHRcdFx0Ly8gZ2V0IHRpbWUgZWxhcHNlZFxuXHRcdFx0XHRlbGFwc2VkVGltZSA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gc3RhcnRUaW1lO1xuXG5cdFx0XHRcdC8vIHNldCBzdGF0ZVxuXHRcdFx0XHRnZXN0dXJlUGhhc2UgPSAndG91Y2hlbmQnO1xuXHRcdFx0XHRnZXN0dXJlVHlwZSA9IGZhbHNlO1xuXG5cdFx0XHRcdC8vIGNoZWNrIHRoYXQgZWxhcHNlZCB0aW1lIGlzIHdpdGhpbiBzcGVjaWZpZWQsXG5cdFx0XHRcdC8vIGhvcml6b250YWwvdmVydGljYWwgZGlzdCB0cmF2ZWxlZCA+PSB0aHJlc2hvbGQsXG5cdFx0XHRcdC8vIGFuZCBob3Jpem9udGFsL3ZlcnRpY2FsIGRpc3QgdHJhdmVsZWQgPD0gcmVzdHJhaW50XG5cblx0XHRcdFx0Ly8gY29uc29sZS5sb2coZWxhcHNlZFRpbWUsIGFsbG93ZWRUaW1lKTtcblx0XHRcdFx0Ly8gY29uc29sZS5sb2coZ2VzdHVyZURpcmVjdGlvbiwgZ2VzdHVyZVBoYXNlLCBnZXN0dXJlVHlwZSwgdG91Y2hEaXN0YW5jZSk7XG5cblx0XHRcdFx0Ly8gdmFsaWQgZ2VzdHVyZVxuXHRcdFx0XHRpZiAoZWxhcHNlZFRpbWUgPD0gYWxsb3dlZFRpbWUpIHtcblxuXHRcdFx0XHRcdC8vIGhvcml6b250YWwgc3dpcGVcblx0XHRcdFx0XHRpZiAoTWF0aC5hYnModG91Y2hEaXN0YW5jZVgpID49IHRocmVzaG9sZCAmJiBNYXRoLmFicyh0b3VjaERpc3RhbmNlWSkgPD0gcmVzdHJhaW50KSB7XG5cdFx0XHRcdFx0XHRnZXN0dXJlVHlwZSA9ICh0b3VjaERpc3RhbmNlWCA8IDApID8gJ3N3aXBlTGVmdCcgOiAnc3dpcGVSaWdodCc7XG5cdFx0XHRcdFx0XHRnZXN0dXJlRGlyZWN0aW9uID0gKHRvdWNoRGlzdGFuY2VYIDwgMCkgPyAnbGVmdCcgOiAncmlnaHQnO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdC8vIHZlcnRpY2FsIHN3aXBlXG5cdFx0XHRcdFx0aWYgKE1hdGguYWJzKHRvdWNoRGlzdGFuY2VZKSA+PSB0aHJlc2hvbGQgJiYgTWF0aC5hYnModG91Y2hEaXN0YW5jZVgpIDw9IHJlc3RyYWludCkge1xuXHRcdFx0XHRcdFx0Z2VzdHVyZVR5cGUgPSAodG91Y2hEaXN0YW5jZVkgPCAwKSA/ICdzd2lwZVVwJyA6ICdzd2lwZURvd24nO1xuXHRcdFx0XHRcdFx0Z2VzdHVyZURpcmVjdGlvbiA9ICh0b3VjaERpc3RhbmNlWCA8IDApID8gJ3VwJyA6ICdkb3duJztcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHQvLyB0YXBcblx0XHRcdFx0XHRpZiAodG91Y2hEaXN0YW5jZVggPT09IHRvdWNoRGlzdGFuY2VZKSB7XG5cdFx0XHRcdFx0XHRnZXN0dXJlVHlwZSA9ICd0YXAnO1xuXHRcdFx0XHRcdFx0Z2VzdHVyZURpcmVjdGlvbiA9IGZhbHNlXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly8gRmlyZSBjYWxsYmFjayBmdW5jdGlvbiB3aXRoIHBhcmFtcyBkaXI9XCJsZWZ0fHJpZ2h0fHVwfGRvd25cIiwgcGhhc2U9XCJlbmRcIiwgc3dpcGV0eXBlPWRpciBldGM6XG5cdFx0XHRcdGNhbGxiYWNrKGV2ZW50LCBnZXN0dXJlRGlyZWN0aW9uLCBnZXN0dXJlUGhhc2UsIGdlc3R1cmVUeXBlLCB0b3VjaERpc3RhbmNlKVxuXG5cdFx0XHRcdC8vIHByZXZlbnQgaXQ/XG5cdFx0XHRcdCFwYXNzaXZlRXZlbnRMaXN0ZW5lciAmJiBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdFx0XHRcdC8vIHJlbW92ZSBldmVudCBmb3IgbW91c2Vcblx0XHRcdFx0aWYgKGV2ZW50LnR5cGUgPT09ICdtb3VzZXVwJykge1xuXHRcdFx0XHRcdGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgaGFuZGxlVG91Y2gsIGZhbHNlKTtcblx0XHRcdFx0XHRlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNldXAnLCBoYW5kbGVUb3VjaCwgZmFsc2UpO1xuXHRcdFx0XHRcdGQucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIGhhbmRsZVRvdWNoLCBmYWxzZSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gb25Ub3VjaENhbmNlbChldmVudCkge1xuXHRcdFx0XHQvLyBGaXJlIGNhbGxiYWNrIGZ1bmN0aW9uXG5cdFx0XHRcdGNhbGxiYWNrKGV2ZW50LCBmYWxzZSwgJ3RvdWNoY2FuY2VsJywgZmFsc2UsIDApO1xuXG5cdFx0XHRcdC8vIHJlbW92ZSBldmVudCBmb3IgbW91c2Vcblx0XHRcdFx0aWYgKGV2ZW50LnR5cGUgPT09ICdtb3VzZWxlYXZlJykge1xuXHRcdFx0XHRcdGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgaGFuZGxlVG91Y2gsIGZhbHNlKTtcblx0XHRcdFx0XHRlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNldXAnLCBoYW5kbGVUb3VjaCwgZmFsc2UpO1xuXHRcdFx0XHRcdGQucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIGhhbmRsZVRvdWNoLCBmYWxzZSk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvLyBwcmV2ZW50IGl0P1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCAmJiBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0fVxuXHRcdH1cblx0fTtcblxuXHRyZXR1cm4ge1xuXHRcdHdhdGNoOiB3YXRjaFxuXHR9O1xuXG59KHdpbmRvdywgd2luZG93LmRvY3VtZW50KSk7XG4iLCIvKlxuXG5IYW5kbGUgZGVzdG9wIGxheW91dFxuXG4qL1xuXG5jb25zdCBncmlkID0gKGZ1bmN0aW9uKHcsIGQsIHVuZGVmaW5lZCkge1xuXG5cdCd1c2Ugc3RyaWN0JztcblxuXHRjb25zdCBzID0ge1xuXHRcdHNlbGVjdG9yczoge1xuXHRcdFx0dGhlR3JpZDogJy5qcy1ncmlkJyxcblx0XHRcdHRoZUdyaWRJdGVtczogJy5qcy1ncmlkX19pdGVtJyxcblx0XHRcdHRoZUdyaWRJdGVtU2VsZWN0b3I6ICcuanMtZ3JpZF9faXRlbS1zZWxlY3RvcicsXG5cdFx0XHR0aGVHcmlkSXRlbUZvY3VzOiAnLmpzLWdyaWRfX2l0ZW0tZm9jdXMnLFxuXHRcdFx0dGhlR3JpZEl0ZW1NZW51OiAnLmpzLWdyaWRfX2l0ZW1fX21lbnUnLFxuXHRcdFx0dGhlR3JpZEl0ZW1OZXh0TGluazogJy5qcy1ncmlkX19pdGVtX19saW5rX19uZXh0Jyxcblx0XHRcdHRoZUJvZHk6ICdib2R5J1xuXHRcdH0sXG5cdFx0Y2xhc3Nlczoge1xuXHRcdFx0aXNBY3RpdmVHcmlkSXRlbTogJ2pzLWdyaWRfX2l0ZW0tLWFjdGl2ZScsXG5cdFx0XHRvcGVuTWVudTogJ2pzLW1lbnUtLWlzT3Blbidcblx0XHR9LFxuXHRcdHN0YXRlOiB7XG5cdFx0XHRjdXJyZW50R3JpZFN0eWxlczogbnVsbCxcblx0XHRcdGN1cnJlbnRQYWdlSW5kZXg6IDAsXG5cdFx0XHRicmVha3BvaW50OiBudWxsLFxuXHRcdFx0c2hvdWxkQW5pbWF0ZTogZmFsc2UsXG5cdFx0XHRldmVudHNEaWRCaW5kOiBmYWxzZSxcblx0XHRcdGFsbG93UGFnZUNoYW5nZTogdHJ1ZSxcblx0XHRcdGdyaWRJdGVtc0Ftb3VudDogMCxcblx0XHRcdGNzc0dhcDogMzIsXG5cdFx0XHR2dzogbnVsbCxcblx0XHRcdHZoOiBudWxsXG5cdFx0fSxcblx0XHRncmlkOiB7XG5cdFx0XHRpdGVtc1BlclJvdzogM1xuXHRcdH1cblx0fSxcblx0XHRlbHMgPSB7fSxcblx0XHRpbml0ID0gZnVuY3Rpb24oKSB7XG5cblx0XHRcdC8vIGRlZmluZSBlbGVtZW50c1xuXHRcdFx0ZWxzLnRoZUdyaWQgPSBkLnF1ZXJ5U2VsZWN0b3Iocy5zZWxlY3RvcnMudGhlR3JpZCk7XG5cdFx0XHRlbHMudGhlR3JpZEl0ZW1zID0gZC5xdWVyeVNlbGVjdG9yQWxsKHMuc2VsZWN0b3JzLnRoZUdyaWRJdGVtcyk7XG5cdFx0XHRlbHMudGhlR3JpZEl0ZW1TZWxlY3RvciA9IGQucXVlcnlTZWxlY3RvckFsbChzLnNlbGVjdG9ycy50aGVHcmlkSXRlbVNlbGVjdG9yKTtcblx0XHRcdGVscy50aGVCb2R5ID0gZC5xdWVyeVNlbGVjdG9yKHMuc2VsZWN0b3JzLnRoZUJvZHkpO1xuXG5cdFx0XHQvLyBzZXQgc3RhdGVcblx0XHRcdHMuc3RhdGUuZ3JpZEl0ZW1zQW1vdW50ID0gZWxzLnRoZUdyaWRJdGVtcy5sZW5ndGg7XG5cblx0XHRcdC8vIGJpbmQgZXZlbnRzXG5cdFx0XHR3LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHJlc2l6ZUl0KTtcblx0XHRcdHcuYWRkRXZlbnRMaXN0ZW5lcigncG9wc3RhdGUnLCBnZXRIaXN0b3J5KTtcblx0XHRcdHcuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIGhhbmRsZUtleURvd24pO1xuXG5cdFx0XHQvLyB0b3VjaCBzdXBwb3J0XG5cdFx0XHRjb25zdCBvcHRpb25zID0geyB0aHJlc2hvbGQ6IDEwMCwgYWxsb3dlZFRpbWU6IDQwMCB9XG5cdFx0XHR3LnRvdWNoSGFuZGxlci53YXRjaChlbHMudGhlR3JpZCwgb3B0aW9ucywgb25Ub3VjaCk7XG5cblx0XHRcdC8vIG5vdCB0aGUgZmFuY3kgbGF5b3V0IC0gYnV6eiBvZlxuXHRcdFx0Y2hlY2tCcmVha3BvaW50KCk7XG5cblx0XHRcdC8vIFNldCBhbmltYXRpb24gdmFsdWUnc1xuXHRcdFx0c2V0QW5pbWF0aW9uVmFsdWVzKCk7XG5cblx0XHRcdC8vIFNjcm9sbCB0byBhY3RpdmUgcGFnZSBvbiBwYWdlbG9hZFxuXHRcdFx0Z2V0QWN0aXZlUGFnZU9uTG9hZCgpO1xuXG5cdFx0fSxcblx0XHRzZXRBbmltYXRpb25WYWx1ZXMgPSBmdW5jdGlvbigpIHtcblxuXHRcdFx0Ly8gbWFpbiBHcmlkIGRlZmF1bHQgcG9zaXRpb25cblx0XHRcdGVscy50aGVHcmlkLnNldEF0dHJpYnV0ZSgnZGF0YS10b3AnLCAwKTtcblx0XHRcdGVscy50aGVHcmlkLnNldEF0dHJpYnV0ZSgnZGF0YS1sZWZ0JywgMCk7XG5cblx0XHRcdC8vIEdyaWQgaXRlbXMgZGVmYXVsdCBwb3NpdGlvbnNcblx0XHRcdGxldCB0aGVSb3cgPSAwLFxuXHRcdFx0XHR0aGVDb2wgPSAwO1xuXG5cdFx0XHRmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgcy5zdGF0ZS5ncmlkSXRlbXNBbW91bnQ7IGluZGV4KyspIHtcblx0XHRcdFx0Ly8gZGVmaW5lIHZhbHVlc1xuXHRcdFx0XHRsZXQgdG9wID0gdGhlUm93O1xuXHRcdFx0XHRsZXQgbGVmdCA9IHRoZUNvbDtcblxuXHRcdFx0XHQvLyBzZXQgdmFsdWVzXG5cdFx0XHRcdGVscy50aGVHcmlkSXRlbXNbaW5kZXhdLnNldEF0dHJpYnV0ZSgnZGF0YS10b3AnLCB0b3ApO1xuXHRcdFx0XHRlbHMudGhlR3JpZEl0ZW1zW2luZGV4XS5zZXRBdHRyaWJ1dGUoJ2RhdGEtbGVmdCcsIGxlZnQpO1xuXG5cdFx0XHRcdC8vIGFkZCBjb3VudFxuXHRcdFx0XHR0aGVDb2wrKztcblx0XHRcdFx0Ly8gaW5jcmVtZW50IHJvdyAvIHJlc2V0IGNvbFxuXHRcdFx0XHRpZiAodGhlQ29sID49IHMuZ3JpZC5pdGVtc1BlclJvdykgeyB0aGVSb3crKzsgdGhlQ29sID0gMDsgfVxuXHRcdFx0fVxuXHRcdH0sXG5cdFx0cmVzaXplSXQgPSBmdW5jdGlvbigpIHtcblxuXHRcdFx0Y2xlYXJUaW1lb3V0KHMudGhlUmVzaXplVGltZXIpO1xuXHRcdFx0cy50aGVSZXNpemVUaW1lciA9IHNldFRpbWVvdXQoY2hlY2tCcmVha3BvaW50LCAxMDApO1xuXG5cdFx0fSxcblx0XHRjaGVja0JyZWFrcG9pbnQgPSBmdW5jdGlvbigpIHtcblxuXHRcdFx0Ly8gU2V0IHN0YXRlXG5cdFx0XHRzLnN0YXRlLnZ3ID0gd2luZG93LmlubmVyV2lkdGg7XG5cdFx0XHRzLnN0YXRlLnZoID0gd2luZG93LmlubmVySGVpZ2h0O1xuXG5cdFx0XHQvLyBHZXQgYnJlYWtwb2ludFxuXHRcdFx0cy5zdGF0ZS5icmVha3BvaW50ID0gdy5nZXRDb21wdXRlZFN0eWxlKGVscy50aGVCb2R5LCAnOmJlZm9yZScpLmdldFByb3BlcnR5VmFsdWUoJ2NvbnRlbnQnKS5yZXBsYWNlKC9cXFwiL2csICcnKTtcblxuXHRcdFx0Ly8gc2V0IHNob3VsZEFuaW1hdGUgc3RhdGVcblx0XHRcdHNldFNob3VsZEFuaW1hdGUoKTtcblxuXHRcdFx0Ly8gaW5pdCBhbmltYXRpb24gZXZlbnRzXG5cdFx0XHRzZXRFdmVudExpc3RlbmVycygpO1xuXG5cdFx0fSxcblx0XHRzZXRTaG91bGRBbmltYXRlID0gZnVuY3Rpb24oKSB7XG5cblx0XHRcdGlmIChzLnN0YXRlLmJyZWFrcG9pbnQgPT09ICd4cycgfHwgcy5zdGF0ZS5icmVha3BvaW50ID09PSAnc20nIHx8IHMuc3RhdGUuYnJlYWtwb2ludCA9PT0gJ21kJykge1xuXHRcdFx0XHRzLnN0YXRlLnNob3VsZEFuaW1hdGUgPSBmYWxzZTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHMuc3RhdGUuc2hvdWxkQW5pbWF0ZSA9IHRydWU7XG5cdFx0XHR9XG5cblx0XHR9LFxuXHRcdHNldEV2ZW50TGlzdGVuZXJzID0gZnVuY3Rpb24oKSB7XG5cblx0XHRcdGlmIChzLnN0YXRlLnNob3VsZEFuaW1hdGUgJiYgIXMuc3RhdGUuZXZlbnRzRGlkQmluZCkge1xuXHRcdFx0XHRmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgcy5zdGF0ZS5ncmlkSXRlbXNBbW91bnQ7IGluZGV4KyspIHtcblx0XHRcdFx0XHRlbHMudGhlR3JpZEl0ZW1zW2luZGV4XS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGhhbmRsZUdyaWRJdGVtQ2xpY2spO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHMuc3RhdGUuZXZlbnRzRGlkQmluZCA9IHRydWU7XG5cblx0XHRcdH0gZWxzZSBpZiAoIXMuc3RhdGUuc2hvdWxkQW5pbWF0ZSAmJiBzLnN0YXRlLmV2ZW50c0RpZEJpbmQpIHtcblx0XHRcdFx0Zm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IHMuc3RhdGUuZ3JpZEl0ZW1zQW1vdW50OyBpbmRleCsrKSB7XG5cdFx0XHRcdFx0ZWxzLnRoZUdyaWRJdGVtc1tpbmRleF0ucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLCBoYW5kbGVHcmlkSXRlbUNsaWNrKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRzLnN0YXRlLmV2ZW50c0RpZEJpbmQgPSBmYWxzZTtcblx0XHRcdH1cblxuXHRcdH0sXG5cdFx0aGFuZGxlR3JpZEl0ZW1DbGljayA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cblx0XHRcdGxldCB0aGVUYXJnZXQgPSBldmVudC50YXJnZXQsXG5cdFx0XHRcdHRoZVRhcmdldENsYXNzZXMgPSB0aGVUYXJnZXQuY2xhc3NMaXN0LFxuXHRcdFx0XHR0aGVHcmlkSXRlbSA9IHRoaXMsXG5cdFx0XHRcdHRoZUdyaWRJdGVtSW5kZXggPSBBcnJheS5wcm90b3R5cGUuaW5kZXhPZi5jYWxsKHRoZUdyaWRJdGVtLnBhcmVudEVsZW1lbnQuY2hpbGRyZW4sIHRoZUdyaWRJdGVtKTtcblxuXHRcdFx0c3dpdGNoICh0cnVlKSB7XG5cdFx0XHRcdC8vIEZvY3VzIGVsZW1lbnRcblx0XHRcdFx0Y2FzZSB0aGVUYXJnZXRDbGFzc2VzLmNvbnRhaW5zKHMuc2VsZWN0b3JzLnRoZUdyaWRJdGVtRm9jdXMucmVwbGFjZSgnLicsICcnKSk6XG5cdFx0XHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHRoYW5kbGVGb2N1cyh0aGVHcmlkSXRlbUluZGV4KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Ly8gU2VsZWN0b3IgZWxlbWVudFxuXHRcdFx0XHRjYXNlIHRoZVRhcmdldENsYXNzZXMuY29udGFpbnMocy5zZWxlY3RvcnMudGhlR3JpZEl0ZW1TZWxlY3Rvci5yZXBsYWNlKCcuJywgJycpKTpcblx0XHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRcdGhhbmRsZVNlbGVjdG9yKCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdC8vIE1lbnUgZWxlbWVudFxuXHRcdFx0XHRjYXNlIHRoZVRhcmdldENsYXNzZXMuY29udGFpbnMocy5zZWxlY3RvcnMudGhlR3JpZEl0ZW1NZW51LnJlcGxhY2UoJy4nLCAnJykpOlxuXHRcdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0aGFuZGxlTWVudSgpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHQvLyBOZXh0IGxpbmsgZWxlbWVudFxuXHRcdFx0XHRjYXNlIHRoZVRhcmdldENsYXNzZXMuY29udGFpbnMocy5zZWxlY3RvcnMudGhlR3JpZEl0ZW1OZXh0TGluay5yZXBsYWNlKCcuJywgJycpKTpcblx0XHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRcdGhhbmRsZU5leHQodGhlR3JpZEl0ZW1JbmRleCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHR9XG5cblx0XHR9LFxuXHRcdGhhbmRsZU5leHQgPSBmdW5jdGlvbihlbGVtZW50SW5kZXgpIHtcblxuXHRcdFx0bGV0IG5leHRHcmlkSXRlbUluZGV4ID0gKChlbGVtZW50SW5kZXggKyAxKSA+PSBzLnN0YXRlLmdyaWRJdGVtc0Ftb3VudCkgPyAwIDogZWxlbWVudEluZGV4ICsgMVxuXHRcdFx0YW5pbWF0ZUdyaWQobmV4dEdyaWRJdGVtSW5kZXgpO1xuXG5cdFx0fSxcblx0XHRoYW5kbGVGb2N1cyA9IGZ1bmN0aW9uKGVsZW1lbnRJbmRleCkge1xuXG5cdFx0XHRhbmltYXRlR3JpZChlbGVtZW50SW5kZXgpO1xuXG5cdFx0fSxcblx0XHRoYW5kbGVTZWxlY3RvciA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0XHRoYW5kbGVNZW51KCk7XG5cblx0XHR9LFxuXHRcdGhhbmRsZU1lbnUgPSBmdW5jdGlvbigpIHtcblxuXHRcdFx0ZWxzLnRoZUJvZHkuY2xhc3NMaXN0LmNvbnRhaW5zKHMuY2xhc3Nlcy5vcGVuTWVudSlcblx0XHRcdFx0PyBlbHMudGhlQm9keS5jbGFzc0xpc3QucmVtb3ZlKHMuY2xhc3Nlcy5vcGVuTWVudSlcblx0XHRcdFx0OiBlbHMudGhlQm9keS5jbGFzc0xpc3QuYWRkKHMuY2xhc3Nlcy5vcGVuTWVudSk7XG5cblx0XHR9LFxuXHRcdGhhbmRsZUtleURvd24gPSBmdW5jdGlvbihldmVudCkge1xuXG5cdFx0XHQvLyBEbyBub3RoaW5nIGlmIHBhZ2UgY2hhbmdlIGlzIHN0aWxsIGFuaW1hdGluZ1xuXHRcdFx0Ly8gRG8gbm90aGluZyBpZiB0aGUgd2UncmUgbm90IGFuaW1hdGluZyBhdCBhbGxcblx0XHRcdC8vIERvIG5vdGhpbmcgaWYgdGhlIGV2ZW50IHdhcyBhbHJlYWR5IHByb2Nlc3NlZFxuXHRcdFx0aWYgKCFzLnN0YXRlLmFsbG93UGFnZUNoYW5nZSB8fCAhcy5zdGF0ZS5zaG91bGRBbmltYXRlIHx8IGV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQpIHsgcmV0dXJuOyB9XG5cblx0XHRcdGxldCBuZXh0UGFnZSA9IHVuZGVmaW5lZDtcblx0XHRcdGxldCBhY3Rpb24gPSB1bmRlZmluZWQ7XG5cblx0XHRcdHN3aXRjaCAoZXZlbnQua2V5KSB7XG5cdFx0XHRcdGNhc2UgXCJEb3duXCI6XG5cdFx0XHRcdGNhc2UgXCJBcnJvd0Rvd25cIjpcblx0XHRcdFx0XHRhY3Rpb24gPSAzO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlIFwiVXBcIjpcblx0XHRcdFx0Y2FzZSBcIkFycm93VXBcIjpcblx0XHRcdFx0XHRhY3Rpb24gPSAtMztcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSBcIkxlZnRcIjpcblx0XHRcdFx0Y2FzZSBcIkFycm93TGVmdFwiOlxuXHRcdFx0XHRcdGFjdGlvbiA9IC0xO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlIFwiUmlnaHRcIjpcblx0XHRcdFx0Y2FzZSBcIkFycm93UmlnaHRcIjpcblx0XHRcdFx0XHRhY3Rpb24gPSAxO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0Ly8gY2FsY3VsYXRlIG5leHQgcGFnZSBpbmRleFxuXHRcdFx0aWYgKHMuc3RhdGUuY3VycmVudFBhZ2VJbmRleCArIGFjdGlvbiA8IDApIHtcblx0XHRcdFx0bmV4dFBhZ2UgPSBzLnN0YXRlLmdyaWRJdGVtc0Ftb3VudCArIChzLnN0YXRlLmN1cnJlbnRQYWdlSW5kZXggKyBhY3Rpb24pO1xuXHRcdFx0fSBlbHNlIGlmIChzLnN0YXRlLmN1cnJlbnRQYWdlSW5kZXggKyBhY3Rpb24gPj0gcy5zdGF0ZS5ncmlkSXRlbXNBbW91bnQpIHtcblx0XHRcdFx0bmV4dFBhZ2UgPSAocy5zdGF0ZS5jdXJyZW50UGFnZUluZGV4ICsgYWN0aW9uKSAtIHMuc3RhdGUuZ3JpZEl0ZW1zQW1vdW50O1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0bmV4dFBhZ2UgPSBzLnN0YXRlLmN1cnJlbnRQYWdlSW5kZXggKyBhY3Rpb247XG5cdFx0XHR9XG5cblx0XHRcdC8vIG5hdmlnYXRlIHRvIHNlbGVjdGVkIHBhZ2Vcblx0XHRcdGFuaW1hdGVHcmlkKG5leHRQYWdlKTtcblxuXHRcdFx0Ly8gQ2FuY2VsIHRoZSBkZWZhdWx0IGFjdGlvbiB0byBhdm9pZCBpdCBiZWluZyBoYW5kbGVkIHR3aWNlXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCAmJiBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdFx0fSxcblx0XHRhbmltYXRlR3JpZCA9IGZ1bmN0aW9uKGVsZW1lbnRJbmRleCwgZXhjbHVkZUZyb21IaXN0b3J5KSB7XG5cblx0XHRcdC8vIGRlbnkgcGFnZWNoYW5nZVxuXHRcdFx0cy5zdGF0ZS5hbGxvd1BhZ2VDaGFuZ2UgPSBmYWxzZTtcblxuXHRcdFx0Ly8gYW5pbWF0ZSBkYXRhXG5cdFx0XHRsZXQgZ3JpZEl0ZW0gPSBlbHMudGhlR3JpZEl0ZW1zW2VsZW1lbnRJbmRleF0sXG5cdFx0XHRcdGdhcCA9IChzLnN0YXRlLmNzc0dhcCAqIDIpICsgJ3B4JyxcblxuXHRcdFx0XHR0b1RvcCA9IGdyaWRJdGVtLmdldEF0dHJpYnV0ZSgnZGF0YS10b3AnKSxcblx0XHRcdFx0dG9MZWZ0ID0gZ3JpZEl0ZW0uZ2V0QXR0cmlidXRlKCdkYXRhLWxlZnQnKSxcblx0XHRcdFx0ZnJvbVRvcCA9IGVscy50aGVHcmlkLmdldEF0dHJpYnV0ZSgnZGF0YS10b3AnKSxcblx0XHRcdFx0ZnJvbUxlZnQgPSBlbHMudGhlR3JpZC5nZXRBdHRyaWJ1dGUoJ2RhdGEtbGVmdCcpO1xuXG5cdFx0XHRhbmltYXRlSXQoe1xuXHRcdFx0XHRkdXJhdGlvbjogTWF0aC5taW4oNzAwLCBNYXRoLm1heCg0NTAsIHMuc3RhdGUudncgLyAyKSksXG5cdFx0XHRcdHRpbWluZyh0aW1lRnJhY3Rpb24pIHtcblx0XHRcdFx0XHQvLyBlYXNlSW5PdXRDdWJpY1xuXHRcdFx0XHRcdHJldHVybiB0aW1lRnJhY3Rpb24gPCAwLjUgPyA0ICogdGltZUZyYWN0aW9uICogdGltZUZyYWN0aW9uICogdGltZUZyYWN0aW9uIDogKHRpbWVGcmFjdGlvbiAtIDEpICogKDIgKiB0aW1lRnJhY3Rpb24gLSAyKSAqICgyICogdGltZUZyYWN0aW9uIC0gMikgKyAxO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRkcmF3KHByb2dyZXNzKSB7XG5cblx0XHRcdFx0XHQvLyByb3VuZCBwcm9ncmVzcyB0byB4IGRlY2ltYWxzXG5cdFx0XHRcdFx0bGV0IHByb2dyZXNzRmxvb3IgPSBNYXRoLmZsb29yKHByb2dyZXNzICogMTAwMCkgLyAxMDAwLFxuXG5cdFx0XHRcdFx0XHQvLyBjYWxjdWxhdGUgYW5pbWF0aW9uIGZyb20gYW5kIHRvIC0gY3VycmVudCB0b3AvbGVmdCB2cyB0YXJnZXQgdG9wL2xlZnQgcG9zaXRpb25cblx0XHRcdFx0XHRcdHRvcFZhbHVlID0gKC1mcm9tVG9wKSArICgtKGZyb21Ub3AgLSB0b1RvcCkgKiBwcm9ncmVzc0Zsb29yKSAqIC0xLFxuXHRcdFx0XHRcdFx0bGVmdFZhbHVlID0gKC1mcm9tTGVmdCkgKyAoLShmcm9tTGVmdCAtIHRvTGVmdCkgKiBwcm9ncmVzc0Zsb29yKSAqIC0xLFxuXG5cdFx0XHRcdFx0XHQvLyBhbmltYXRpb24gdGVtcGxhdGVzXG5cdFx0XHRcdFx0XHRhbmltYXRpb25UZW1wbGF0ZVRvcCA9IGB0b3A6IGNhbGMoJHt0b3BWYWx1ZX0gKiAoMTAwdmggLSAke2dhcH0pIClgLFxuXHRcdFx0XHRcdFx0YW5pbWF0aW9uVGVtcGxhdGVMZWZ0ID0gYGxlZnQ6IGNhbGMoJHtsZWZ0VmFsdWV9ICogKDEwMHZ3IC0gJHtnYXB9KSApYDtcblxuXHRcdFx0XHRcdC8vIGFuaW1hdGUgaXRcblx0XHRcdFx0XHRlbHMudGhlR3JpZC5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgYW5pbWF0aW9uVGVtcGxhdGVUb3AgKyAnOyAnICsgYW5pbWF0aW9uVGVtcGxhdGVMZWZ0ICsgJzsgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC40cyBlYXNlLWluOycpO1xuXG5cdFx0XHRcdFx0Ly8gc2F2ZSBzdHlsZXNcblx0XHRcdFx0XHRzLnN0YXRlLmN1cnJlbnRHcmlkU3R5bGVzID0gYW5pbWF0aW9uVGVtcGxhdGVUb3AgKyAnOyAnICsgYW5pbWF0aW9uVGVtcGxhdGVMZWZ0ICsgJzsgJztcblx0XHRcdFx0fSxcblx0XHRcdFx0YW5pbWF0aW9uRW5kKCkge1xuXHRcdFx0XHRcdC8vIHNldC9yZW1vdmUgY2xhc3Nlc1xuXHRcdFx0XHRcdGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCBzLnN0YXRlLmdyaWRJdGVtc0Ftb3VudDsgaW5kZXgrKykgeyBlbHMudGhlR3JpZEl0ZW1zW2luZGV4XS5jbGFzc0xpc3QucmVtb3ZlKHMuY2xhc3Nlcy5pc0FjdGl2ZUdyaWRJdGVtKTsgfVxuXHRcdFx0XHRcdGVscy50aGVHcmlkSXRlbXNbZWxlbWVudEluZGV4XS5jbGFzc0xpc3QuYWRkKHMuY2xhc3Nlcy5pc0FjdGl2ZUdyaWRJdGVtKTtcblxuXHRcdFx0XHRcdC8vIGFsbG93IHBhZ2VjaGFuZ2Vcblx0XHRcdFx0XHRzLnN0YXRlLmFsbG93UGFnZUNoYW5nZSA9IHRydWU7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXG5cdFx0XHQvLyBzZXQgJ2Zyb20nIHBvc2l0aW9uIGZvciB0aGUgbmV4dCBhbmltYXRpb25cblx0XHRcdGVscy50aGVHcmlkLnNldEF0dHJpYnV0ZSgnZGF0YS10b3AnLCB0b1RvcCk7XG5cdFx0XHRlbHMudGhlR3JpZC5zZXRBdHRyaWJ1dGUoJ2RhdGEtbGVmdCcsIHRvTGVmdCk7XG5cblx0XHRcdC8vIHNldEhpc3Rvcnlcblx0XHRcdCFleGNsdWRlRnJvbUhpc3RvcnkgJiYgc2V0SGlzdG9yeShlbGVtZW50SW5kZXgpO1xuXG5cdFx0XHQvLyBzZXQgc3RhdGVcblx0XHRcdHMuc3RhdGUuY3VycmVudFBhZ2VJbmRleCA9IGVsZW1lbnRJbmRleDtcblxuXHRcdH0sXG5cdFx0c2V0SGlzdG9yeSA9IGZ1bmN0aW9uKHBhZ2VJbmRleCkge1xuXG5cdFx0XHRoaXN0b3J5LnB1c2hTdGF0ZShwYWdlSW5kZXgsIG51bGwsIGA/cGFnaW5hPSR7cGFnZUluZGV4fWApO1xuXG5cdFx0fSxcblx0XHRnZXRIaXN0b3J5ID0gZnVuY3Rpb24oKSB7XG5cblx0XHRcdGNvbnN0IGVsZW1lbnRJbmRleCA9IHcuaGlzdG9yeS5zdGF0ZSB8fCAwO1xuXHRcdFx0ZWxlbWVudEluZGV4ID8gYW5pbWF0ZUdyaWQoZWxlbWVudEluZGV4LCB0cnVlKSA6IGFuaW1hdGVHcmlkKGVsZW1lbnRJbmRleCk7XG5cblx0XHR9LFxuXHRcdGdldEFjdGl2ZVBhZ2VPbkxvYWQgPSBmdW5jdGlvbigpIHtcblxuXHRcdFx0aWYgKHcuaGlzdG9yeS5zdGF0ZSkge1xuXHRcdFx0XHRjb25zdCBlbGVtZW50SW5kZXggPSB3Lmhpc3Rvcnkuc3RhdGU7XG5cdFx0XHRcdGFuaW1hdGVHcmlkKGVsZW1lbnRJbmRleCk7XG5cdFx0XHR9XG5cblx0XHR9LFxuXHRcdG9uVG91Y2ggPSBmdW5jdGlvbihldmVudCwgZ2VzdHVyZURpcmVjdGlvbiwgZ2VzdHVyZVBoYXNlLCBnZXN0dXJlVHlwZSwgdG91Y2hEaXN0YW5jZSkge1xuXG5cdFx0XHQvLyB0b3VjaCBtb3ZlIGFuaW1hdGlvblxuXHRcdFx0aGFuZGxlVG91Y2hNb3ZlQW5pbWF0aW9uKGdlc3R1cmVEaXJlY3Rpb24sIGdlc3R1cmVQaGFzZSwgZ2VzdHVyZVR5cGUsIHRvdWNoRGlzdGFuY2UpO1xuXG5cdFx0XHQvLyBtb2JpbGUgLSBoYW5kbGUgbWVudSBvcGVuIGFuZCBjbG9zZVxuXHRcdFx0aGFuZGxlTWVudVN3aXBlcyhnZXN0dXJlVHlwZSk7XG5cblx0XHRcdC8vIHN0b3J5IHRlbGxpbmcgbGF5b3V0IC0gaGFuZGxlIHN3aXBlIGdlc3R1cmVzXG5cdFx0XHRoYW5kbGVTd2lwZUdlc3R1cmVzKGdlc3R1cmVUeXBlKTtcblxuXHRcdH0sXG5cdFx0aGFuZGxlVG91Y2hNb3ZlQW5pbWF0aW9uID0gZnVuY3Rpb24oZ2VzdHVyZURpcmVjdGlvbiwgZ2VzdHVyZVBoYXNlLCBnZXN0dXJlVHlwZSwgdG91Y2hEaXN0YW5jZSkge1xuXG5cdFx0XHQvLyB2YXJpYWJsZXNcblx0XHRcdGNvbnN0IGN1cnJlbnRHcmlkU3R5bGVzID0gcy5zdGF0ZS5jdXJyZW50R3JpZFN0eWxlcyB8fCAnJztcblx0XHRcdGxldCBheGlzID0gJ1gnO1xuXG5cdFx0XHQvLyBzZXQgYW5pbWF0aW9uIG9uIHRvdWNobW92ZVxuXHRcdFx0aWYgKHMuc3RhdGUuc2hvdWxkQW5pbWF0ZSAmJiBnZXN0dXJlUGhhc2UgPT09ICd0b3VjaG1vdmUnKSB7XG5cblx0XHRcdFx0Ly8gZGVmaW5lIHRyYW5zbGF0ZSBheGlzXG5cdFx0XHRcdHN3aXRjaCAoZ2VzdHVyZURpcmVjdGlvbikge1xuXHRcdFx0XHRcdGNhc2UgJ2xlZnQnOlxuXHRcdFx0XHRcdGNhc2UgJ3JpZ2h0Jzpcblx0XHRcdFx0XHRcdGF4aXMgPSAnWCdcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGNhc2UgJ3VwJzpcblx0XHRcdFx0XHRjYXNlICdkb3duJzpcblx0XHRcdFx0XHRcdGF4aXMgPSAnWSdcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vIGFwcGx5IHRyYW5zbGF0ZVxuXHRcdFx0XHRsZXQgYW5pbWF0aW9uVGVtcGxhdGUgPSBgdHJhbnNmb3JtOiB0cmFuc2xhdGUke2F4aXN9KCR7dG91Y2hEaXN0YW5jZSAvIDEwfXB4KWA7XG5cdFx0XHRcdGVscy50aGVHcmlkLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBjdXJyZW50R3JpZFN0eWxlcyArIGFuaW1hdGlvblRlbXBsYXRlKTtcblx0XHRcdH1cblxuXHRcdFx0Ly8gZW5kIGFuaW1hdGlvbiBvbiB0b3VjaGVuZCBvciB0b3VjaGNhbmNlbCAtIHJlc2V0IHRyYW5zbGF0ZVxuXHRcdFx0aWYgKHMuc3RhdGUuc2hvdWxkQW5pbWF0ZSAmJiAoZ2VzdHVyZVBoYXNlID09PSAndG91Y2hlbmQnIHx8IGdlc3R1cmVQaGFzZSA9PT0gJ3RvdWNoY2FuY2VsJykpIHtcblx0XHRcdFx0bGV0IGFuaW1hdGlvblRlbXBsYXRlID0gYHRyYW5zZm9ybTogdHJhbnNsYXRlJHtheGlzfSgwZGVnKTsgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC40cyBlYXNlLWluO2A7XG5cdFx0XHRcdGVscy50aGVHcmlkLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBjdXJyZW50R3JpZFN0eWxlcyArIGFuaW1hdGlvblRlbXBsYXRlKTtcblx0XHRcdH1cblxuXHRcdH0sXG5cdFx0aGFuZGxlTWVudVN3aXBlcyA9IGZ1bmN0aW9uKGdlc3R1cmVUeXBlKSB7XG5cblx0XHRcdGlmICghcy5zdGF0ZS5zaG91bGRBbmltYXRlKSB7XG5cdFx0XHRcdHN3aXRjaCAoZ2VzdHVyZVR5cGUpIHtcblx0XHRcdFx0XHRjYXNlICdzd2lwZVJpZ2h0Jzpcblx0XHRcdFx0XHRcdGVscy50aGVCb2R5LmNsYXNzTGlzdC5hZGQocy5jbGFzc2VzLm9wZW5NZW51KTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGNhc2UgJ3N3aXBlTGVmdCc6XG5cdFx0XHRcdFx0XHRlbHMudGhlQm9keS5jbGFzc0xpc3QucmVtb3ZlKHMuY2xhc3Nlcy5vcGVuTWVudSk7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHR9LFxuXHRcdGhhbmRsZVN3aXBlR2VzdHVyZXMgPSBmdW5jdGlvbihnZXN0dXJlVHlwZSkge1xuXG5cdFx0XHRpZiAocy5zdGF0ZS5zaG91bGRBbmltYXRlICYmIGdlc3R1cmVUeXBlICYmIGdlc3R1cmVUeXBlICE9PSAndGFwJykge1xuXG5cdFx0XHRcdC8vIHNpbXVsYXRlIGtleWRvd24gZXZlbnRcblx0XHRcdFx0bGV0IHNpbXVsYXRlZEV2ZW50ID0ge1xuXHRcdFx0XHRcdGtleTogJ3N3aXBlTGVmdCdcblx0XHRcdFx0fTtcblxuXHRcdFx0XHRzd2l0Y2ggKGdlc3R1cmVUeXBlKSB7XG5cdFx0XHRcdFx0Y2FzZSAnc3dpcGVVcCc6XG5cdFx0XHRcdFx0XHRzaW11bGF0ZWRFdmVudC5rZXkgPSAnRG93bic7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlICdzd2lwZVJpZ2h0Jzpcblx0XHRcdFx0XHRcdHNpbXVsYXRlZEV2ZW50LmtleSA9ICdMZWZ0Jztcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGNhc2UgJ3N3aXBlRG93bic6XG5cdFx0XHRcdFx0XHRzaW11bGF0ZWRFdmVudC5rZXkgPSAnVXAnO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0Y2FzZSAnc3dpcGVMZWZ0Jzpcblx0XHRcdFx0XHRcdHNpbXVsYXRlZEV2ZW50LmtleSA9ICdSaWdodCc7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly8gc2ltdWxhdGUgaXRcblx0XHRcdFx0aGFuZGxlS2V5RG93bihzaW11bGF0ZWRFdmVudClcblx0XHRcdH1cblxuXHRcdH07XG5cblx0cmV0dXJuIHtcblx0XHRpbml0OiBpbml0LFxuXHRcdGhhbmRsZU1lbnU6IGhhbmRsZU1lbnUsXG5cdFx0YW5pbWF0ZUdyaWQ6IGFuaW1hdGVHcmlkXG5cdH07XG5cbn0od2luZG93LCB3aW5kb3cuZG9jdW1lbnQpKTtcbiIsIi8qXG5cbkhhbmRsZSBNZW51IGV2ZW50c1xuXG4qL1xuXG5jb25zdCBtZW51ID0gKGZ1bmN0aW9uKHcsIGQsIHVuZGVmaW5lZCkge1xuXG5cdCd1c2Ugc3RyaWN0JztcblxuXHRjb25zdCBzID0ge1xuXHRcdHNlbGVjdG9yczoge1xuXHRcdFx0dGhlTWVudTogJy5qcy1tZW51Jyxcblx0XHRcdHRoZU1lbnVUcmlnZ2VyOiAnLmpzLWJ1dHRvbi0tbWVudScsXG5cdFx0XHR0aGVOYXZMaW5rczogJy5qcy1uYXZpZ2F0aW9uX19saW5rJyxcblx0XHRcdHRoZUJvZHk6ICdib2R5J1xuXHRcdH0sXG5cdFx0c3RhdGU6IHtcblx0XHRcdGJyZWFrcG9pbnQ6IG51bGwsXG5cdFx0XHRicmVha3BvaW50Q2hhbmdlZDogZmFsc2UsXG5cdFx0XHRpc0ZhbmN5TGF5b3V0OiBmYWxzZSxcblx0XHRcdG1lbnVUcmlnZ2VyQW1vdW50OiAwLFxuXHRcdFx0bGlua0l0ZW1zQW1vdW50OiAwXG5cdFx0fSxcblx0XHRjbGFzc2VzOiB7XG5cdFx0XHRvcGVuTWVudTogJ2pzLW1lbnUtLWlzT3Blbidcblx0XHR9XG5cdH0sXG5cdFx0ZWxzID0ge30sXG5cdFx0aW5pdCA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0XHQvLyBkZWZpbmUgZWxlbWVudHNcblx0XHRcdGVscy50aGVNZW51ID0gZC5xdWVyeVNlbGVjdG9yKHMuc2VsZWN0b3JzLnRoZU1lbnUpO1xuXHRcdFx0ZWxzLnRoZU1lbnVUcmlnZ2VyID0gZC5xdWVyeVNlbGVjdG9yQWxsKHMuc2VsZWN0b3JzLnRoZU1lbnVUcmlnZ2VyKTtcblx0XHRcdGVscy50aGVOYXZMaW5rcyA9IGQucXVlcnlTZWxlY3RvckFsbChzLnNlbGVjdG9ycy50aGVOYXZMaW5rcyk7XG5cdFx0XHRlbHMudGhlQm9keSA9IGQucXVlcnlTZWxlY3RvcihzLnNlbGVjdG9ycy50aGVCb2R5KTtcblxuXHRcdFx0Ly8gc2V0IHN0YXRlXG5cdFx0XHRzLnN0YXRlLm1lbnVUcmlnZ2VyQW1vdW50ID0gZWxzLnRoZU1lbnVUcmlnZ2VyLmxlbmd0aDtcblx0XHRcdHMuc3RhdGUubGlua0l0ZW1zQW1vdW50ID0gZWxzLnRoZU5hdkxpbmtzLmxlbmd0aDtcblxuXHRcdFx0Ly8gdGhlIGJpbmRcblx0XHRcdC8vIFRvZ2dlbCBtZW51IHRyaWdnZXJzXG5cdFx0XHRmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgcy5zdGF0ZS5tZW51VHJpZ2dlckFtb3VudDsgaW5kZXgrKykge1xuXHRcdFx0XHRlbHMudGhlTWVudVRyaWdnZXJbaW5kZXhdLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4geyB3LmdyaWQuaGFuZGxlTWVudSgpOyB9KTtcblx0XHRcdH1cblx0XHRcdC8vIE1lbnUgbGluayBJdGVtc1xuXHRcdFx0Zm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IHMuc3RhdGUubGlua0l0ZW1zQW1vdW50OyBpbmRleCsrKSB7XG5cdFx0XHRcdGVscy50aGVOYXZMaW5rc1tpbmRleF0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBoYW5kbGVNZW51TGlua0NsaWNrKTtcblx0XHRcdH1cblxuXHRcdFx0Ly8gdG91Y2ggc3VwcG9ydFxuXHRcdFx0Y29uc3Qgb3B0aW9ucyA9IHsgdGhyZXNob2xkOiAxMDAsIGFsbG93ZWRUaW1lOiA0MDAgfVxuXHRcdFx0dy50b3VjaEhhbmRsZXIud2F0Y2goZWxzLnRoZU1lbnUsIG9wdGlvbnMsIG9uVG91Y2gpO1xuXG5cdFx0XHQvLyBSZXNpemVcblx0XHRcdHcuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgcmVzaXplSXQpO1xuXHRcdFx0Y2hlY2tCcmVha3BvaW50KCk7XG5cblx0XHR9LFxuXHRcdHJlc2l6ZUl0ID0gZnVuY3Rpb24oKSB7XG5cblx0XHRcdGNsZWFyVGltZW91dChzLnRoZVJlc2l6ZVRpbWVyKTtcblx0XHRcdHMudGhlUmVzaXplVGltZXIgPSBzZXRUaW1lb3V0KGNoZWNrQnJlYWtwb2ludCwgMTAwKTtcblxuXHRcdH0sXG5cdFx0Y2hlY2tCcmVha3BvaW50ID0gZnVuY3Rpb24oKSB7XG5cblx0XHRcdC8vIEdldCBicmVha3BvaW50XG5cdFx0XHRzLnN0YXRlLmJyZWFrcG9pbnQgPSB3LmdldENvbXB1dGVkU3R5bGUoZWxzLnRoZUJvZHksICc6YmVmb3JlJykuZ2V0UHJvcGVydHlWYWx1ZSgnY29udGVudCcpLnJlcGxhY2UoL1xcXCIvZywgJycpO1xuXG5cdFx0XHQvLyBzZXQgaXNGYW5jeUxheW91dCBzdGF0ZVxuXHRcdFx0aWYgKHMuc3RhdGUuYnJlYWtwb2ludCA9PT0gJ3hzJyB8fCBzLnN0YXRlLmJyZWFrcG9pbnQgPT09ICdzbScgfHwgcy5zdGF0ZS5icmVha3BvaW50ID09PSAnbWQnKSB7XG5cdFx0XHRcdHMuc3RhdGUuaXNGYW5jeUxheW91dCA9IGZhbHNlO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cy5zdGF0ZS5pc0ZhbmN5TGF5b3V0ID0gdHJ1ZTtcblx0XHRcdH1cblx0XHR9LFxuXHRcdGhhbmRsZU1lbnVMaW5rQ2xpY2sgPSBmdW5jdGlvbihldmVudCkge1xuXG5cdFx0XHQvLyB2YXJpYWJsZXNcblx0XHRcdGxldCBlbGVtZW50ID0gdGhpcyxcblx0XHRcdFx0dGhlVGFyZ2V0ID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJocmVmXCIpLFxuXHRcdFx0XHR0aGVHcmlkSXRlbSA9IGQucXVlcnlTZWxlY3Rvcih0aGVUYXJnZXQpLFxuXHRcdFx0XHR0aGVHcmlkSXRlbUluZGV4ID0gQXJyYXkucHJvdG90eXBlLmluZGV4T2YuY2FsbCh0aGVHcmlkSXRlbS5wYXJlbnRFbGVtZW50LmNoaWxkcmVuLCB0aGVHcmlkSXRlbSk7XG5cblxuXHRcdFx0Ly8gZmFuY3kgbGF5b3V0XG5cdFx0XHRpZiAocy5zdGF0ZS5pc0ZhbmN5TGF5b3V0KSB7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdHcuZ3JpZC5hbmltYXRlR3JpZCh0aGVHcmlkSXRlbUluZGV4KTtcblx0XHRcdH1cblx0XHR9LFxuXHRcdG9uVG91Y2ggPSBmdW5jdGlvbihldmVudCwgZ2VzdHVyZURpcmVjdGlvbiwgZ2VzdHVyZVBoYXNlLCBnZXN0dXJlVHlwZSwgdG91Y2hEaXN0YW5jZSkge1xuXG5cdFx0XHRpZiAoZ2VzdHVyZVR5cGUgPT09ICdzd2lwZUxlZnQnKSB7XG5cdFx0XHRcdGVscy50aGVCb2R5LmNsYXNzTGlzdC5yZW1vdmUocy5jbGFzc2VzLm9wZW5NZW51KTtcblx0XHRcdH1cblx0XHR9O1xuXG5cdHJldHVybiB7XG5cdFx0aW5pdDogaW5pdFxuXHR9O1xuXG59KHdpbmRvdywgd2luZG93LmRvY3VtZW50KSk7XG4iXX0=