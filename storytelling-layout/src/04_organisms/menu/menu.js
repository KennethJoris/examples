/*

Handle Menu events

*/

const menu = (function(w, d, undefined) {

	'use strict';

	const s = {
		selectors: {
			theMenu: '.js-menu',
			theMenuTrigger: '.js-button--menu',
			theNavLinks: '.js-navigation__link',
			theBody: 'body'
		},
		state: {
			breakpoint: null,
			breakpointChanged: false,
			isFancyLayout: false,
			menuTriggerAmount: 0,
			linkItemsAmount: 0
		},
		classes: {
			openMenu: 'js-menu--isOpen'
		}
	},
		els = {},
		init = function() {

			// define elements
			els.theMenu = d.querySelector(s.selectors.theMenu);
			els.theMenuTrigger = d.querySelectorAll(s.selectors.theMenuTrigger);
			els.theNavLinks = d.querySelectorAll(s.selectors.theNavLinks);
			els.theBody = d.querySelector(s.selectors.theBody);

			// set state
			s.state.menuTriggerAmount = els.theMenuTrigger.length;
			s.state.linkItemsAmount = els.theNavLinks.length;

			// the bind
			// Toggel menu triggers
			for (let index = 0; index < s.state.menuTriggerAmount; index++) {
				els.theMenuTrigger[index].addEventListener('click', () => { w.grid.handleMenu(); });
			}
			// Menu link Items
			for (let index = 0; index < s.state.linkItemsAmount; index++) {
				els.theNavLinks[index].addEventListener('click', handleMenuLinkClick);
			}

			// touch support
			const options = { threshold: 100, allowedTime: 400 }
			w.touchHandler.watch(els.theMenu, options, onTouch);

			// Resize
			w.addEventListener('resize', resizeIt);
			checkBreakpoint();

		},
		resizeIt = function() {

			clearTimeout(s.theResizeTimer);
			s.theResizeTimer = setTimeout(checkBreakpoint, 100);

		},
		checkBreakpoint = function() {

			// Get breakpoint
			s.state.breakpoint = w.getComputedStyle(els.theBody, ':before').getPropertyValue('content').replace(/\"/g, '');

			// set isFancyLayout state
			if (s.state.breakpoint === 'xs' || s.state.breakpoint === 'sm' || s.state.breakpoint === 'md') {
				s.state.isFancyLayout = false;
			} else {
				s.state.isFancyLayout = true;
			}
		},
		handleMenuLinkClick = function(event) {

			// variables
			let element = this,
				theTarget = element.getAttribute("href"),
				theGridItem = d.querySelector(theTarget),
				theGridItemIndex = Array.prototype.indexOf.call(theGridItem.parentElement.children, theGridItem);


			// fancy layout
			if (s.state.isFancyLayout) {
				event.preventDefault();
				w.grid.animateGrid(theGridItemIndex);
			}
		},
		onTouch = function(event, gestureDirection, gesturePhase, gestureType, touchDistance) {

			if (gestureType === 'swipeLeft') {
				els.theBody.classList.remove(s.classes.openMenu);
			}
		};

	return {
		init: init
	};

}(window, window.document));
