/*

Handle destop layout

*/

const grid = (function(w, d, undefined) {

	'use strict';

	const s = {
		selectors: {
			theGrid: '.js-grid',
			theGridItems: '.js-grid__item',
			theGridItemSelector: '.js-grid__item-selector',
			theGridItemFocus: '.js-grid__item-focus',
			theGridItemMenu: '.js-grid__item__menu',
			theGridItemNextLink: '.js-grid__item__link__next',
			theBody: 'body'
		},
		classes: {
			isActiveGridItem: 'js-grid__item--active',
			openMenu: 'js-menu--isOpen'
		},
		state: {
			currentGridStyles: null,
			currentPageIndex: 0,
			breakpoint: null,
			shouldAnimate: false,
			eventsDidBind: false,
			allowPageChange: true,
			gridItemsAmount: 0,
			cssGap: 32,
			vw: null,
			vh: null
		},
		grid: {
			itemsPerRow: 3
		}
	},
		els = {},
		init = function() {

			// define elements
			els.theGrid = d.querySelector(s.selectors.theGrid);
			els.theGridItems = d.querySelectorAll(s.selectors.theGridItems);
			els.theGridItemSelector = d.querySelectorAll(s.selectors.theGridItemSelector);
			els.theBody = d.querySelector(s.selectors.theBody);

			// set state
			s.state.gridItemsAmount = els.theGridItems.length;

			// bind events
			w.addEventListener('resize', resizeIt);
			w.addEventListener('popstate', getHistory);
			w.addEventListener('keydown', handleKeyDown);

			// touch support
			const options = { threshold: 100, allowedTime: 400 }
			w.touchHandler.watch(els.theGrid, options, onTouch);

			// not the fancy layout - buzz of
			checkBreakpoint();

			// Set animation value's
			setAnimationValues();

			// Scroll to active page on pageload
			getActivePageOnLoad();

		},
		setAnimationValues = function() {

			// main Grid default position
			els.theGrid.setAttribute('data-top', 0);
			els.theGrid.setAttribute('data-left', 0);

			// Grid items default positions
			let theRow = 0,
				theCol = 0;

			for (let index = 0; index < s.state.gridItemsAmount; index++) {
				// define values
				let top = theRow;
				let left = theCol;

				// set values
				els.theGridItems[index].setAttribute('data-top', top);
				els.theGridItems[index].setAttribute('data-left', left);

				// add count
				theCol++;
				// increment row / reset col
				if (theCol >= s.grid.itemsPerRow) { theRow++; theCol = 0; }
			}
		},
		resizeIt = function() {

			clearTimeout(s.theResizeTimer);
			s.theResizeTimer = setTimeout(checkBreakpoint, 100);

		},
		checkBreakpoint = function() {

			// Set state
			s.state.vw = window.innerWidth;
			s.state.vh = window.innerHeight;

			// Get breakpoint
			s.state.breakpoint = w.getComputedStyle(els.theBody, ':before').getPropertyValue('content').replace(/\"/g, '');

			// set shouldAnimate state
			setShouldAnimate();

			// init animation events
			setEventListeners();

		},
		setShouldAnimate = function() {

			if (s.state.breakpoint === 'xs' || s.state.breakpoint === 'sm' || s.state.breakpoint === 'md') {
				s.state.shouldAnimate = false;
			} else {
				s.state.shouldAnimate = true;
			}

		},
		setEventListeners = function() {

			if (s.state.shouldAnimate && !s.state.eventsDidBind) {
				for (let index = 0; index < s.state.gridItemsAmount; index++) {
					els.theGridItems[index].addEventListener('click', handleGridItemClick);
				}
				s.state.eventsDidBind = true;

			} else if (!s.state.shouldAnimate && s.state.eventsDidBind) {
				for (let index = 0; index < s.state.gridItemsAmount; index++) {
					els.theGridItems[index].removeEventListener('click', handleGridItemClick);
				}
				s.state.eventsDidBind = false;
			}

		},
		handleGridItemClick = function(event) {

			let theTarget = event.target,
				theTargetClasses = theTarget.classList,
				theGridItem = this,
				theGridItemIndex = Array.prototype.indexOf.call(theGridItem.parentElement.children, theGridItem);

			switch (true) {
				// Focus element
				case theTargetClasses.contains(s.selectors.theGridItemFocus.replace('.', '')):
					event.preventDefault();
					handleFocus(theGridItemIndex);
					break;
				// Selector element
				case theTargetClasses.contains(s.selectors.theGridItemSelector.replace('.', '')):
					event.preventDefault();
					handleSelector();
					break;
				// Menu element
				case theTargetClasses.contains(s.selectors.theGridItemMenu.replace('.', '')):
					event.preventDefault();
					handleMenu();
					break;
				// Next link element
				case theTargetClasses.contains(s.selectors.theGridItemNextLink.replace('.', '')):
					event.preventDefault();
					handleNext(theGridItemIndex);
					break;
				default:
					break;
			}

		},
		handleNext = function(elementIndex) {

			let nextGridItemIndex = ((elementIndex + 1) >= s.state.gridItemsAmount) ? 0 : elementIndex + 1
			animateGrid(nextGridItemIndex);

		},
		handleFocus = function(elementIndex) {

			animateGrid(elementIndex);

		},
		handleSelector = function() {

			handleMenu();

		},
		handleMenu = function() {

			els.theBody.classList.contains(s.classes.openMenu)
				? els.theBody.classList.remove(s.classes.openMenu)
				: els.theBody.classList.add(s.classes.openMenu);

		},
		handleKeyDown = function(event) {

			// Do nothing if page change is still animating
			// Do nothing if the we're not animating at all
			// Do nothing if the event was already processed
			if (!s.state.allowPageChange || !s.state.shouldAnimate || event.defaultPrevented) { return; }

			let nextPage = undefined;
			let action = undefined;

			switch (event.key) {
				case "Down":
				case "ArrowDown":
					action = 3;
					break;
				case "Up":
				case "ArrowUp":
					action = -3;
					break;
				case "Left":
				case "ArrowLeft":
					action = -1;
					break;
				case "Right":
				case "ArrowRight":
					action = 1;
					break;
				default:
					return;
			}

			// calculate next page index
			if (s.state.currentPageIndex + action < 0) {
				nextPage = s.state.gridItemsAmount + (s.state.currentPageIndex + action);
			} else if (s.state.currentPageIndex + action >= s.state.gridItemsAmount) {
				nextPage = (s.state.currentPageIndex + action) - s.state.gridItemsAmount;
			} else {
				nextPage = s.state.currentPageIndex + action;
			}

			// navigate to selected page
			animateGrid(nextPage);

			// Cancel the default action to avoid it being handled twice
			event.preventDefault && event.preventDefault();

		},
		animateGrid = function(elementIndex, excludeFromHistory) {

			// deny pagechange
			s.state.allowPageChange = false;

			// animate data
			let gridItem = els.theGridItems[elementIndex],
				gap = (s.state.cssGap * 2) + 'px',

				toTop = gridItem.getAttribute('data-top'),
				toLeft = gridItem.getAttribute('data-left'),
				fromTop = els.theGrid.getAttribute('data-top'),
				fromLeft = els.theGrid.getAttribute('data-left');

			animateIt({
				duration: Math.min(700, Math.max(450, s.state.vw / 2)),
				timing(timeFraction) {
					// easeInOutCubic
					return timeFraction < 0.5 ? 4 * timeFraction * timeFraction * timeFraction : (timeFraction - 1) * (2 * timeFraction - 2) * (2 * timeFraction - 2) + 1;
				},
				draw(progress) {

					// round progress to x decimals
					let progressFloor = Math.floor(progress * 1000) / 1000,

						// calculate animation from and to - current top/left vs target top/left position
						topValue = (-fromTop) + (-(fromTop - toTop) * progressFloor) * -1,
						leftValue = (-fromLeft) + (-(fromLeft - toLeft) * progressFloor) * -1,

						// animation templates
						animationTemplateTop = `top: calc(${topValue} * (100vh - ${gap}) )`,
						animationTemplateLeft = `left: calc(${leftValue} * (100vw - ${gap}) )`;

					// animate it
					els.theGrid.setAttribute('style', animationTemplateTop + '; ' + animationTemplateLeft + '; transition: transform .4s ease-in;');

					// save styles
					s.state.currentGridStyles = animationTemplateTop + '; ' + animationTemplateLeft + '; ';
				},
				animationEnd() {
					// set/remove classes
					for (let index = 0; index < s.state.gridItemsAmount; index++) { els.theGridItems[index].classList.remove(s.classes.isActiveGridItem); }
					els.theGridItems[elementIndex].classList.add(s.classes.isActiveGridItem);

					// allow pagechange
					s.state.allowPageChange = true;
				}
			});

			// set 'from' position for the next animation
			els.theGrid.setAttribute('data-top', toTop);
			els.theGrid.setAttribute('data-left', toLeft);

			// setHistory
			!excludeFromHistory && setHistory(elementIndex);

			// set state
			s.state.currentPageIndex = elementIndex;

		},
		setHistory = function(pageIndex) {

			history.pushState(pageIndex, null, `?pagina=${pageIndex}`);

		},
		getHistory = function() {

			const elementIndex = w.history.state || 0;
			elementIndex ? animateGrid(elementIndex, true) : animateGrid(elementIndex);

		},
		getActivePageOnLoad = function() {

			if (w.history.state) {
				const elementIndex = w.history.state;
				animateGrid(elementIndex);
			}

		},
		onTouch = function(event, gestureDirection, gesturePhase, gestureType, touchDistance) {

			// touch move animation
			handleTouchMoveAnimation(gestureDirection, gesturePhase, gestureType, touchDistance);

			// mobile - handle menu open and close
			handleMenuSwipes(gestureType);

			// story telling layout - handle swipe gestures
			handleSwipeGestures(gestureType);

		},
		handleTouchMoveAnimation = function(gestureDirection, gesturePhase, gestureType, touchDistance) {

			// variables
			const currentGridStyles = s.state.currentGridStyles || '';
			let axis = 'X';

			// set animation on touchmove
			if (s.state.shouldAnimate && gesturePhase === 'touchmove') {

				// define translate axis
				switch (gestureDirection) {
					case 'left':
					case 'right':
						axis = 'X'
						break;
					case 'up':
					case 'down':
						axis = 'Y'
						break;
					default:
						break;
				}

				// apply translate
				let animationTemplate = `transform: translate${axis}(${touchDistance / 10}px)`;
				els.theGrid.setAttribute('style', currentGridStyles + animationTemplate);
			}

			// end animation on touchend or touchcancel - reset translate
			if (s.state.shouldAnimate && (gesturePhase === 'touchend' || gesturePhase === 'touchcancel')) {
				let animationTemplate = `transform: translate${axis}(0deg); transition: transform .4s ease-in;`;
				els.theGrid.setAttribute('style', currentGridStyles + animationTemplate);
			}

		},
		handleMenuSwipes = function(gestureType) {

			if (!s.state.shouldAnimate) {
				switch (gestureType) {
					case 'swipeRight':
						els.theBody.classList.add(s.classes.openMenu);
						break;
					case 'swipeLeft':
						els.theBody.classList.remove(s.classes.openMenu);
						break;
					default:
						return;
				}
			}

		},
		handleSwipeGestures = function(gestureType) {

			if (s.state.shouldAnimate && gestureType && gestureType !== 'tap') {

				// simulate keydown event
				let simulatedEvent = {
					key: 'swipeLeft'
				};

				switch (gestureType) {
					case 'swipeUp':
						simulatedEvent.key = 'Down';
						break;
					case 'swipeRight':
						simulatedEvent.key = 'Left';
						break;
					case 'swipeDown':
						simulatedEvent.key = 'Up';
						break;
					case 'swipeLeft':
						simulatedEvent.key = 'Right';
						break;
					default:
						return;
				}

				// simulate it
				handleKeyDown(simulatedEvent)
			}

		};

	return {
		init: init,
		handleMenu: handleMenu,
		animateGrid: animateGrid
	};

}(window, window.document));
