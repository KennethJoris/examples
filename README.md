# Example projects and snippets

- carbuilder: React app workshop for fellow developers. View live: http://ontwikkeling.surge.sh/
- kenneth-joris: My portfolio website. (1) View live: https://kenneth-joris.be/
- storytelling-layout: Proof of concept for a new layout. (1) View live: http://cwd.surge.sh/
-- Swipe, keyboard & mouse navigation

** (1) The node taskrunners are not included in these examples **