import React, { useContext } from 'react';
import { AppContext } from '00_global/scripts/appContext';
import '../styles/_brandListing.scss';
import Brand from '02_atoms/brand/scripts/brand';

/* Homepage brandlisting
---------------------------------------------*/
function BrandListing() {

	const { brands } = useContext(AppContext);

	return (
		<ul className="brandListing">
			{brands.map((brand, i) => {

				const { logoUrl, make } = brand;

				return (
					<li className="brandListing__item" key={i}>
						<Brand
							logo={logoUrl}
							name={make}
						/>
					</li>
				)
			})}
		</ul>
	);
}

export default BrandListing;
