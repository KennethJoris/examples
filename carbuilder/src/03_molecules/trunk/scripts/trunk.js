import React, { useContext } from 'react';
import { AppContext } from '00_global/scripts/appContext';
import '../styles/_trunk.scss';

/* Car default component
---------------------------------------------*/
function Trunk() {

	const { logo, name } = useContext(AppContext);

	return (
		<div className="trunk">
			<img className="trunk__brand" src={logo} alt={name} />
		</div>
	);
}

export default Trunk;
