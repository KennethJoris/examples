import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_carPartListing.scss';
import CarPartItem from '02_atoms/carPartItem/scripts/carPartItem';
import Airvent from '02_atoms/airvent/scripts/airvent';
import Door from '03_molecules/door/scripts/door';
import DoorStrip from '02_atoms/doorStrip/scripts/doorStrip';
import Luggage from '03_molecules/luggage/scripts/luggage';
import RoofExtention from '02_atoms/roofExtention/scripts/roofExtention';
import Spoiler from '02_atoms/spoiler/scripts/spoiler';
import Turbo from '02_atoms/turbo/scripts/turbo';
import Wheel from '03_molecules/wheel/scripts/wheel';

/* Editor - Carparts listing
---------------------------------------------*/
function CarPartListing() {

	const { options, selectedArea, components } = useContext(EditorContext);
	const selectedParts = options[selectedArea];

	return (
		<ul className={`carPartListing carPartListing--${selectedArea}`}>

			{
				// eslint-disable-next-line
				selectedParts.map((part, i) => {

					if (part === 'turbo') {
						return (
							<CarPartItem
								className={part}
								isSelected={components.turbo}
								handlerData={{
									name: part,
									value: !components.turbo
								}}
								title={part}
								key={i}
							>
								<Turbo />
							</CarPartItem>
						);
					}

					if (part === 'airvent') {
						return (
							<CarPartItem
								className={part}
								isSelected={components.airvent}
								handlerData={{
									name: part,
									value: !components.airvent
								}}
								title={part}
								key={i}
							>
								<Airvent />
							</CarPartItem>
						);
					}

					if (part === 'luggage') {
						return (
							<CarPartItem
								className={part}
								isSelected={components.luggage}
								handlerData={{
									name: part,
									value: !components.luggage
								}}
								title={part}
								key={i}
							>
								<Luggage />
							</CarPartItem>
						);
					}

					if (part === 'doorStrip') {
						return (
							<CarPartItem
								className={part}
								isSelected={components.strips}
								handlerData={{
									name: 'strips',
									value: !components.strips
								}}
								title={part}
								key={i}
							>
								<DoorStrip />
								<DoorStrip />
							</CarPartItem>
						);
					}

					if (part === 'roofExtention') {
						return (
							<CarPartItem
								className={part}
								isSelected={components.roofExtention}
								handlerData={{
									name: part,
									value: !components.roofExtention
								}}
								title={part}
								key={i}
							>
								<RoofExtention />
							</CarPartItem>
						);
					}

					if (part === 'spoiler') {
						return (
							<CarPartItem
								className={part}
								isSelected={components.spoiler}
								handlerData={{
									name: part,
									value: !components.spoiler
								}}
								title={part}
								key={i}
							>
								<Spoiler />
							</CarPartItem>
						);
					}

					if (part === 'door') {
						return (
							<React.Fragment key={i}>

								{/* Variation - Four doors */}
								<CarPartItem
									className={part}
									isSelected={components.fourDoor}
									handlerData={{
										name: 'fourDoor',
										value: true
									}}
									title={'fourdoor'}
									key={`fourDoor-${i}`}
								>
									<Door isFourDoor />
									<Door isBackDoor isFourDoor />
								</CarPartItem>

								{/* Variation - Two doors */}
								<CarPartItem
									className={part}
									isSelected={!components.fourDoor}
									handlerData={{
										name: 'fourDoor',
										value: false
									}}
									title={'twodoor'}
									key={`twoDoor-${i}`}
								>
									<Door isFourDoor />
									<Door isBackDoor />
								</CarPartItem>

							</React.Fragment>
						);
					}

					if (part === 'wheel') {
						return (
							<React.Fragment key={i}>

								{/* Variation - Solid rim */}
								<CarPartItem
									className={part}
									isSelected={components.wheel.solid}
									handlerData={{
										name: part,
										type: 'solid',
										value: true
									}}
									title={'Solid rim'}
									key={`solid-rim-${i}`}
								>
									<Wheel wheel={{ solid: true, sidewall: false, colored: false }} />
								</CarPartItem>

								{/* Variation - Alloy rim */}
								<CarPartItem
									className={part}
									isSelected={!components.wheel.solid}
									handlerData={{
										name: part,
										type: 'solid',
										value: false
									}}
									title={'Alloy rim'}
									key={`alloy-rim-${i}`}
								>
									<Wheel wheel={{ solid: false, sidewall: false, colored: false }} />
								</CarPartItem>

								{/* Variation - Tire Sidewall */}
								<CarPartItem
									className={part}
									isSelected={components.wheel.sidewall}
									handlerData={{
										name: part,
										type: 'sidewall',
										value: !components.wheel.sidewall
									}}
									title={'Tire sidewall'}
									key={`sidewall-${i}`}
								>
									<Wheel wheel={{ solid: false, sidewall: true, colored: false }} />
								</CarPartItem>

								{/* Variation - Colored Rim */}
								<CarPartItem
									className={part}
									isSelected={components.wheel.colored}
									handlerData={{
										name: part,
										type: 'colored',
										value: !components.wheel.colored
									}}
									title={'Colored rim'}
									key={`colored-rim-${i}`}
								>
									<Wheel wheel={{ solid: false, sidewall: false, colored: true }} />
								</CarPartItem>

							</React.Fragment>
						);
					}
				})
			}
		</ul>
	);
}

export default CarPartListing;
