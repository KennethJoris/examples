import React from 'react';
import '../styles/_engine.scss';
import Airvent from '02_atoms/airvent/scripts/airvent';

/* Car default component
---------------------------------------------*/
function Engine(props) {
	return (
		<div className="engine">
			{props.hasAirvent && <Airvent />}
		</div>
	);
}

export default Engine;
