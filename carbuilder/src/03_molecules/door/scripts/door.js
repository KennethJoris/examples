import React from 'react';
import '../styles/_door.scss';
import Window from '02_atoms/window/scripts/window';
import DoorHandle from '02_atoms/doorHandle/scripts/doorHandle';
import DoorStrip from '02_atoms/doorStrip/scripts/doorStrip';

/* Carpart option
---------------------------------------------*/
function Door(props) {

	const { isBackDoor, isFourDoor, hasDoorStrips } = props;

	return (
		<div className={`door 
			${isBackDoor ? 'door--back' : ''}
		`}>
			<Window 
				canOpen={!isBackDoor || isFourDoor} 
			/>
			{
				(!isBackDoor || isFourDoor) && <DoorHandle />
			}
			{
				hasDoorStrips && <DoorStrip />
			}
		</div>
	);
}

export default Door;
