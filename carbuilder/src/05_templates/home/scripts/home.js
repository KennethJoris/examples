import React, { useContext, useState, useEffect } from 'react';
import { AppContext } from '00_global/scripts/appContext';
import '../styles/_home.scss';
import Brandlisting from '03_molecules/brandListing/scripts/brandListing';

/* Home page
---------------------------------------------*/
function Home(props) {

	const { isLoading } = useContext(AppContext);
	const [showAnimation, setAnimation] = useState(isLoading);

	useEffect(() => {
		if (isLoading) { setAnimation(true); }
	}, [isLoading]);

	return (
		<div className="home">
			{
				showAnimation
					? (
						<h1
							onAnimationEnd={() => {
								if (!isLoading) setAnimation(false);
							}}
							className="home__title home__title--loading">
							Loading ...
						</h1>
					)
					: (
						<React.Fragment>
							<h1 className="home__title">Select your brand of choice:</h1>
							<Brandlisting />
						</React.Fragment>
					)
			}
		</div>
	);
}

export default Home;
