import React from 'react';
import { AppContext } from '00_global/scripts/appContext';
import { EditorContextProvider, setEditorStateByBrand } from '00_global/scripts/editorContext';
import '../styles/_editor.scss';
import Car from '04_organisms/car/scripts/car';
import Road from '02_atoms/road/scripts/road';
import ColorSelection from '03_molecules/colorSelection/scripts/colorSelection';
import CarPartPreview from '04_organisms/carPartPreview/scripts/carPartPreview';

/* Editor page
---------------------------------------------*/
class Editor extends React.Component {

	static contextType = AppContext;

	constructor(props, context) {
		super(props, context);

		const brandName = this.context.name;
		const handleCarPartsSelection = this.handleCarPartsSelection.bind(this);
		const handleSelectedArea = this.handleSelectedArea.bind(this);

		this.state = {
			...setEditorStateByBrand(brandName),
			handleCarPartsSelection: handleCarPartsSelection,
			handleSelectedArea: handleSelectedArea
		}
	}


	/* Set user selected car parts
	---------------------------------------------*/
	handleCarPartsSelection(selection) {
		if (selection.name === 'wheel') {
			this.setState(prevState => ({
				components: {
					...prevState.components,
					wheel: {
						...prevState.components.wheel,
						[selection.type]: selection.value
					}
				}
			}));
		} else {
			this.setState(prevState => ({
				components: {
					...prevState.components,
					[selection.name]: selection.value
				}
			}));
		}
	}


	/* Render car options by selected area
	---------------------------------------------*/
	handleSelectedArea(selectedArea) {
		this.setState({ selectedArea });
	}


	render() {

		const { selectedArea } = this.state;

		return (
			<EditorContextProvider value={this.state} >
				<div className="editor">
					<ColorSelection />
					<Car />
					<Road />
					{
						selectedArea && <CarPartPreview />
					}
				</div>
			</EditorContextProvider>
		);
	}
}

export default Editor;
