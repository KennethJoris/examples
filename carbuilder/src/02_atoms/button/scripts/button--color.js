import React from 'react';
import '../styles/_button.scss';
import '../styles/_button--color.scss';

/* Car color selection button
---------------------------------------------*/
class Colorbutton extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			carColor: '#ECECEC'
		};

		this.handleSelectedColor = this.handleSelectedColor.bind(this);
		this.setCarColorCssVariable = this.setCarColorCssVariable.bind(this);
	}


	/* Read selected css variable
	---------------------------------------------*/
	handleSelectedColor() {
		this.setState({
			carColor: getComputedStyle(document.documentElement).getPropertyValue(this.props.cssVariable)
		}, () => {
			this.setCarColorCssVariable();
		});
	}


	/* Color the car
	---------------------------------------------*/
	setCarColorCssVariable() {
		document.documentElement.style.setProperty('--car--color', this.state.carColor);
	}


	render() {
		return (
			<button
				onClick={this.handleSelectedColor}
				className={`button button--color ${this.props.buttonClass}`}
				type="button"
			>
				<i className="fa fa-paint-brush"></i>
			</button>
		);
	}
}

export default Colorbutton;
