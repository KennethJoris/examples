import React from 'react';
import '../styles/_bumper.scss';

/* Carpart option
---------------------------------------------*/
function Bumper(props) {
	return (
		<div className={`bumper ${props.back ? 'bumper--back' : ''}`}></div>
	);
}

export default Bumper;
