import React from 'react';
import '../styles/_rim.scss';

/* Carpart option - Wheel sub-component
---------------------------------------------*/
function Rim(props) {

	const { solid, colored } = props;
	
	return (
		<div className={`rim 
			${solid ? 'rim--solid' : ''} 
			${colored ? 'rim--gold' : ''}
		`}></div>
	);
}

export default Rim;
