import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_road.scss';

/* Car default scenery
---------------------------------------------*/
function Road() {

	const { isMoving } = useContext(EditorContext);

	return (
		<div className={`road-wrapper ${isMoving ? 'road-wrapper--moving' : ''}`}>
			<div className="road"></div>
		</div>
	);
}

export default Road;
