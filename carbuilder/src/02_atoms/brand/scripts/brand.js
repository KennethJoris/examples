import React, { useContext, useState } from 'react';
import { AppContext } from '00_global/scripts/appContext';
import '../styles/_brand.scss';

/* Homepage brand selection
---------------------------------------------*/
function Brand(props) {

	const { logo, name } = props;
	const { handleBrandSelection } = useContext(AppContext);
	const [isSelected, setSelected] = useState(false);

	function handleSelection(event) {
		event.preventDefault();
		setSelected(true);
	}

	function handleTransitionEnd() {
		isSelected && handleBrandSelection({ logo, name });
	}

	return (
		<button
			type="button"
			className={`brand ${isSelected && 'brand--is-selected'}`}
			onClick={handleSelection}
			onTransitionEnd={() => {
				handleTransitionEnd();
			}}
		>
			<img className="brand__logo" src={logo} alt={name} />
			<p className="brand__name">{name}</p>
		</button>
	);
}

export default Brand;
