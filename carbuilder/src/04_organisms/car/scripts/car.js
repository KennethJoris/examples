import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_car.scss';
import '../styles/_car-wrapper.scss';
import EngineBay from '04_organisms/engineBay/scripts/engineBay';
import PassengerArea from '04_organisms/passengerArea/scripts/passengerArea';
import TrunkSpace from '04_organisms/trunkSpace/scripts/trunkSpace';

/* Editor main component
---------------------------------------------*/
function Car() {

	const { isMoving } = useContext(EditorContext);

	return (
		<div className="car-wrapper">
			<div className={`car ${isMoving ? 'car--moving' : ''}`}>
				<EngineBay />
				<PassengerArea />
				<TrunkSpace />
			</div>
		</div>
	);
}

export default Car;
