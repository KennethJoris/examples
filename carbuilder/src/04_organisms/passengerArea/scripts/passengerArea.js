import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_passengerArea.scss';
import Luggage from '03_molecules/luggage/scripts/luggage';
import Door from '03_molecules/door/scripts/door';
import Editbutton from '02_atoms/button/scripts/button--edit';

/* Car section - middle
---------------------------------------------*/
function PassengerArea() {

	const { components } = useContext(EditorContext);

	return (
		<div className="passengerArea">
			{
				components.luggage && <Luggage />
			}
			<Door
				isFrontDoor
				hasDoorStrips={components.strips}
				isFourDoor={components.fourDoor}
			/>
			<Door
				isBackDoor
				hasDoorStrips={components.strips}
				isFourDoor={components.fourDoor}
			/>
			<Editbutton
				area={'passengerArea'}
			/>
		</div>
	);
}

export default PassengerArea;
