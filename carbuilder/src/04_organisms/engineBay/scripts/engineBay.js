import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_engineBay.scss';
import Turbo from '02_atoms/turbo/scripts/turbo';
import Bumper from '02_atoms/bumper/scripts/bumper';
import Wheel from '03_molecules/wheel/scripts/wheel';
import Engine from '03_molecules/engine/scripts/engine';
import Editbutton from '02_atoms/button/scripts/button--edit';

/* Car section - front
---------------------------------------------*/
function EngineBay() {

	const { components } = useContext(EditorContext);

	return (
		<div className="engineBay">
			{
				components.turbo && <Turbo />
			}
			<Engine hasAirvent={components.airvent} />
			<Wheel wheel={components.wheel} />
			<Bumper />
			<Editbutton
				area={'engineBay'}
			/>
		</div>
	);
}

export default EngineBay;
