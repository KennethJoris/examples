import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_carPartPreview.scss';
import Button from '02_atoms/button/scripts/button';
import Carpartlisting from '03_molecules/carPartListing/scripts/carPartListing';

/* Editor component
---------------------------------------------*/
function CarPartPreview() {

	const { selectedArea, handleSelectedArea } = useContext(EditorContext);

	return (
		<div className={`carPartPreview carPartPreview--${selectedArea}`}>
			<div className="carPartPreview__action">
				<Button
					className="button--carPartPreview"
					onClick={() => handleSelectedArea('')}
				>
					Close carpart selection
				</Button>
			</div>
			<Carpartlisting />
		</div>
	);
}

export default CarPartPreview;
