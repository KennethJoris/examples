import React from 'react'

/* initial editor state
---------------------------------------------*/
let EditorState = {
	isMoving: true,
	components: {
		turbo: false,
		airvent: false,
		luggage: false,
		fourDoor: false,
		strips: false,
		roofExtention: false,
		spoiler: false,
		wheel: {
			solid: false,
			sidewall: false,
			colored: false
		}
	},
	options: {
		engineBay: ['turbo', 'airvent', 'wheel'],
		passengerArea: ['luggage', 'doorStrip', 'door'],
		trunkSpace: ['roofExtention', 'spoiler', 'wheel']
	},
	selectedArea: '',
	handleSelectedArea: () => { },
	handleCarPartsSelection: () => { }
};


/* Sets default car options by brand
---------------------------------------------*/
const setEditorStateByBrand = (brand) => {

	/* Define current brand
	---------------------------------------------*/
	const brandName = brand.toUpperCase();
	const isBMW = brandName === 'BMW';
	const isVW = brandName === 'VW';
	const isAudi = brandName === 'AUDI';

	/* Define new state by brand
	---------------------------------------------*/
	const newState = {
		...EditorState,
		components: {
			...EditorState.components,
			airvent: isBMW,
			fourDoor: !isVW,
			spoiler: isAudi,
			wheel: {
				solid: isVW,
				sidewall: isBMW,
				colored: isAudi
			}
		}
	}

	/* Set new state
	---------------------------------------------*/
	EditorState = newState;


	return EditorState;
};


/* React context variables
---------------------------------------------*/
const EditorContext = React.createContext(EditorState);
const EditorContextProvider = EditorContext.Provider;
const EditorContextConsumer = EditorContext.Consumer;


export {
	EditorState,
	setEditorStateByBrand,
	EditorContext,
	EditorContextProvider,
	EditorContextConsumer
}