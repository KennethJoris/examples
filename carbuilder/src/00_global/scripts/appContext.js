import React from 'react'

/* initial app state
---------------------------------------------*/
let AppState = {
	brand: {
		name: '',
		logo: ''
	},
	brands: [],
	isLoading: true,
	error: null,
	handleBrandSelection: () => { }
};


/* React context variables
---------------------------------------------*/
const AppContext = React.createContext(AppState);
const AppContextProvider = AppContext.Provider;


export {
	AppState,
	AppContext,
	AppContextProvider
}