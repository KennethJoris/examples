import React from 'react';
import { AppContextProvider, AppState } from '00_global/scripts/appContext';
import '../styles/main.scss';
import Home from '05_templates/home/scripts/home';
import Editor from '05_templates/editor/scripts/editor';

class App extends React.Component {

	constructor(props) {
		super(props);

		const handleBrandSelection = this.handleBrandSelection.bind(this);

		this.state = {
			...AppState,
			handleBrandSelection
		};
	}


	/* Home page brand selection
	---------------------------------------------*/
	handleBrandSelection(brand) {
		this.setState({ brand })
	}


	/* Retrieve different brands
	---------------------------------------------*/
	componentDidMount() {
		fetch('https://car-api.firebaseio.com/rest.json')
			.then(response => {
				if (response.ok) {
					return response.json();
				} else {
					throw new Error('Something went wrong ...');
				}
			})
			.then(brands => this.setState({ brands, isLoading: false }))
			.catch(error => this.setState({ error, brands: null, isLoading: false }));
	}


	render() {

		const { brands, brand, isLoading, error, handleBrandSelection } = this.state;

		if (error) {
			return <p>{error.message}</p>;
		}

		/* Render Home page or Editor page
		---------------------------------------------*/
		return (
			<React.Fragment>
				{
					brand.name && brand.logo
						? (
							<AppContextProvider value={brand} >
								<Editor />
							</AppContextProvider>
						)
						: (
							<AppContextProvider value={{ isLoading, brands, handleBrandSelection }} >
								<Home />
							</AppContextProvider>
						)
				}
			</React.Fragment>
		);
	}
}

export default App;
